
<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <li class="nav-item">
            <a class="nav-link" href="${pageContext.request.contextPath}/admin/home">
                <i class="fa fa-dashboard"></i>
                <span class="menu-title">Manage Home</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="${pageContext.request.contextPath}/admin/services">
                <i class="mdi mdi-view-carousel"></i>
                <span class="menu-title">Manage Service</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="${pageContext.request.contextPath}/admin/contactus">
                <i class="mdi mdi-clipboard-text"></i>
                <span class="menu-title">Manage Contact</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="${pageContext.request.contextPath}/admin/detailservice">
                <i class="mdi mdi-tooltip-outline-plus"></i>
                <span class="menu-title">Detail Service</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="${pageContext.request.contextPath}/admin/technologyRobotic">
                <i class="mdi mdi-spotlight"></i>
                <span class="menu-title">Technology & Robotic</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="${pageContext.request.contextPath}/admin/profile">
                <i class="mdi mdi-clipboard-account"></i>
                <span class="menu-title">My Profile</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="${pageContext.request.contextPath}/admin/config">
                <i class="mdi mdi-settings"></i>
                <span class="menu-title">Manage Config</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="${pageContext.request.contextPath}/admin/news">
                <i class="mdi mdi-newspaper"></i>
                <span class="menu-title">Manage News</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="${pageContext.request.contextPath}/admin/menu">
                <i class="mdi mdi-menu"></i>
                <span class="menu-title">Manage Menu</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/admin/logout">
                <i class="mdi mdi-lock-open-outline"></i>
                <span class="menu-title">Logout</span>
            </a>
        </li>
    </ul>
</nav>


