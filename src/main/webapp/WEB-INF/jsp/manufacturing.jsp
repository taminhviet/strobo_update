<%--
  Created by IntelliJ IDEA.
  User: MyKavalli
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="${pageContext.request.contextPath}/images/favicon.png" type="image/png" sizes="16x16">
    <title>Manufacturing</title>

    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/Meta_Serif.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/Lato.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/slick.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/fullpage.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/jquery.scrollbar.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/owl.carousel.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/my.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/manufacturing.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/st_general.css" />
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

    <script type="text/javascript" src="${pageContext.request.contextPath}/js/strobo/scrolloverflow.js"></script>
    <script src="${pageContext.request.contextPath}/js/strobo/scrolloverflow.js"></script>
    <script src="${pageContext.request.contextPath}/js/strobo/fullpage.js"></script>
    <script src="${pageContext.request.contextPath}/js/strobo/slick.js"></script>
    <script src="${pageContext.request.contextPath}/js/strobo/owl.carousel.js"></script>
    <script src="${pageContext.request.contextPath}/js/strobo/my.js"></script>
</head>
<body class="body-item manufacturing">
    <div class="menu-body">
        <img src="${pageContext.request.contextPath}/assets/menu_black_close.png" class="close-menu">
        <div class="menu-content">
            <div class="content">
        <ul>
            <c:forEach items="${menuDefault}" var="menu">
                <c:if test="${menu.url == '#home'}">
                    <li><a class="menu-item" data-menu="home" href="${pageContext.request.contextPath}/">${menu.value}</a></li>
                </c:if>
                <c:if test="${menu.url == '#about'}">
                    <li><a class="menu-item" data-menu="about" href="${pageContext.request.contextPath}/#about-us">${menu.value}</a></li>
                </c:if>
                <c:if test="${menu.url == '#core'}">
                    <li><a class="menu-item" data-menu="core" href="${pageContext.request.contextPath}/technology">${menu.value}</a></li>
                </c:if>
            </c:forEach>
            <li><a  class="menu-item solutions" data-menu="solutions" href="${pageContext.request.contextPath}/services">Our Solutions</a>
                <ol class="submenu">
                    <c:forEach items="${menuDefault}" var="menu">
                        <c:if test="${menu.url == '#seaport'}">
                            <li><a class="sub-item" data-menu="seaport" href="${pageContext.request.contextPath}/services/seaport">${menu.value}</a></li>
                        </c:if>
                        <c:if test="${menu.url == '#manufacturing'}">
                            <li><a class="sub-item active" data-menu="manufacturing" href="${pageContext.request.contextPath}/services/manufacturing">${menu.value}</a></li>
                        </c:if>
                        <c:if test="${menu.url == '#warehouse'}">
                            <li><a class="sub-item" data-menu="warehouse" href="${pageContext.request.contextPath}/services/warehouse">${menu.value}</a></li>
                        </c:if>
                        <c:if test="${menu.url == '#airport'}">
                            <li><a class="sub-item" data-menu="airport" href="${pageContext.request.contextPath}/services/airport">${menu.value}</a></li>
                        </c:if>
                    </c:forEach>
                </ol>
            </li>

            <c:forEach items="${menuNew}" var="menu">
                <c:if test="${menu.status == true}">
                    <li><a class="menu-item" href="${pageContext.request.contextPath}/${menu.url}">${fn:toUpperCase(menu.value)}</a></li>
                </c:if>
            </c:forEach>

            <c:forEach items="${menuDefault}" var="menu">
                <c:if test="${menu.url == '#contact'}">
                    <li><a class="menu-item" data-menu="contact" href="${pageContext.request.contextPath}/contact">${menu.value}</a></li>
                </c:if>
            </c:forEach>
            <li><a class="menu-item" data-menu="contact" href="${pageContext.request.contextPath}/news">News</a></li>
            <li><img class="st-engineering" src="${pageContext.request.contextPath}/assets/ST_engineering.png"></li>
        </ul>
    </div>
    </div>
    </div>
    <div class="body-content manufacturing">
        <div class="item-content active">
            <div class="all-item manufacturing active">
                <div class="manufacturing-header header">
                    <a href="${pageContext.request.contextPath}/" class="logo-back-home"><img class="logo" src="${pageContext.request.contextPath}/assets/logo.png"></a>
                    <c:if test="${ not empty pagemanufacturing.pageContent}">
                        <p>${pagemanufacturing.pageContent}</p>
                     </c:if>
                     <c:if test="${ empty pagemanufacturing.pageContent}">
                        <p>Comprehensive end-to-end automation solutions to improve processes and efficiency, helping customers reduce waste, investment and manpower requirements.</p>
                     </c:if>
                    <a href="#" class="menu"></a>
                </div>
                <div class="manufacturing-content content">
                    <a href="${pageContext.request.contextPath}/services/seaport" class="prev-detail-service"></a>
                    <c:if test="${ not empty pagemanufacturing.pageName}">
                        <img class="bg" src="${pageContext.request.contextPath}/media/images/${pagemanufacturing.pageName}.png">
                    </c:if>
                    <c:if test="${ empty pagemanufacturing.pageName}">
                        <img src="assets/all_manufacturing.png" alt="" class="bg">
                    </c:if>
                    <div class="manufacturing_1">
                        <div class="description">
                            <c:if test="${ not empty pagemanufacturing1.content}">
                                <p class="right" type-for="manufacturing" data-img="manufacturing_1">${pagemanufacturing1.content}</p>
                            </c:if>
                            <c:if test="${ empty pagemanufacturing1.content}">
                                <p class="right">Robotic Arm Solutions</p>
                            </c:if>
                        </div>
                        <div class="image">
                            <div class="table-cell">
                                <c:if test="${ not empty pagemanufacturing1.pageName}">
                                    <img src="${pageContext.request.contextPath}/media/images/${pagemanufacturing1.pageName}.png" type-for="manufacturing" data-img="manufacturing_1">
                                </c:if>
                                <c:if test="${ empty pagemanufacturing1.pageName}">
                                    <img src="${pageContext.request.contextPath}/assets/manufacturing_1.png" data-img="manufacturing_1">
                                </c:if>
                            </div>
                        </div>
                    </div>
                    <div class="manufacturing_2">
                        <div class="description">
                            <c:if test="${ not empty pagemanufacturing2.content}">
                                <p type-for="manufacturing" data-img="manufacturing_2" class="right">${pagemanufacturing2.content}</p>
                            </c:if>
                            <c:if test="${ empty pagemanufacturing2.content}">
                                <p class="right">Autonomous Pallet Truck</p>
                            </c:if>
                        </div>
                        <div class="image">
                            <div class="table-cell">
                                <c:if test="${ not empty pagemanufacturing2.pageName}">
                                    <img src="${pageContext.request.contextPath}/media/images/${pagemanufacturing2.pageName}.png" type-for="manufacturing" data-img="manufacturing_2">
                                </c:if>
                                <c:if test="${ empty pagemanufacturing2.pageName}">
                                    <img src="${pageContext.request.contextPath}/assets/manufacturing_2.png" data-img="manufacturing_2">
                                </c:if>
                            </div>
                        </div>
                    </div>
                    <div class="manufacturing_3">
                        <div class="image">
                            <div class="table-cell">
                                <c:if test="${ not empty pagemanufacturing3.pageName}">
                                    <img src="${pageContext.request.contextPath}/media/images/${pagemanufacturing3.pageName}.png"  type-for="manufacturing" data-img="manufacturing_3">
                                </c:if>
                                <c:if test="${ empty pagemanufacturing3.pageName}">
                                    <img src="${pageContext.request.contextPath}/assets/manufacturing_2.png" data-img="manufacturing_3">
                                </c:if>
                            </div>
                        </div>
                        <div class="description">
                            <c:if test="${ not empty pagemanufacturing3.content}">
                                <p type-for="manufacturing" data-img="manufacturing_3">${pagemanufacturing3.content}</p>
                            </c:if>
                            <c:if test="${ empty pagemanufacturing3.content}">
                                <p>TUG Autonomous Mobile Robot</p>
                            </c:if>
                        </div>
                    </div>
                    <a href="${pageContext.request.contextPath}/services/warehouse" class="next-detail-service"></a>
                </div>
                <div class="all-footer footer">
                    <span class="first-detail" goto-for="manufacturing_1" ></span>
                    <h2>Manufacturing Solutions</h2>
                    <p class="go-rms">Robotics Management System</p>
                </div>
            </div>
        </div>
        <div class="item-details transition-down-hide">

            <!-- MANUFACTURING -->
            <div class="for-manufacturing for">
                <div da-slick="manufacturing_1" class="item manufacturing_1">
                    <div class="detail-header header">
                        <a href="${pageContext.request.contextPath}/" class="logo-back-home"><img class="logo" src="${pageContext.request.contextPath}/assets/logo.png"></a>
                        <a href="#" class="menu"></a>
                    </div>
                    <div class="detail-content">
                        <span class="pre-detail" type-move="manufacturing"></span>
                        <div class="view">
                            <div class="content-left">
                                <div>
                                    <div class="for-main">
                                        <c:if test="${fn:length(pagemanufacturing1.listSlide) > 1}">
                                            <span class="prev-item" data-slide="manufacturing_1"></span>
                                        </c:if>
                                        <c:if test="${not empty pagemanufacturing1.listSlide}">
                                            <c:set var="slide21" value="1"/>
                                            <c:forEach items="${pagemanufacturing1.listSlide}" var="imgSlide21">
                                                <c:if test="${slide21 == 1}">
                                                    <img class="service-img main manufacturing_1" src="${pageContext.request.contextPath}/media/images/${imgSlide21.slideimg}">
                                                </c:if>
                                            <c:set var="slide21" value="${slide21 + 1}"/>
                                            </c:forEach>
                                        </c:if>
                                        <c:if test="${fn:length(pagemanufacturing1.listSlide) > 1}">
                                            <span class="next-item" data-slide="manufacturing_1"></span>
                                        </c:if>
                                    </div>
                                    <c:if test="${fn:length(pagemanufacturing1.listSlide) > 1}">
                                        <div class="body-thumbnail">
                                            <div class="thumbnail owl-carousel">
                                                <c:set var="n21" value="1"/>
                                                <c:forEach items="${pagemanufacturing1.listSlide}" var="imgSlide">
                                                    <c:if test="${not empty imgSlide}" >
                                                        <c:if test="${n21 == 1}">
                                                            <c:set var="addActive21" value="active"/>
                                                        </c:if>
                                                        <div class="around">
                                                            <img data-img="${n21}" type-click="manufacturing_1" class="img-thumbnail img-${n21} ${addActive21} manufacturing_1" src="${pageContext.request.contextPath}/media/images/${imgSlide.slideimg}">
                                                        </div>
                                                        <c:set var="n21" value="${n21 + 1}"/>
                                                    </c:if>
                                                </c:forEach>
                                            </div>
                                        </div>
                                    </c:if>
                                </div>
                            </div>
                            <div class="content-right">
                                <c:if test="${not empty pagemanufacturing1.content}">
                                    <h2>${pagemanufacturing1.content}</h2>
                                </c:if>
                                <c:if test="${ empty pagemanufacturing1.content}">
                                    <h2>Robotic Arm Solutions</h2>
                                </c:if>
                                
                                <p>KEY FEATURES</p>
                                <c:if test="${ not empty pagemanufacturing1.detailContent}">
                                    ${pagemanufacturing1.detailContent}
                                </c:if>
                                <c:if test="${ empty pagemanufacturing1.detailContent}">
                                    <ul>
                                        <li>Customised solutions for automated manufacturing via robotic systems</li>
                                        <li>Robot automation to handle unique functions such as tools change and part handling</li>
                                        <li>Wide range of arm reach, payload to flexibility of robotic systems</li>
                                        <li>Robot and work cell inventory integrated with smart safety sensors</li>
                                        <li>Boosts productivity and delivers high quality results regardless of mix and volume</li>
                                    </ul>
                                </c:if>
                                    <c:if test="${not empty pagemanufacturing1.brochure}">
                                        <div class="download">
                                            <a href="/brochure/${pagemanufacturing1.brochure}">Download brochure</a>
                                        </div>
                                    </c:if>
                                <div class="watch-video"><a href="#" type-video="manufacturing_1">Watch video</a></div>
                            </div>
                        </div>
                            <div class="video">
                                <video id="player_manufacturing_1" class="video-manufacturing_1" controls width="100%">
                                    <source src="${pageContext.request.contextPath}/media/video/manufacturing/manufacturing.mp4" type="video/mp4">
                                </video>
                                <div class="play-button"><span></span></div>
                                <p class="play-video" >Watch video</p>
                            </div>
                        <span class="next-detail" type-move="manufacturing"></span>
                    </div>
                    <div class="detail-footer footer">
                        <span class="back detail-back" num-video="manufacturing_1" type-back="manufacturing"></span>
                        <span class="scroll-down">scroll down</span>
                    </div>
                </div>
                <div da-slick="manufacturing_2" class="item manufacturing_2">
                    <div class="detail-header header">
                        <a href="${pageContext.request.contextPath}/" class="logo-back-home"><img class="logo" src="${pageContext.request.contextPath}/assets/logo.png"></a>
                        <a href="#" class="menu"></a>
                    </div>
                    <div class="detail-content">
                        <span class="pre-detail" type-move="manufacturing"></span>
                        <div class="view">
                            <div class="content-left">
                                <div>
                                    <div class="for-main">
                                        <c:if test="${fn:length(pagemanufacturing2.listSlide) > 1}">
                                            <span class="prev-item"  data-slide="manufacturing_2"></span>
                                        </c:if>
                                        <c:if test="${not empty pagemanufacturing2.listSlide}">
                                            <c:set var="slide22" value="1"/>
                                            <c:forEach items="${pagemanufacturing2.listSlide}" var="imgSlide22">
                                                <c:if test="${slide22 == 1}">
                                                    <img class="service-img main manufacturing_2" src="${pageContext.request.contextPath}/media/images/${imgSlide22.slideimg}">
                                                </c:if>
                                            <c:set var="slide22" value="${slide22 + 1}"/>
                                            </c:forEach>
                                        </c:if>
                                        <c:if test="${fn:length(pagemanufacturing2.listSlide) > 1}">
                                            <span class="next-item"  data-slide="manufacturing_2"></span>
                                        </c:if>
                                    </div>
                                    <c:if test="${fn:length(pagemanufacturing2.listSlide) > 1}">
                                        <div class="body-thumbnail">
                                            <div class="thumbnail owl-carousel">
                                                <c:set var="n22" value="1"/>
                                                <c:forEach items="${pagemanufacturing2.listSlide}" var="imgSlide">
                                                    <c:if test="${not empty imgSlide}" >
                                                        <c:if test="${n22 == 1}">
                                                            <c:set var="addActive22" value="active"/>
                                                        </c:if>
                                                        <div class="around">
                                                            <img data-img="${n22}" type-click="manufacturing_2" class="img-thumbnail img-${n22} ${addActive22} manufacturing_2" src="${pageContext.request.contextPath}/media/images/${imgSlide.slideimg}">
                                                        </div>
                                                        <c:set var="n22" value="${n22 + 1}"/>
                                                    </c:if>
                                                </c:forEach>
                                            </div>
                                        </div>
                                    </c:if>
                                </div>
                            </div>
                            <div class="content-right">
                                <c:if test="${not empty pagemanufacturing2.content}">
                                    <h2>${pagemanufacturing2.content}</h2>
                                </c:if>
                                <c:if test="${ empty pagemanufacturing2.content}">
                                    <h2>Autonomous Pallet Truck</h2>
                                </c:if>    
                                
                                <p>KEY FEATURES</p>
                                <c:if test="${not empty pagemanufacturing2.detailContent}">
                                    ${pagemanufacturing2.detailContent}
                                </c:if>
                                <c:if test="${ empty pagemanufacturing2.detailContent}">
                                    <ul>
                                        <li>Suitable for indoor and outdoor operations</li>
                                        <li>Collision detection and avoidance</li>
                                        <li>Advanced pallet detection and recognition capability</li>
                                        <li>Interfaces with Warehouse Management</li>
                                        <li>System for seamless operation</li>
                                    </ul>
                                </c:if>
                                    <c:if test="${not empty pagemanufacturing2.brochure}">
                                        <div class="download">
                                            <a href="/brochure/${pagemanufacturing2.brochure}">Download brochure</a>
                                        </div>
                                    </c:if>
                                <div class="watch-video"><a href="#" type-video="manufacturing_2">Watch video</a></div>
                            </div>
                        </div>
                            <div class="video">
                                <video id="player_manufacturing_2" class="video-manufacturing_1" controls width="100%">
                                    <source src="${pageContext.request.contextPath}/media/video/manufacturing/manufacturing.mp4" type="video/mp4">
                                </video>
                                <div class="play-button"><span></span></div>
                                <p class="play-video" >Watch video</p>
                            </div>
                        <span class="next-detail" type-move="manufacturing"></span>
                    </div>
                    <div class="detail-footer footer">
                        <span class="back detail-back" num-video="manufacturing_2" type-back="manufacturing"></span>
                        <span class="scroll-down">scroll down</span>
                    </div>
                </div>
                <div da-slick="manufacturing_3" class="item manufacturing_3">
                    <div class="detail-header header">
                        <a href="${pageContext.request.contextPath}/" class="logo-back-home"><img class="logo" src="${pageContext.request.contextPath}/assets/logo.png"></a>
                        <a href="#" class="menu"></a>
                    </div>
                    <div class="detail-content">
                        <span class="pre-detail" type-move="manufacturing"></span>
                        <div class="view">
                            <div class="content-left">
                                <div>
                                    <div class="for-main">
                                        <c:if test="${fn:length(pagemanufacturing3.listSlide) > 1}">
                                            <span class="prev-item" data-slide="manufacturing_3"></span>
                                        </c:if>
                                        <c:if test="${not empty pagemanufacturing3.listSlide}">
                                            <c:set var="slide23" value="1"/>
                                            <c:forEach items="${pagemanufacturing3.listSlide}" var="imgSlide23">
                                                <c:if test="${slide23 == 1}">
                                                    <img class="service-img main manufacturing_3" src="${pageContext.request.contextPath}/media/images/${imgSlide23.slideimg}">
                                                </c:if>
                                            <c:set var="slide23" value="${slide23 + 1}"/>
                                            </c:forEach>
                                        </c:if>
                                        <c:if test="${fn:length(pagemanufacturing3.listSlide) > 1}">
                                            <span class="next-item" data-slide="manufacturing_3"></span>
                                        </c:if>
                                    </div>
                                    <c:if test="${fn:length(pagemanufacturing3.listSlide) > 1}">
                                        <div class="body-thumbnail">
                                            <div class="thumbnail owl-carousel">
                                                <c:set var="n23" value="1"/>
                                                <c:forEach items="${pagemanufacturing3.listSlide}" var="imgSlide">
                                                    <c:if test="${not empty imgSlide}" >
                                                        <c:if test="${n23 == 1}">
                                                            <c:set var="addActive23" value="active"/>
                                                        </c:if>
                                                        <div class="around">
                                                            <img data-img="${n23}" type-click="manufacturing_3" class="img-thumbnail img-${n23} ${addActive23} manufacturing_3" src="${pageContext.request.contextPath}/media/images/${imgSlide.slideimg}">
                                                        </div>
                                                        <c:set var="n23" value="${n23 + 1}"/>
                                                    </c:if>
                                                </c:forEach>
                                            </div>
                                        </div>
                                    </c:if>
                                </div>
                            </div>
                            <div class="content-right">
                                <c:if test="${not empty pagemanufacturing3.content}">
                                    <h2 class="tug"><a href="https://aethon.com/" target="_blank">${pagemanufacturing3.content}</a></h2>
                                </c:if>
                                <c:if test="${ empty pagemanufacturing3.content}">
                                    <h2 class="tug"><a href="https://aethon.com/" target="_blank">TUG Autonomous Mobile Robot</a></h2>
                                </c:if>
                                
                                <p>KEY FEATURES</p>
                                <c:if test="${not empty pagemanufacturing3.detailContent}">
                                    ${pagemanufacturing3.detailContent}
                                </c:if>
                                <c:if test="${ empty pagemanufacturing3.detailContent}">
                                    <ul>
                                        <li>24x7 improved productivity</li>
                                        <li>Auto docks and auto charges</li>
                                        <li>Automatic drop off and pickup of carts</li>
                                        <li>Can travel on irregular concrete floors</li>
                                        <li>Navigates on internal map of facility using laser</li>
                                        <li>Has multiple dispatch modes</li>
                                    </ul>
                                </c:if>
                                    <c:if test="${not empty pagemanufacturing3.brochure}">
                                        <div class="download">
                                            <a href="/brochure/${pagemanufacturing3.brochure}">Download brochure</a>
                                        </div>
                                    </c:if>
                                <div class="watch-video"><a href="#" type-video="manufacturing_3">Watch video</a></div>
                            </div>
                        </div>
                            <div class="video">
                                <video id="player_manufacturing_3" class="video-manufacturing_1" controls width="100%">
                                    <source src="${pageContext.request.contextPath}/media/video/manufacturing/manufacturing.mp4" type="video/mp4">
                                </video>
                                <div class="play-button"><span></span></div>
                                <p class="play-video" >Watch video</p>
                            </div>
                        <span class="next-detail" type-move="manufacturing"></span>
                    </div>
                    <div class="detail-footer footer">
                        <span class="back detail-back" num-video="manufacturing_3" type-back="manufacturing"></span>
                        <span class="scroll-down">scroll down</span>
                    </div>
                </div>
            </div>
            <!-- END MANUFACTURING -->

            <!-- RMS -->

            <div class="item rms">
                <div class="detail-header header">
                    <a href="${pageContext.request.contextPath}/" class="logo-back-home"><img class="logo" src="${pageContext.request.contextPath}/assets/logo.png"></a>
                    <a href="#" class="menu"></a>
                </div>
                <div class="detail-content">
                    <div class="view">
                        <div class="content-left">
                            <div>
                                <img class="service-img main" src="${pageContext.request.contextPath}/assets/detail_rms.png">
                            </div>
                        </div>
                        <div class="content-right">
                            <c:if test="${empty robotic.name}">
                                <h2>Robotics Management System</h2>
                            </c:if>
                            <c:if test="${not empty robotic.name}">
                                <h2>${robotic.name}</h2>
                            </c:if>
                            <p>DESCRIPTION</p>
                            <c:if test="${empty robotic.description}">
                                <h5>Harnessing the power of technology and smart engineering, every STROBO autonomous solution is integrated with ST Engineering’s proprietary Robotics Management System (RMS) which allows seamless operation and fleet management in existing infrastructure.</h5>
                            </c:if>
                            <c:if test="${not empty robotic.description}">
                                <h5>${robotic.description}</h5>
                            </c:if>
                            <p>KEY FEATURES</p>
                            <c:if test="${not empty robotic.keyfeatures}">
                                ${robotic.keyfeatures}
                            </c:if>
                            <c:if test="${ empty robotic.keyfeatures}">
                                <ul>
                                    <li>Real-time platform status</li>
                                    <li>Fail-safe disabling</li>
                                    <li>Interface for mobility mode control</li>
                                    <li>Interface for payload control</li>
                                    <li>Manned to unmanned switching</li>
                                    <li>Linked to existing infrastructure</li>
                                    <li>Collaborative networking</li>
                                    <li>Wireless transmission Cybersecure</li>
                                </ul>
                            </c:if>
                        </div>
                    </div>
                </div>
                <div class="detail-footers footer">
                    <span class="back detail-back" num-video="rms" type-back="manufacturing"></span>
                    <span class="scroll-down">scroll down</span>
                </div>
            </div>
            <!-- END RMS -->

        </div>
        <div class="all-footers footer">
            <p class="footer">COPYRIGHT @2018 ST ENGINEERING LAND SYSTEMS LTD. | ALL RIGHTS RESERVED</p>
        </div>
    </div>
</body>
</html>