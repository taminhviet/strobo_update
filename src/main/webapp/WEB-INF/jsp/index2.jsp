<%--
  Created by IntelliJ IDEA.
  User: My Kavalli
  Date: 04/09/2018
  Time: 13:42 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<html>
    <head>

        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="keywords" content="${homePage.keyword}">
        <meta name="description" content="${homePage.meta}">
        <c:if test="${empty homePage.title}" ><c:set var="home_title" value="STrobo"/></c:if>
        <c:if test="${not empty homePage.title}" ><c:set var="home_title" value="${homePage.title}"/></c:if>
        <c:if test="${empty pageInfo.title}" ><c:set var="service_title" value="STrobo Service"/></c:if>
        <c:if test="${not empty pageInfo.title}" ><c:set var="service_title" value="${pageInfo.title}"/></c:if>
        <c:if test="${empty contacts.title}" ><c:set var="contact_title" value="STrobo Contact"/></c:if>
        <c:if test="${not empty contacts.title}" ><c:set var="contact_title" value="${contacts.title}"/></c:if>
        <title>${home_title}</title>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/home.css" type="text/css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/fonts.css" type="text/css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/contact.css" type="text/css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/service.css" type="text/css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/frontend.css" type="text/css">
        <script src="${pageContext.request.contextPath}/js/strobo/jquery-3.3.1.js"></script>
        <script src="${pageContext.request.contextPath}/js/strobo/home.js" type="text/javascript"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/2.6.6/jquery.fullPage.css" rel="stylesheet" type="text/css">
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/images/favicon.png" />
        <style type="text/css">.pixelParallel-panel[data-v-54cb82b4]{position:fixed;right:10px;bottom:10px;z-index:2147483647;width:375px;height:265px;transform:translate(0);background:#fff;transition:width .2s ease-out,height .2s ease-out;will-change:top,left,width,height,transform;opacity:0}.pixelParallel-panel-inner[data-v-54cb82b4]{position:relative;overflow:hidden;height:100%;border:1px solid #dddedf;box-shadow:0 2px 10px rgba(0,0,0,.1);box-sizing:border-box}.pixelParallel-panel-handle[data-v-54cb82b4]{position:absolute;top:-10px;right:-10px;z-index:3;width:20px;height:20px;background:red;transform:rotate(45deg);background:linear-gradient(0deg,transparent,transparent 50%,#ccc 0,#ccc);background-size:100% 2px;cursor:move}.pixelParallel-panel-isolator[data-v-54cb82b4]{border:0;width:100%;height:100%;overflow:hidden;position:relative}.pixelParallel-panel-dragging .pixelParallel-panel-inner[data-v-54cb82b4]{pointer-events:none}.pixelParallel-panel-minimized[data-v-54cb82b4]{width:112px;height:50px;transition:width .2s ease-out .15s,height .2s ease-out .15s}@media (max-width:395px){.pixelParallel-panel[data-v-54cb82b4]{right:0;bottom:0;width:320px}.pixelParallel-panel-minimized[data-v-54cb82b4]{width:112px}}</style><style type="text/css">.pixelParallel-overlay{position:absolute;left:0;top:0;width:100%;text-align:center;white-space:nowrap;display:none}.pixelParallel-overlay-enabled{display:block}</style><style type="text/css">.pixelParallel-image-outer{visibility:hidden;position:absolute;top:0;left:0;width:100%;overflow:hidden;max-width:100vw;min-height:100vh}.pixelParallel-image-inner{position:absolute;z-index:2147483646;left:50%;top:0;cursor:move;transform:translateX(-50%);transition:opacity .15s}.pixelParallel-image-inner img{width:auto;height:auto;max-width:none;max-height:none;vertical-align:top;margin:0;padding:0;position:relative;transform-origin:50% 0;top:-1px;border:1px dashed #333}.pixelParallel-image-inner img:not([src]),.pixelParallel-image-inner img[src=""]{visibility:hidden}.pixelParallel-image-enabled{visibility:visible}.pixelParallel-image-difference{mix-blend-mode:difference}.pixelParallel-image-difference img{opacity:1!important}.pixelParallel-image-locked,.pixelParallel-image-no-image{pointer-events:none}.pixelParallel-image-locked .pixelParallel-image-inner img{top:0;border:0 none}</style><style type="text/css">.pixelParallel-grids{position:relative;z-index:2147483646;pointer-events:none}.pixelParallel-grid-horizontal,.pixelParallel-grid-vertical{position:fixed;z-index:1;pointer-events:none;visibility:hidden}.pixelParallel-grid-horizontal{top:50%;left:50%;display:table;width:100vw;height:200vh;opacity:.5;table-layout:fixed;border-spacing:30px;transform:translate(-50%,-50%)}.pixelParallel-grid-horizontal span{display:table-cell;background:red;height:200vh}.pixelParallel-grid-vertical{top:0;left:0;width:100%;height:100%;background:linear-gradient(180deg,transparent,transparent 90%,blue 0,blue);background-size:100% 10px;opacity:.5}.pixelParallel-grid-horizontal-enabled,.pixelParallel-grid-vertical-enabled{visibility:visible}</style><style type="text/css">.pixelParallel-rulers{position:relative;z-index:2147483646}.pixelParallel-ruler-x,.pixelParallel-ruler-y{display:none;position:fixed;top:0;left:0;z-index:2;background:cyan}.pixelParallel-ruler-x:after,.pixelParallel-ruler-y:after{content:"";position:absolute;top:-8px;left:-8px;right:0;bottom:0;padding:10px}.pixelParallel-ruler-x{right:0;height:1px;cursor:row-resize}.pixelParallel-ruler-y{bottom:0;width:1px;cursor:col-resize}.pixelParallel-rulers-enabled .pixelParallel-ruler-x,.pixelParallel-rulers-enabled .pixelParallel-ruler-y{display:block}
        </style>
    </head>
    <body>
        <div class="menu-list ">
            <a class="menu-close" style="display: block;">
                <div class="icon"></div>
            </a>
            <ul class="m-menu">
                <c:forEach items="${menuDefault}" var="menu">
                    <c:if test="${menu.url == 'firstSection/1/#airport'}"><c:set var="id" value="airport" /></c:if>
                    <c:if test="${menu.url == 'firstSection/1/#seaport'}"><c:set var="id" value="seaport" /></c:if>
                    <c:if test="${menu.url == 'firstSection/1/#warehouse'}"><c:set var="id" value="warehouse" /></c:if>
                    <c:if test="${menu.url == 'firstSection/1/#manufacturing'}"><c:set var="id" value="manufacturing" /></c:if>
                    <c:if test="${menu.url == 'firstSection/1'}"><c:set var="id" value="all-products" /></c:if>
                    <c:if test="${menu.url == 'index'}"><li><a data-link="${menu.url}" data-title="${home_title}" id="m_home" href="${pageContext.request.contextPath}/">${fn:toUpperCase(menu.value)}</a></li></c:if>
                    <c:if test="${menu.url != 'index' && menu.url != 'firstSection/2'}"><li><a data-link="${menu.url}"  data-title="${service_title}" id="m_${id}" href="${pageContext.request.contextPath}#${menu.url}">${fn:toUpperCase(menu.value)}</a></li></c:if>
                    <c:if test="${menu.url == 'firstSection/2'}"><li><a data-link="${menu.url}" data-title="${contact_title}" id="m_contact" href="${pageContext.request.contextPath}#${menu.url}">${fn:toUpperCase(menu.value)}</a></li></c:if>
                </c:forEach>
                <c:forEach items="${menuNew}" var="menu">
                    <c:if test="${menu.status == true}">
                        <li><a id="m_${menu.url}" href="${pageContext.request.contextPath}/${menu.url}">${fn:toUpperCase(menu.value)}</a></li>
                    </c:if>
                </c:forEach>
            </ul>
        </div>
        <div id="fullpage">
            <section class="vertical-scrolling">
                <c:if test="${homeStatus == true}">
                    <div class="horizontal-scrolling">
                        <div class="home">
                            <video loop muted id="bkg-video" class="bkg-video" poster="${pageContext.request.contextPath}/images/bkg-home.png"><source src="${pageContext.request.contextPath}/media/video/${homePage.file}" type="video/mp4">
                            </video>
                            <div class="video-overlay"></div>
                            <div class="header">
                                <div class="wrap">
                                    <div class="logo">
                                        <a href="/" title="ST Robo">
                                            <img src="${pageContext.request.contextPath}/images/logo-white.svg">
                                        </a>
                                    </div>
                                    <div class="menu">
                                        <a class="menu-open" style="display: block;">
                                            <div class="icon"></div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="content wrap">
                                <h2>${homePage.heading1}<span>${homePage.heading2}</span>${homePage.heading3}<span class="dot_tit_home">.</span></h2>
<%--                                 <a class="home-link" href="${homePage.buttonLink}"> --%>
                                    <button class="btn">
                                        <span>${homePage.buttonLabel}</span>
                                    </button>
                                </a>
                            </div>
                            <div class="footer">
                                <div class="wrap">
                                    <div class="right">
                                        <c:if test="${serviceStatus == true}">
                                            <div class="line"></div>
                                            <a class="next-page" data-link="#firstSection/1" data-title="${service_title}" href="#firstSection/1" style="display: block;">
                                                <div class="icon"></div>
                                            </a>
                                        </c:if>
                                        <c:if test="${serviceStatus == false}">
                                            <c:if test="${contactStatus == true}">
                                                <div class="line"></div>
                                                <a class="next-page" data-link="#firstSection/2" data-title="${contact_title}" href="#firstSection/2" style="display: block;">
                                                    <div class="icon"></div>
                                                </a>
                                            </c:if>
                                        </c:if>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </c:if>
                <c:if test="${serviceStatus == true}">
                    <div class="horizontal-scrolling">
                        <div class="page">
                            <div class="header">
                                <div class="wrap">
                                    <div class="logo">
                                        <a href="/" title="ST Robo">
                                            <img src="${pageContext.request.contextPath}/images/logo.png">
                                        </a>
                                    </div>
                                    <div class="menu">
                                        <a class="menu-open" style="display: block;">
                                            <div class="icon"></div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="content">
                                <div class="listing-service">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <c:set var="num_service" value="1" />
                                            <c:forEach items="${services}" var="service">
                                                <c:if test="${num_service == 1}"><c:set var="id_service" value="sm_airport" /></c:if>
                                                <c:if test="${num_service == 2}"><c:set var="id_service" value="sm_seaport" /></c:if>
                                                <c:if test="${num_service == 3}"><c:set var="id_service" value="sm_warehouse" /></c:if>
                                                <c:if test="${num_service == 4}"><c:set var="id_service" value="sm_manufacturing" /></c:if>
                                                <div class="service service-${num_service} col-sm-6 col-md-3" id="${id_service}">
                                                    <div class="sv-content">
                                                        <span class="number">${num_service}</span>
                                                        <div class="text">
                                                            <h4>${service.titleService}</h4>
                                                            <div class="desc">${service.contentService}</div>
                                                        </div>
                                                    </div>
                                                    <div class="sv-content-hover">
                                                        <c:if test="${num_service == 1 || num_service == 4}">
                                                            <span class="number">${num_service}</span>
                                                            <div class="text">
                                                                <h4>${service.titleService}</h4>
                                                                <div class="desc">${service.contentService}</div>
                                                                <div class="image">
                                                                    <img src="${pageContext.request.contextPath}/media/images/img_service/${service.imageService}" alt="${service.titleService}"/>
                                                                </div>
                                                            </div>
                                                        </c:if>
                                                        <c:if test="${num_service == 2 || num_service == 3}">
                                                            <div class="image">
                                                                <img src="${pageContext.request.contextPath}/media/images/img_service/${service.imageService}" alt="${service.titleService}"/>
                                                            </div>
                                                            <span class="number">${num_service}</span>
                                                            <div class="text">
                                                                <h4>${service.titleService}</h4>
                                                                <div class="desc">${service.contentService}</div>
                                                            </div>
                                                        </c:if>
                                                    </div>
                                                </div>
                                            <c:set var="num_service" value="${num_service + 1}" />
                                            </c:forEach>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <div class="wrap">
                                    <div class="right">
                                        <c:if test="${contactStatus == true}">
                                            <div class="line"></div>
                                            <a class="next-page" data-link="#firstSection/2" data-title="${contact_title}" href="#firstSection/2" style="display: block;">
                                                <div class="icon"></div>
                                            </a>
                                        </c:if>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </c:if>
                <c:if test="${contactStatus == true}">
                    <div class="horizontal-scrolling">
                        <div class="page">
                            <div class="header">
                                <div class="wrap">
                                    <div class="logo">
                                        <a href="/" title="ST Robo">
                                            <img src="${pageContext.request.contextPath}/images/logo.png">
                                        </a>
                                    </div>
                                    <div class="menu">
                                        <a class="menu-open" style="display: block;">
                                            <div class="icon"></div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="content contact-content">
                                <div class="contact">
                                    <div class="container">
                                        <div class="row">
                                            <div class="left-content col-sm-12 col-md-5">
                                                <h2>Contact Us</h2>
                                                <div class="contact-info">
                                                    <div class="email">
                                                        <p>${fn:toUpperCase(contacts.email)}</p>
                                                    </div>
                                                    <div class="tel">
                                                        ${fn:toUpperCase(contacts.phone)}
                                                    </div>
                                                    <div class="address">
                                                        ${fn:toUpperCase(contacts.address)}
                                                    </div>
                                                </div>
                                                <div class="follow-us">
                                                    <h2>Follow Us</h2>
                                                    <ul class="social">
                                                        <li>
                                                            <a href="${contacts.facebook}"><img src="${pageContext.request.contextPath}/images/fb.png" alt=""/></a>
                                                        </li>
                                                        <li>
                                                            <a href="${contacts.instagram}"><img width="21" src="${pageContext.request.contextPath}/images/ins.svg" alt=""/></a>
                                                        </li>
                                                        <li>
                                                            <a href="${contacts.twitter}"><img width="21" src="${pageContext.request.contextPath}/images/tw.svg" alt=""/></a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="right-content col-sm-12 col-md-7">
                                                <iframe style="width: 100%;height: calc(100% - 45px);" src="https://maps.google.com/maps?q=${contacts.googleApi}&amp;t=&amp;z=13&amp;ie=UTF8&amp;iwloc=&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer footer-contactpage">
                                <div class="wrap">
                                    <div class="site-info">
                                        <p class="mb-0">STROBO 2018 | All Copyrights Reserved</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </c:if>
            </section>
        </div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/2.6.6/jquery.fullPage.min.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $(window).on('load',function(){
//                     playVid();
                });
                var vid = document.getElementById("bkg-video");
                function playVid() {
                    vid.play();
                }
                $(".btn").click(function(){
                	playVid();
                })

                // Check url change title
                var url_check = window.location.href;
                checkActive(url_check);
                $("ul.m-menu li a").click(function () {
                    checkTitle($(this).attr('data-link'), $(this).attr('data-title'))
                });
                $(".next-page").click(function(){
                    checkTitle($(this).attr('data-link'), $(this).attr('data-title'))
                });
                function checkTitle(url_check, title_check){
                    if (url_check.includes("firstSection/1")){
                        document.title = title_check;
                        if (url_check.includes('airport'))
                        {
                            $('.service-1').addClass('s-active');
                        }
                        if (url_check.includes('seaport'))
                        {
                            $('.service-2').addClass('s-active');
                        }
                        if (url_check.includes('warehouse'))
                        {
                            $('.service-3').addClass('s-active');
                        }
                        if (url_check.includes('manufacturing'))
                        {
                            $('.service-4').addClass('s-active');
                            $('.footer .wrap .right').addClass('.hover_sevice4');
                        }
                    }
                    if (url_check.includes("firstSection/2")){
                        document.title = title_check;
                    }
                }
                function checkActive(url_check){
                    if (url_check.includes("firstSection/1")){
                        if (url_check.includes('airport'))
                        {
                            $('.service-1').addClass('s-active');
                            document.getElementById("m_airport").style.fontweight = 'bold';
                        }
                        if (url_check.includes('seaport'))
                        {
                            $('.service-2').addClass('s-active');
                            document.getElementById("m_seaport").style.fontweight = 'bold';
                        }
                        if (url_check.includes('warehouse'))
                        {
                            $('.service-3').addClass('s-active');
                            document.getElementById("m_warehouse").style.fontweight = 'bold';
                        }
                        if (url_check.includes('manufacturing'))
                        {
                            $('.service-4').addClass('s-active');
                            document.getElementById("m_manufacturing").style.fontweight = 'bold';
                        }
                    }
                    if (url_check.includes("firstSection/2")) {
                        document.getElementById("m_contact").style.fontweight = 'bold';
                    }
                }

                // variables
                var $header_top = $('.header-top');
                var $nav = $('nav');

                // toggle menu
                $header_top.find('a').on('click', function() {
                    $(this).parent().toggleClass('open-menu');
                });
                // fullpage customization
                $('#fullpage').fullpage({
                    sectionsColor: ['#B8AE9C', '#348899', '#F2AE72', '#5C832F', '#B8B89F'],
                    sectionSelector: '.vertical-scrolling',
                    slideSelector: '.horizontal-scrolling',
                    navigation: true,
                    slidesNavigation: true,
                    css3: true,
                    controlArrows: false,
                    anchors: ['firstSection', 'secondSection', 'thirdSection', 'fourthSection', 'fifthSection'],
                    menu: '#menu',

                    afterLoad: function(anchorLink, index) {
                        $header_top.css('background', 'rgba(0, 47, 77, .3)');
                        $nav.css('background', 'rgba(0, 47, 77, .25)');
                        if (index == 5) {
                            $('#fp-nav').hide();
                        }
                    },

                    onLeave: function(index, nextIndex, direction) {
                        if(index == 5) {
                            $('#fp-nav').show();
                        }
                    },

                    afterSlideLoad: function( anchorLink, index, slideAnchor, slideIndex) {
                        if(anchorLink == 'fifthSection' && slideIndex == 1) {
                            $.fn.fullpage.setAllowScrolling(false, 'up');
                            $header_top.css('background', 'transparent');
                            $nav.css('background', 'transparent');
                            $(this).css('background', '#374140');
                            $(this).find('h2').css('color', 'white');
                            $(this).find('h3').css('color', 'white');
                            $(this).find('p').css(
                                {
                                    'color': '#DC3522',
                                    'opacity': 1,
                                    'transform': 'translateY(0)'
                                }
                            );
                        }
                    },

                    onSlideLeave: function( anchorLink, index, slideIndex, direction) {
                        if(anchorLink == 'fifthSection' && slideIndex == 1) {
                            $.fn.fullpage.setAllowScrolling(true, 'up');
                            $header_top.css('background', 'rgba(0, 47, 77, .3)');
                            $nav.css('background', 'rgba(0, 47, 77, .25)');
                        }
                    }
                });
            });

        </script>
    </body>
</html>
