<%--
  Created by IntelliJ IDEA.
  User: My Kavalli
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="${pageContext.request.contextPath}/images/favicon.png" type="image/png" sizes="16x16">
    <meta name="keywords" content="${contact.keyword}">
    <meta name="description" content="${contact.meta}">
    <title>${contact.title}</title>
    <c:if test="${empty contact.title}">
        <title>Contact Us</title>
    </c:if>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/Meta_Serif.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/Lato.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/slick.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/fullpage.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/jquery.scrollbar.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/my.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/contact.css" />
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/mobile.css" />
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

    <script type="text/javascript" src="${pageContext.request.contextPath}/js/strobo/scrolloverflow.js"></script>
    <script src="${pageContext.request.contextPath}/js/strobo/scrolloverflow.js"></script>
    <script src="${pageContext.request.contextPath}/js/strobo/fullpage.js"></script>
    <script src="${pageContext.request.contextPath}/js/strobo/slick.js"></script>
    <script src="${pageContext.request.contextPath}/js/strobo/my.js"></script>

</head>
<body class="page">
    <div class="menu-body">
        <img src="${pageContext.request.contextPath}/assets/menu_black_close.png" class="close-menu">
        <div class="menu-content">
            <div class="content">
        <ul>
            <c:forEach items="${menuDefault}" var="menu">
                <c:if test="${menu.url == '#home'}">
                    <li><a class="menu-item" data-menu="home" href="${pageContext.request.contextPath}/">${menu.value}</a></li>
                </c:if>
                <c:if test="${menu.url == '#about'}">
                    <li><a class="menu-item" data-menu="about" href="${pageContext.request.contextPath}/#about-us">${menu.value}</a></li>
                </c:if>
                <c:if test="${menu.url == '#core'}">
                    <li><a class="menu-item" data-menu="core" href="${pageContext.request.contextPath}/technology">${menu.value}</a></li>
                </c:if>
            </c:forEach>
            <li><a  class="menu-item solutions" data-menu="solutions" href="${pageContext.request.contextPath}/services">Our Solutions</a>
                <ol class="submenu">
                    <c:forEach items="${menuDefault}" var="menu">
                        <c:if test="${menu.url == '#seaport'}">
                            <li><a class="sub-item" data-menu="seaport" href="${pageContext.request.contextPath}/services/seaport">${menu.value}</a></li>
                        </c:if>
                        <c:if test="${menu.url == '#manufacturing'}">
                            <li><a class="sub-item" data-menu="manufacturing" href="${pageContext.request.contextPath}/services/manufacturing">${menu.value}</a></li>
                        </c:if>
                        <c:if test="${menu.url == '#warehouse'}">
                            <li><a class="sub-item" data-menu="warehouse" href="${pageContext.request.contextPath}/services/warehouse">${menu.value}</a></li>
                        </c:if>
                        <c:if test="${menu.url == '#airport'}">
                            <li><a class="sub-item" data-menu="airport" href="${pageContext.request.contextPath}/services/airport">${menu.value}</a></li>
                        </c:if>
                    </c:forEach>
                </ol>
            </li>

            <c:forEach items="${menuNew}" var="menu">
                <c:if test="${menu.status == true}">
                    <li><a class="menu-item" href="${pageContext.request.contextPath}/${menu.url}">${fn:toUpperCase(menu.value)}</a></li>
                </c:if>
            </c:forEach>

            <c:forEach items="${menuDefault}" var="menu">
                <c:if test="${menu.url == '#contact'}">
                    <li><a class="menu-item active" data-menu="contact" href="${pageContext.request.contextPath}/contact">${menu.value}</a></li>
                </c:if>
            </c:forEach>
            <li><a class="menu-item" data-menu="contact" href="${pageContext.request.contextPath}/news">News</a></li>
        <li><img class="st-engineering" src="${pageContext.request.contextPath}/assets/ST_engineering.png"></li>
        </ul>
    </div>
    </div>
    </div>
    <div class="mobile-header">
        <img src="${pageContext.request.contextPath}/assets/logo.png" class="logo">
        <div class ="menucontact"><a href="#" class="menu"></a></div>
    </div>
    <div class="contact-page">
        <div class="contact-header header">
            <a href="${pageContext.request.contextPath}/#home" class="logo-back-home"><img class="logo" src="${pageContext.request.contextPath}/assets/logo.png"></a>
            <a href="#" class="menu"></a>
        </div>
        <div class="contact-content">
            <div class="content-left">
                <div class="content">
                    <h2>Contact Us</h2>
                    <p class="mail"><a href="mailto:${contact.email}">${contact.email}</a></p>
                    <p class="phone"><a href="tel:${contact.phone}">${contact.phone}</a></p>
                    <p class="address"><a target="_blank" href="https://maps.google.com/maps?q=${contact.googleApi}">${contact.address}</a></p>
                </div>
            </div>
            <div class="content-right">
                <iframe frameborder="0" src="https://maps.google.com/maps?q=${contact.googleApi}&t=&z=13&ie=UTF8&iwloc=&output=embed"></iframe>
            </div>
        </div>
        <div class="contact-footer footer">
            <p class="footer">COPYRIGHT @2018 ST ENGINEERING LAND SYSTEMS LTD. | ALL RIGHTS RESERVED</p>
        </div>
    </div>
</body>
</html>