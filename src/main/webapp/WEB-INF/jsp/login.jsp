<%--
  Created by IntelliJ IDEA.
  User: My Kavalli
  Date: 06/09/2018
  Time: 09:45 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Admin</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/node_modules/mdi/css/materialdesignicons.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/node_modules/perfect-scrollbar/dist/css/perfect-scrollbar.min.css">
    <!-- endinject -->
    <!-- inject:css -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/fonts.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/login.css">
    <!-- endinject -->
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/images/favicon.png" />
    <script src="${pageContext.request.contextPath}/node_modules/jquery/dist/jquery.min.js"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script>
        function recaptchaCallback()
        {
            $('#btnLogin').removeAttr('disabled');
            document.getElementById('btnLogin').type = 'submit';
        };
    </script>
</head>

<body style="font-size: 18px;">
<div class="container-scroller">
    <div class="container-fluid page-body-wrapper">
        <div class="row">
            <div class="content-wrapper full-page-wrapper d-flex align-items-center auth-pages">
                <div class="card col-lg-4 mx-auto">
                    <div class="card-body px-5 py-5">
                        <div class="text-center mb-4"><img src="${pageContext.request.contextPath}/images/logo.png"></div>
                        <h3 class="card-title text-left mb-3 text-center">ADMIN LOGIN</h3>
                        <form method="post" validate action="/login">
                            <div class="form-group">
                                <label>Username</label>
                                <input name="username" placeholder="Username" type="text" class="form-control p_input" required="required">
                            </div>
                            <div class="form-group">
                                <label>Password</label>
                                <input name="password" placeholder="Password" type="password" class="form-control p_input" required="required">
                            </div>
                            <c:if test="${status}">
							<div class="g-recaptcha" data-callback="recaptchaCallback" data-sitekey="${site_key}"></div>
							<div class="text-center">
								<button id="btnLogin" type="button" class="btn btn-primary btn-block enter-btn" disabled="disabled">Login</button>
							</div>
							</c:if>
							
							<c:if test="${!status}">
							<div class="text-center">
								<button id="btnLogin" type="submit" class="btn btn-primary btn-block enter-btn">Login</button>
							</div>
							</c:if>
							</form>
                    </div>
                </div>
            </div>
            <!-- content-wrapper ends -->
        </div>
        <!-- row ends -->
    </div>
    <!-- page-body-wrapper ends -->
</div>
<script type="text/javascript">
</script>
<!-- container-scroller -->
<!-- plugins:js -->
<script src="${pageContext.request.contextPath}/node_modules/popper.js/dist/umd/popper.min.js"></script>
<script src="${pageContext.request.contextPath}/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/node_modules/perfect-scrollbar/dist/js/perfect-scrollbar.jquery.min.js"></script>
<!-- endinject -->
<!-- inject:js -->
<script src="${pageContext.request.contextPath}/js/off-canvas.js"></script>
<script src="${pageContext.request.contextPath}/js/hoverable-collapse.js"></script>
<script src="${pageContext.request.contextPath}/js/misc.js"></script>
<!-- endinject -->
</body>

</html>
