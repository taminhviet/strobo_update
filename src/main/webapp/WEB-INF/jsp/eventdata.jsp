<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Event</title>
</head>
<body>

	<table>
		<thead>
			<tr>
				<th>Title</th>
				<th>Content</th>
				<th>Date</th>
				<th>MainImg</th>
				<th>Gallery</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${listEvent}" var="event">
				<tr>
					<td>${event.title}</td>
					<td>${event.content}</td>
					<td>${event.date}</td>
					<td>${event.mainImage}</td>
					<td>${event.gallery}</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</body>
</html>