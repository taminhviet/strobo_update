<%--
  Created by IntelliJ IDEA.
  User: My Kavalli
  Date: 19/09/2018
  Time: 09:44 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Invalid</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/node_modules/mdi/css/materialdesignicons.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/node_modules/perfect-scrollbar/dist/css/perfect-scrollbar.min.css">
    <!-- endinject -->
    <!-- plugin css for this page -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/node_modules/icheck/skins/all.css" />
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/custom.css">
    <!-- endinject -->
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/images/favicon.png" />
</head>
<body>
<div class="http-error">
    <div class="container">
        <img src="${pageContext.request.contextPath}/images/logo.png">
        <h1 class="text-warning mb-4 mt-4">Failure</h1>
        <h3 class="message-text text-danger mb-4">Invalid username/password! Please try again.</h3>
        <button type="button" class="btn btn-primary" onclick="history.back();">Login</button>
    </div>
</div>
</body>
</html>
