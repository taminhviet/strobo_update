<%--
  Created by IntelliJ IDEA.
  User: MyKavalli
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="${pageContext.request.contextPath}/images/favicon.png" type="image/png" sizes="16x16">
    <title>Warehouse</title>

    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/Meta_Serif.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/Lato.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/slick.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/fullpage.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/jquery.scrollbar.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/owl.carousel.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/my.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/warehouse.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/st_general.css" />
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

    <script type="text/javascript" src="${pageContext.request.contextPath}/js/strobo/scrolloverflow.js"></script>
    <script src="${pageContext.request.contextPath}/js/strobo/scrolloverflow.js"></script>
    <script src="${pageContext.request.contextPath}/js/strobo/fullpage.js"></script>
    <script src="${pageContext.request.contextPath}/js/strobo/slick.js"></script>
    <script src="${pageContext.request.contextPath}/js/strobo/owl.carousel.js"></script>
    <script src="${pageContext.request.contextPath}/js/strobo/my.js"></script>
</head>
<body class="body-item warehouse">
    <div class="menu-body">
        <img src="${pageContext.request.contextPath}/assets/menu_black_close.png" class="close-menu">
        <div class="menu-content">
            <div class="content">
        <ul>
            <c:forEach items="${menuDefault}" var="menu">
                <c:if test="${menu.url == '#home'}">
                    <li><a class="menu-item" data-menu="home" href="${pageContext.request.contextPath}/">${menu.value}</a></li>
                </c:if>
                <c:if test="${menu.url == '#about'}">
                    <li><a class="menu-item" data-menu="about" href="${pageContext.request.contextPath}/#about-us">${menu.value}</a></li>
                </c:if>
                <c:if test="${menu.url == '#core'}">
                    <li><a class="menu-item" data-menu="core" href="${pageContext.request.contextPath}/technology">${menu.value}</a></li>
                </c:if>
            </c:forEach>
            <li><a  class="menu-item solutions" data-menu="solutions" href="${pageContext.request.contextPath}/services">Our Solutions</a>
                <ol class="submenu">
                    <c:forEach items="${menuDefault}" var="menu">
                        <c:if test="${menu.url == '#seaport'}">
                            <li><a class="sub-item" data-menu="seaport" href="${pageContext.request.contextPath}/services/seaport">${menu.value}</a></li>
                        </c:if>
                        <c:if test="${menu.url == '#manufacturing'}">
                            <li><a class="sub-item" data-menu="manufacturing" href="${pageContext.request.contextPath}/services/manufacturing">${menu.value}</a></li>
                        </c:if>
                        <c:if test="${menu.url == '#warehouse'}">
                            <li><a class="sub-item active" data-menu="warehouse" href="${pageContext.request.contextPath}/services/warehouse">${menu.value}</a></li>
                        </c:if>
                        <c:if test="${menu.url == '#airport'}">
                            <li><a class="sub-item" data-menu="airport" href="${pageContext.request.contextPath}/services/airport">${menu.value}</a></li>
                        </c:if>
                    </c:forEach>
                </ol>
            </li>

            <c:forEach items="${menuNew}" var="menu">
                <c:if test="${menu.status == true}">
                    <li><a class="menu-item" href="${pageContext.request.contextPath}/${menu.url}">${fn:toUpperCase(menu.value)}</a></li>
                </c:if>
            </c:forEach>

            <c:forEach items="${menuDefault}" var="menu">
                <c:if test="${menu.url == '#contact'}">
                    <li><a class="menu-item" data-menu="contact" href="${pageContext.request.contextPath}/contact">${menu.value}</a></li>
                </c:if>
            </c:forEach>
            <li><a class="menu-item" data-menu="contact" href="${pageContext.request.contextPath}/news">News</a></li>
            <li><img class="st-engineering" src="${pageContext.request.contextPath}/assets/ST_engineering.png"></li>
        </ul>
    </div>
    </div>
    </div>
    <div class="body-content warehouse">
        <div class="item-content active">
            <div class="all-item warehouse active">
                <div class="warehouse-header header">
                    <a href="${pageContext.request.contextPath}/" class="logo-back-home"><img class="logo" src="${pageContext.request.contextPath}/assets/logo.png"></a>
                    <c:if test="${not empty pagewarehouse.pageContent}">
                        <p>${pagewarehouse.pageContent}</p> 
                    </c:if>
                    <c:if test="${empty pagewarehouse.pageContent}">
                        <p>Fully autonomous turnkey intra-logistics solutions to facilitate seamless operations for warehouses across all market sectors.</p>
                    </c:if>
                    <a href="#" class="menu"></a>
                </div>
                <div class="warehouse-content content">
                    <a href="${pageContext.request.contextPath}/services/manufacturing" class="prev-detail-service"></a>
                    <c:if test="${ not empty pagewarehouse.pageName}">
                        <img class="bg" src="${pageContext.request.contextPath}/media/images/${pagewarehouse.pageName}.png">
                    </c:if>
                    <c:if test="${ empty pagewarehouse.pageName}">
                        <img src="assets/all_warehouse.png" alt="" class="bg">
                    </c:if>
                    <div class="warehouse_1">
                        <div class="description">
                            <c:if test="${not empty pagewarehouse1.content}">
                                <p type-for="warehouse" data-img="warehouse_1" class="right">${pagewarehouse1.content}</p>
                            </c:if>
                            <c:if test="${ empty pagewarehouse1.content}">
                                <p class="right">Warehouse Management System</p>
                            </c:if>
                        </div>
                        <div class="image">
                            <div class="table-cell">
                                <c:if test="${ not empty pagewarehouse1.pageName}">
                                    <img src="${pageContext.request.contextPath}/media/images/${pagewarehouse1.pageName}.png" type-for="warehouse" data-img="warehouse_1">
                                </c:if>
                                <c:if test="${ empty pagewarehouse1.pageName}">
                                    <img src="${pageContext.request.contextPath}/assets/warehouse_1.png" data-img="warehouse_1">
                                </c:if>
                            </div>
                        </div>
                    </div>
                    <div class="warehouse_2">
                        <div class="description">
                            <c:if test="${not empty pagewarehouse2.content}">
                                <p type-for="warehouse" data-img="warehouse_2" class="right">${pagewarehouse2.content}</p>
                            </c:if>
                            <c:if test="${ empty pagewarehouse2.content}">
                                <p class="right">Autonomous Pallet Truck</p>
                            </c:if>
                        </div>
                        <div class="image">
                            <div class="table-cell">
                                <c:if test="${ not empty pagewarehouse2.pageName}">
                                    <img src="${pageContext.request.contextPath}/media/images/${pagewarehouse2.pageName}.png" type-for="warehouse" data-img="warehouse_2">
                                </c:if>
                                <c:if test="${ empty pagewarehouse2.pageName}">
                                    <img src="${pageContext.request.contextPath}/assets/warehouse_2.png" data-img="warehouse_2">
                                </c:if>
                            </div>
                        </div>
                    </div>
                    <div class="warehouse_3">
                        <div class="description">
                            <c:if test="${not empty pagewarehouse3.content}">
                                <p type-for="warehouse" data-img="warehouse_3" class="right">${pagewarehouse3.content}</p>
                            </c:if>
                            <c:if test="${ empty pagewarehouse3.content}">
                                <p class="right">Autonomous Forklift</p>
                            </c:if>
                        </div>
                        <div class="image">
                            <div class="table-cell">
                                <c:if test="${ not empty pagewarehouse3.pageName}">
                                    <img src="${pageContext.request.contextPath}/media/images/${pagewarehouse3.pageName}.png" type-for="warehouse" data-img="warehouse_3">
                                </c:if>
                                <c:if test="${ empty pagewarehouse3.pageName}">
                                    <img src="${pageContext.request.contextPath}/assets/warehouse_3.png" data-img="warehouse_3">
                                </c:if>                 
                            </div>                   
                        </div>
                    </div>
                    <div class="warehouse_4">
                        <div class="image">
                            <div class="table-cell">
                                <c:if test="${ not empty pagewarehouse4.pageName}">
                                    <img src="${pageContext.request.contextPath}/media/images/${pagewarehouse4.pageName}.png" type-for="warehouse" data-img="warehouse_4">
                                </c:if>
                                <c:if test="${ empty pagewarehouse4.pageName}">
                                    <img src="${pageContext.request.contextPath}/assets/warehouse_4.png" data-img="warehouse_4">
                                </c:if>
                            </div>
                        </div>
                        <div class="description">
                            <c:if test="${not empty pagewarehouse4.content}">
                                <p type-for="warehouse" data-img="warehouse_4">${pagewarehouse4.content}</p>
                            </c:if>
                            <c:if test="${ empty pagewarehouse4.content}">
                                <p>Autonomous VNA Truck</p>
                            </c:if>
                        </div>
                    </div>
                    <div class="warehouse_5">
                        <div class="image">
                            <div class="table-cell">
                                <c:if test="${ not empty pagewarehouse5.pageName}">
                                    <img src="${pageContext.request.contextPath}/media/images/${pagewarehouse5.pageName}.png" type-for="warehouse" data-img="warehouse_5">
                                </c:if>
                                <c:if test="${ empty pagewarehouse5.pageName}">
                                    <img src="${pageContext.request.contextPath}/assets/warehouse_5.png" data-img="warehouse_5">
                                </c:if>
                            </div>
                        </div>
                        <div class="description">
                            <c:if test="${not empty pagewarehouse5.content}">
                                <p type-for="warehouse" data-img="warehouse_5">${pagewarehouse5.content}</p>
                            </c:if>
                            <c:if test="${ empty pagewarehouse5.content}">
                                <p>Autonomous Tow Tractor</p>
                            </c:if>
                        </div>
                    </div>
                    <div class="warehouse_6">
                        <div class="image">
                            <div class="table-cell">
                                <c:if test="${ not empty pagewarehouse6.pageName}">
                                    <img src="${pageContext.request.contextPath}/media/images/${pagewarehouse6.pageName}.png" type-for="warehouse" data-img="warehouse_6">
                                </c:if>
                                <c:if test="${ empty pagewarehouse6.pageName}">
                                    <img src="${pageContext.request.contextPath}/assets/warehouse_6.png" data-img="warehouse_6">
                                </c:if>
                            </div>
                        </div>
                        <div class="description">
                            <c:if test="${not empty pagewarehouse6.content}">
                                <p type-for="warehouse" data-img="warehouse_6">${pagewarehouse6.content}</p>
                            </c:if>
                            <c:if test="${ empty pagewarehouse6.content}">
                                <p>Autonomous Pallet Stacker</p>
                            </c:if>
                        </div>
                    </div>
                    <a href="${pageContext.request.contextPath}/services/airport" class="next-detail-service"></a>
                </div>
                <div class="all-footer footer">
                    <span class="first-detail" goto-for="warehouse_1" ></span>
                    <h2>Warehouse Solutions</h2>
                    <p class="go-rms">Robotics Management System</p>
                </div>
            </div>
        </div>
        <div class="item-details transition-down-hide">

            <!-- WAREHOUSE -->
            <div class="for-warehouse for">
                <div da-slick="warehouse_1" class="item warehouse_1">
                    <div class="detail-header header">
                        <a href="${pageContext.request.contextPath}/" class="logo-back-home"><img class="logo" src="${pageContext.request.contextPath}/assets/logo.png"></a>
                        <a href="#" class="menu"></a>
                    </div>
                    <div class="detail-content">
                        <span class="pre-detail" type-move="warehouse"></span>
                        <div class="view">
                            <div class="content-left">
                                <div>
                                    <div class="for-main">
                                        <c:if test="${fn:length(pagewarehouse1.listSlide) > 1}">
                                            <span class="prev-item" data-slide="warehouse_1"></span>
                                        </c:if>
                                        <c:if test="${not empty pagewarehouse1.listSlide}">
                                            <c:set var="slide31" value="1"/>
                                            <c:forEach items="${pagewarehouse1.listSlide}" var="imgSlide31">
                                                <c:if test="${slide31 == 1}">
                                                    <img class="service-img main warehouse_1" src="${pageContext.request.contextPath}/media/images/${imgSlide31.slideimg}">
                                                </c:if>
                                            <c:set var="slide31" value="${slide31 + 1}"/>
                                            </c:forEach>
                                        </c:if>
                                        <c:if test="${fn:length(pagewarehouse1.listSlide) > 1}">
                                            <span class="next-item" data-slide="warehouse_1"></span>
                                        </c:if>
                                    </div>
                                    <c:if test="${fn:length(pagewarehouse1.listSlide) > 1}">
                                        <div class="body-thumbnail">
                                            <div class="thumbnail owl-carousel">
                                                <c:set var="n31" value="1"/>
                                                <c:forEach items="${pagewarehouse1.listSlide}" var="imgSlide">
                                                    <c:if test="${not empty imgSlide}" >
                                                        <c:if test="${n31 == 1}">
                                                            <c:set var="addActive31" value="active"/>
                                                        </c:if>
                                                        <div class="around">
                                                            <img data-img="${n31}" type-click="warehouse_1" class="img-thumbnail img-${n31} ${addActive31} warehouse_1" src="${pageContext.request.contextPath}/media/images/${imgSlide.slideimg}">
                                                        </div>
                                                        <c:set var="n31" value="${n31 + 1}"/>
                                                    </c:if>
                                                </c:forEach>
                                            </div>
                                        </div>
                                    </c:if>
                                </div>
                            </div>
                            <div class="content-right">
                                <c:if test="${not empty pagewarehouse1.content}">
                                    <h2>${pagewarehouse1.content}</h2>
                                </c:if>
                                <c:if test="${ empty pagewarehouse1.content}">
                                    <h2>Warehouse Management System</h2>
                                </c:if>
                                <c:if test="${not empty pagewarehouse1.detailContent}">
                                    <h5>${pagewarehouse1.detailContent}</h5>
                                </c:if>
                                <c:if test="${ empty pagewarehouse1.detailContent}">
                                    <h5>STROBO’s highly customisable Warehouse Management System (WMS) is able to interface with existing enterprise resource planning systems and various automation equipment at the warehouse control/robotics management system supervisory level. It caters to a full range of warehouse applications - from receiving, putaway, picking and replenishment to value-added services and dispatch - allowing for greater inventory control and system optimisation.</h5>
                                </c:if>
                            </div>
                            <c:if test="${not empty pagewarehouse1.brochure}">
                                <div class="download">
                                    <a href="/brochure/${pagewarehouse1.brochure}">Download brochure</a>
                                </div>
                            </c:if>
                        </div>
                        <span class="next-detail" type-move="warehouse"></span>
                    </div>
                    <div class="detail-footer footer">
                        <span class="back detail-back" num-video="warehouse_1" type-back="warehouse"></span>
                    </div>
                </div>
                <div da-slick="warehouse_2" class="item warehouse_2">
                    <div class="detail-header header">
                        <a href="${pageContext.request.contextPath}/" class="logo-back-home"><img class="logo" src="${pageContext.request.contextPath}/assets/logo.png"></a>
                        <a href="#" class="menu"></a>
                    </div>
                    <div class="detail-content">
                        <span class="pre-detail" type-move="warehouse"></span>
                        <div class="view">
                            <div class="content-left">
                                <div>
                                    <div class="for-main">
                                        <c:if test="${fn:length(pagewarehouse2.listSlide) > 1}">
                                            <span class="prev-item" data-slide="warehouse_2"></span>
                                        </c:if>
                                        <c:if test="${not empty pagewarehouse2.listSlide}">
                                            <c:set var="slide32" value="1"/>
                                            <c:forEach items="${pagewarehouse2.listSlide}" var="imgSlide32">
                                                <c:if test="${slide32 == 1}">
                                                    <img class="service-img main warehouse_2" src="${pageContext.request.contextPath}/media/images/${imgSlide32.slideimg}">
                                                </c:if>
                                            <c:set var="slide32" value="${slide32 + 1}"/>
                                            </c:forEach>
                                        </c:if>
                                        <c:if test="${fn:length(pagewarehouse2.listSlide) > 1}">
                                            <span class="next-item" data-slide="warehouse_2"></span>
                                        </c:if>
                                    </div>
                                    <c:if test="${fn:length(pagewarehouse2.listSlide) > 1}">
                                        <div class="body-thumbnail">
                                            <div class="thumbnail owl-carousel">
                                                <c:set var="n32" value="1"/>
                                                <c:forEach items="${pagewarehouse2.listSlide}" var="imgSlide">
                                                    <c:if test="${n32 == 1}">
                                                        <c:set var="addActive32" value="active"/>
                                                    </c:if>
                                                    <div class="around">
                                                        <img data-img="${n32}" type-click="warehouse_2" class="img-thumbnail img-${n32} ${addActive32} warehouse_2" src="${pageContext.request.contextPath}/media/images/${imgSlide.slideimg}">
                                                    </div>
                                                    <c:set var="n32" value="${n32 + 1}"/>
                                                </c:forEach>
                                            </div>
                                        </div>
                                    </c:if>
                                </div>
                            </div>
                            <div class="content-right">
                                <c:if test="${not empty pagewarehouse2.content}">
                                    <h2>${pagewarehouse2.content}</h2>
                                </c:if>
                                <c:if test="${ empty pagewarehouse2.content}">
                                    <h2>Autonomous Pallet Truck</h2>
                                </c:if>
                                <p>KEY FEATURES</p>
                                <c:if test="${not empty pagewarehouse2.detailContent}">
                                    ${pagewarehouse2.detailContent}
                                </c:if>
                                <c:if test="${ empty pagewarehouse2.detailContent}">
                                    <ul>
                                        <li>Suitable for indoor and outdoor operations</li>
                                        <li>Collision detection and avoidance</li>
                                        <li>Advanced pallet detection and recognition capability</li>
                                        <li>Interfaces with Warehouse Management</li>
                                        <li>System for seamless operation</li>
                                    </ul>
                                </c:if>
                                <c:if test="${not empty pagewarehouse2.brochure}">
                                    <div class="download">
                                        <a href="/brochure/${pagewarehouse2.brochure}">Download brochure</a>
                                    </div>
                                </c:if>
                                <div class="watch-video"><a href="#" type-video="warehouse_2">Watch video</a> <a class="tech-spec-warehouse_2" href="#">View Tech Specs</a> </div>
                            </div>
                        </div>
                            <div class="video">
                                <video id="player_warehouse_2" class="video-warehouse_2" controls width="100%">
                                    <source src="${pageContext.request.contextPath}/media/video/warehouse/warehouse.mp4" type="video/mp4">
                                </video>
                                <div class="play-button"><span></span></div>
                                <p class="play-video" >Watch video</p>
                            </div>
                        <span class="next-detail" type-move="warehouse"></span>
                    </div>
                    <div class ="spec-warehouse_2">
                        <h4>Autonomous Pallet Truck <a class ="back-warehouse2" href="#">Back</a> </h4> 
                        <table class="table-bordered">
                            <tbody>
                                <tr>
                                    <th colspan="3" class ="center header">TECHNICAL SPECIFICATIONS</th>
                                </tr>
                                <tr>
                                    <td>Payload</td>
                                    <td class ="center">kg</td>
                                    <td class ="center">2000</td>
                                </tr>
                                <tr>
                                    <td>Service Weight (Including Battery)</td>
                                    <td class ="center">kg</td>
                                    <td class ="center">500</td>
                                </tr>
                                <tr>
                                    <td>Dimensions(LxWxH)</td>
                                    <td class ="center">mm</td>
                                    <td class ="center">1890 x 750 x 1900</td>
                                </tr>
                                <tr>
                                    <td>Fork Size(LxWxH)</td>
                                    <td class ="center">mm</td>
                                    <td class ="center">1125 x 180 x 50</td>
                                </tr>
                                <tr class ="colors">
                                    <td>Fork Spread (Width over forks)</td>
                                    <td class ="center">mm</td>
                                    <td class ="center">690</td>
                                </tr>
                                <tr class ="colors">
                                    <td>Fork Lowered Height</td>
                                    <td class ="center">mm</td>
                                    <td class ="center">85</td>
                                </tr>
                                <tr class ="colors">
                                    <td>Fork Lifted Height</td>
                                    <td class ="center">mm</td>
                                    <td class ="center">195</td>
                                </tr>
                                <tr>
                                    <td>Walking Speed</td>
                                    <td class ="center">kph</td>
                                    <td class ="center">6</td>
                                </tr>
                                <tr>
                                    <td>Gradaliblity</th>
                                    <td class ="center">%</td>
                                    <td class ="center">8</td>
                                </tr>
                                <tr>
                                    <td>Turning Radius</td>
                                    <td class ="center">mm</td>
                                    <td class ="center">1650</td>
                                </tr>
                                <tr>
                                    <td>Operating Noise Level</td>
                                    <td class ="center">dBA</td>
                                    <td class ="center">75</td>
                                </tr>
                                <tr class ="colors">
                                    <td>Motor Controller</td>
                                    <td class ="center">Type</td>
                                    <td class ="center">Low Voltage AC</td>
                                </tr>
                                <tr class ="colors">
                                    <td>Voltage</td>
                                    <td class ="center">V</td>
                                    <td class ="center">24</td>
                                </tr>
                                <tr class ="colors">
                                    <td>Battery Capacity</td>
                                    <td class ="center">VAH</td>
                                    <td class ="center">110</td>
                                </tr>
                                <tr class ="colors">
                                    <td>Change Time (24V30A Charger)</td>
                                    <td class ="center">Hr</td>
                                    <td class ="center">4</td>
                                </tr>
                                <tr class ="colors">
                                    <td>Change Time (24V90A Charger)</td>
                                    <td class ="center">Hr</td>
                                    <td class ="center">1.5</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="detail-footer footer">
                        <span class="back detail-back" num-video="warehouse_2" type-back="warehouse"></span>
                        <span class="scroll-down">scroll down</span>
                    </div>
                </div>
                <div da-slick="warehouse_3" class="item warehouse_3">
                    <div class="detail-header header">
                        <a href="${pageContext.request.contextPath}/" class="logo-back-home"><img class="logo" src="${pageContext.request.contextPath}/assets/logo.png"></a>
                        <a href="#" class="menu"></a>
                    </div>
                    <div class="detail-content">
                        <span class="pre-detail" type-move="warehouse"></span>
                        <div class="view">
                            <div class="content-left">
                                <div>
                                    <div class="for-main">
                                        <c:if test="${fn:length(pagewarehouse3.listSlide) > 1}">
                                            <span class="prev-item" data-slide="warehouse_3"></span>
                                        </c:if>
                                        <c:if test="${not empty pagewarehouse3.listSlide}">
                                            <c:set var="slide33" value="1"/>
                                            <c:forEach items="${pagewarehouse3.listSlide}" var="imgSlide33">
                                                <c:if test="${slide33 == 1}">
                                                    <img class="service-img main warehouse_3" src="${pageContext.request.contextPath}/media/images/${imgSlide33.slideimg}">
                                                </c:if>
                                            <c:set var="slide33" value="${slide33 + 1}"/>
                                            </c:forEach>
                                        </c:if>
                                        <c:if test="${fn:length(pagewarehouse3.listSlide) > 1}">
                                            <span class="next-item" data-slide="warehouse_3"></span>
                                        </c:if>
                                    </div>
                                    <c:if test="${fn:length(pagewarehouse3.listSlide) > 1}">
                                        <div class="body-thumbnail">
                                            <div class="thumbnail owl-carousel">
                                                <c:set var="n33" value="1"/>
                                                <c:forEach items="${pagewarehouse3.listSlide}" var="imgSlide">
                                                    <c:if test="${not empty imgSlide}" >
                                                        <c:if test="${n33 == 1}">
                                                            <c:set var="addActive33" value="active"/>
                                                        </c:if>
                                                        <div class="around">
                                                            <img data-img="${n33}" type-click="warehouse_3" class="img-thumbnail img-${n33} ${addActive33} warehouse_3" src="${pageContext.request.contextPath}/media/images/${imgSlide.slideimg}">
                                                        </div>
                                                        <c:set var="n33" value="${n33 + 1}"/>
                                                    </c:if>
                                                </c:forEach>
                                            </div>
                                        </div>
                                    </c:if>
                                </div>
                            </div>
                            <div class="content-right">
                                <c:if test="${not empty pagewarehouse3.content}">
                                    <h2>${pagewarehouse3.content}</h2>
                                </c:if>
                                <c:if test="${ empty pagewarehouse3.content}">
                                    <h2>Autonomous Forklift</h2>
                                </c:if>
                                <p>KEY FEATURES</p>
                                <c:if test="${not empty pagewarehouse3.detailContent}">
                                    ${pagewarehouse3.detailContent}
                                </c:if>
                                <c:if test="${ empty pagewarehouse3.detailContent}">
                                    <ul>
                                        <li>Suitable for both indoor and outdoor operations, across different terrains Move and stack goods on demand, or pre-schedule in real-time</li>
                                        <li>Interfaces with Warehouse Management</li>
                                        <li>System for seamless operation</li>
                                    </ul>
                                </c:if>
                                <c:if test="${not empty pagewarehouse3.brochure}">
                                    <div class="download">
                                        <a href="/brochure/${pagewarehouse3.brochure}">Download brochure</a>
                                    </div>
                                </c:if>
                                <div class="watch-video"><a href="#" type-video="warehouse_3">Watch video</a> <a class="tech-spec-warehouse_3" href="#">View Tech Specs</a></div>
                            </div>
                        </div>
                            <div class="video">
                                <video id="player_warehouse_3" class="video-warehouse_2" controls width="100%">
                                    <source src="${pageContext.request.contextPath}/media/video/warehouse/warehouse.mp4" type="video/mp4">
                                </video>
                                <div class="play-button"><span></span></div>
                                <p class="play-video" >Watch video</p>
                            </div>
                        <span class="next-detail" type-move="warehouse"></span>
                    </div>
                    <div class ="spec-warehouse_3">
                        <h4>Autonomous Forklift <a class ="back-warehouse3" href="#">Back</a></h4>
                        <table class="table-bordered">
                            <tbody>
                                <tr>
                                    <th colspan="3" class ="center header">TECHNICAL SPECIFICATIONS</th>
                                </tr>
                                <tr>
                                    <td>Payload</td>
                                    <td class ="center">kg</td>
                                    <td class ="center">2000</td>
                                </tr>
                                <tr>
                                    <td>Service Weight (Including Battery)</td>
                                    <td class ="center">kg</td>
                                    <td class ="center">3620</td>
                                </tr>
                                <tr>
                                    <td>Dimensions(LxWxH)</td>
                                    <td class ="center">mm</td>
                                    <td class ="center">3500 x 1220 x 2195</td>
                                </tr>
                                <tr>
                                    <td>Fork Size(LxWxH)</td>
                                    <td class ="center">mm</td>
                                    <td class ="center">1070 x 100 x 40</td>
                                </tr>
                                <tr class ="colors">
                                    <td>Fork Spread (Width over forks)</td>
                                    <td class ="center">mm</td>
                                    <td class ="center">Movable</td>
                                </tr>
                                <tr class ="colors">
                                    <td>Fork Lowered Height</td>
                                    <td class ="center">mm</td>
                                    <td class ="center">50</td>
                                </tr>
                                <tr class ="colors">
                                    <td>Fork Lifted Height</td>
                                    <td class ="center">mm</td>
                                    <td class ="center">3000</td>
                                </tr>
                                <tr>
                                    <td>Walking Speed</td>
                                    <td class ="center">kph</td>
                                    <td class ="center">15</td>
                                </tr>
                                <tr>
                                    <td>Gradaliblity</td>
                                    <td class ="center">%</td>
                                    <td class ="center">15</td>
                                </tr>
                                <tr>
                                    <td>Turning Radius</td>
                                    <td class ="center">mm</td>
                                    <td class ="center">2020</td>
                                </tr>
                                <tr>
                                    <td>Operating Noise Level</td>
                                    <td class ="center">dBA</td>
                                    <td class ="center">75</td>
                                </tr>
                                <tr class ="colors">
                                    <td>Motor Controller</td>
                                    <td class ="center">Type</td>
                                    <td class ="center">Low Voltage AC</td>
                                </tr>
                                <tr class ="colors">
                                    <td>Voltage</td>
                                    <td class ="center">V</td>
                                    <td class ="center">80</td>
                                </tr>
                                <tr class ="colors">
                                    <td>Battery Capacity</td>
                                    <td class ="center">VAH</td>
                                    <td class ="center">270</td>
                                </tr>
                                <tr class ="colors">
                                    <td>Change Time (24V30A Charger)</td>
                                    <td class ="center">Hr</td>
                                    <td class ="center">8</td>
                                </tr>
                                <tr class ="colors">
                                    <td>Change Time (24V90A Charger)</td>
                                    <td class ="center">Hr</td>
                                    <td class ="center">3</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="detail-footer footer">
                        <span class="back detail-back" num-video="warehouse_3" type-back="warehouse"></span>
                        <span class="scroll-down">scroll down</span>
                    </div>
                </div>
                <div da-slick="warehouse_4" class="item warehouse_4">
                    <div class="detail-header header">
                        <a href="${pageContext.request.contextPath}/" class="logo-back-home"><img class="logo" src="${pageContext.request.contextPath}/assets/logo.png"></a>
                        <a href="#" class="menu"></a>
                    </div>
                    <div class="detail-content">
                        <span class="pre-detail" type-move="warehouse"></span>
                        <div class="view">
                            <div class="content-left">
                                <div>
                                    <div class="for-main">
                                        <c:if test="${fn:length(pagewarehouse4.listSlide) > 1}">
                                            <span class="prev-item" data-slide="warehouse_4"></span>
                                        </c:if>
                                        <c:if test="${not empty pagewarehouse4.listSlide}">
                                            <c:set var="slide34" value="1"/>
                                            <c:forEach items="${pagewarehouse4.listSlide}" var="imgSlide34">
                                                <c:if test="${slide34 == 1}">
                                                    <img class="service-img main warehouse_4" src="${pageContext.request.contextPath}/media/images/${imgSlide34.slideimg}">
                                                </c:if>
                                            <c:set var="slide34" value="${slide34 + 1}"/>
                                            </c:forEach>
                                        </c:if>
                                        <c:if test="${fn:length(pagewarehouse4.listSlide) > 1}">
                                            <span class="next-item" data-slide="warehouse_4"></span>
                                        </c:if>
                                    </div>
                                    <c:if test="${fn:length(pagewarehouse4.listSlide) > 1}">
                                        <div class="body-thumbnail">
                                            <div class="thumbnail owl-carousel">
                                                <c:set var="n34" value="1"/>
                                                <c:forEach items="${pagewarehouse4.listSlide}" var="imgSlide">
                                                    <c:if test="${not empty imgSlide}" >
                                                        <c:if test="${n34 == 1}">
                                                            <c:set var="addActive34" value="active"/>
                                                        </c:if>
                                                        <div class="around">
                                                            <img data-img="${n34}" type-click="warehouse_4" class="img-thumbnail img-${n34} ${addActive34} warehouse_4" src="${pageContext.request.contextPath}/media/images/${imgSlide.slideimg}">
                                                        </div>
                                                        <c:set var="n34" value="${n34 + 1}"/>
                                                    </c:if>
                                                </c:forEach>
                                            </div>
                                        </div>
                                    </c:if>
                                </div>
                            </div>
                            <div class="content-right">
                                <c:if test="${not empty pagewarehouse4.content}">
                                    <h2>${pagewarehouse4.content}</h2>
                                </c:if>
                                <c:if test="${ empty pagewarehouse4.content}">
                                    <h2>Autonomous VNA Truck</h2>
                                </c:if>
                                <p>KEY FEATURES</p>
                                <c:if test="${not empty pagewarehouse4.detailContent}">
                                    ${pagewarehouse4.detailContent}
                                </c:if>
                                <c:if test="${ empty pagewarehouse4.content}">
                                    <ul>
                                        <li>Advanced lifting system that autonomously picks and stacks at heights of up to 14m</li>
                                        <li>Able to operate in tight spaces</li>
                                        <li>Automatic alignment into aisle via laser SLAM navigation</li>
                                        <li>Interfaces with Warehouse ManagementSystem for seamless operation</li>
                                    </ul>
                                </c:if>
                                <c:if test="${not empty pagewarehouse4.brochure}">
                                    <div class="download">
                                        <a href="/brochure/${pagewarehouse4.brochure}">Download brochure</a>
                                    </div>
                                </c:if>
                                <div class="watch-video"><a href="#" type-video="warehouse_4">Watch video</a></div>
                            </div>
                        </div>
                            <div class="video">
                                <video id="player_warehouse_4" class="video-warehouse_2" controls width="100%">
                                    <source src="${pageContext.request.contextPath}/media/video/warehouse/warehouse.mp4" type="video/mp4">
                                </video>
                                <div class="play-button"><span></span></div>
                                <p class="play-video" >Watch video</p>
                            </div>
                        <span class="next-detail" type-move="warehouse"></span>
                    </div>
                    <div class="detail-footer footer">
                        <span class="back detail-back" num-video="warehouse_4" type-back="warehouse"></span>
                        <span class="scroll-down">scroll down</span>
                    </div>
                </div>
                <div da-slick="warehouse_5" class="item warehouse_5">
                    <div class="detail-header header">
                        <a href="${pageContext.request.contextPath}/" class="logo-back-home"><img class="logo" src="${pageContext.request.contextPath}/assets/logo.png"></a>
                        <a href="#" class="menu"></a>
                    </div>
                    <div class="detail-content">
                        <span class="pre-detail" type-move="warehouse"></span>
                        <div class="view">
                            <div class="content-left">
                                <div>
                                    <div class="for-main">
                                        <c:if test="${fn:length(pagewarehouse5.listSlide) > 1}">
                                            <span class="prev-item" data-slide="warehouse_5"></span>
                                        </c:if>
                                        <c:if test="${not empty pagewarehouse5.listSlide}">
                                            <c:set var="slide35" value="1"/>
                                            <c:forEach items="${pagewarehouse5.listSlide}" var="imgSlide35">
                                                <c:if test="${slide35 == 1}">
                                                    <img class="service-img main warehouse_5" src="${pageContext.request.contextPath}/media/images/${imgSlide35.slideimg}">
                                                </c:if>
                                            <c:set var="slide35" value="${slide35 + 1}"/>
                                            </c:forEach>
                                        </c:if>
                                        <c:if test="${fn:length(pagewarehouse5.listSlide) > 1}">
                                            <span class="next-item" data-slide="warehouse_5"></span>
                                        </c:if>
                                    </div>
                                    <c:if test="${fn:length(pagewarehouse5.listSlide) > 1}">
                                        <div class="body-thumbnail">
                                            <div class="thumbnail owl-carousel">
                                                <c:set var="n35" value="1"/>
                                                <c:forEach items="${pagewarehouse5.listSlide}" var="imgSlide">
                                                    <c:if test="${not empty imgSlide}" >
                                                        <c:if test="${n35 == 1}">
                                                            <c:set var="addActive35" value="active"/>
                                                        </c:if>
                                                        <div class="around">
                                                            <img data-img="${n35}" type-click="warehouse_5" class="img-thumbnail img-${n35} ${addActive35} warehouse_5" src="${pageContext.request.contextPath}/media/images/${imgSlide.slideimg}">
                                                        </div>
                                                        <c:set var="n35" value="${n35 + 1}"/>
                                                    </c:if>
                                                </c:forEach>
                                            </div>
                                        </div>
                                    </c:if>
                                </div>
                            </div>
                            <div class="content-right">
                                <c:if test="${not empty pagewarehouse5.content}">
                                    <h2>${pagewarehouse5.content}</h2>
                                </c:if>
                                <c:if test="${ empty pagewarehouse5.content}">
                                    <h2>Autonomous Tow Tractor</h2>
                                </c:if>
                                <p>KEY FEATURES</p>
                                <c:if test="${not empty pagewarehouse5.detailContent}">
                                    ${pagewarehouse5.detailContent}
                                </c:if>
                                <c:if test="${ empty pagewarehouse5.content}">
                                    <ul>
                                        <li>Suitable for outdoor operations</li>
                                        <li>Collision detection and avoidance</li>
                                        <li>Interfaces with Warehouse Management System for seamless operation</li>
                                    </ul>
                                </c:if>                                
                                <c:if test="${not empty pagewarehouse5.brochure}">
                                    <div class="download">
                                        <a href="/brochure/${pagewarehouse5.brochure}">Download brochure</a>
                                    </div>
                                </c:if>
                                <div class="watch-video"><a href="#" type-video="warehouse_5">Watch video</a></div>
                            </div>
                        </div>
                            <div class="video">
                                <video id="player_warehouse_5" class="video-warehouse_2" controls width="100%">
                                    <source src="${pageContext.request.contextPath}/media/video/warehouse/warehouse.mp4" type="video/mp4">
                                </video>
                                <div class="play-button"><span></span></div>
                                <p class="play-video" >Watch video</p>
                            </div>
                        <span class="next-detail" type-move="warehouse"></span>
                    </div>
                    <div class="detail-footer footer">
                        <span class="back detail-back" num-video="warehouse_5" type-back="warehouse"></span>
                        <span class="scroll-down">scroll down</span>
                    </div>
                </div>
                <div da-slick="warehouse_6" class="item warehouse_6">
                    <div class="detail-header header">
                        <a href="${pageContext.request.contextPath}/" class="logo-back-home"><img class="logo" src="${pageContext.request.contextPath}/assets/logo.png"></a>
                        <a href="#" class="menu"></a>
                    </div>
                    <div class="detail-content">
                        <span class="pre-detail" type-move="warehouse"></span>
                        <div class="view">
                            <div class="content-left">
                                <div>
                                    <div class="for-main">
                                        <c:if test="${fn:length(pagewarehouse6.listSlide) > 1}">
                                            <span class="prev-item" data-slide="warehouse_6"></span>
                                        </c:if>
                                        <c:if test="${not empty pagewarehouse6.listSlide}">
                                            <c:set var="slide36" value="1"/>
                                            <c:forEach items="${pagewarehouse6.listSlide}" var="imgSlide36">
                                                <c:if test="${slide36 == 1}">
                                                    <img class="service-img main warehouse_6" src="${pageContext.request.contextPath}/media/images/${imgSlide36.slideimg}">
                                                </c:if>
                                            <c:set var="slide36" value="${slide36 + 1}"/>
                                            </c:forEach>
                                        </c:if>
                                        <c:if test="${fn:length(pagewarehouse6.listSlide) > 1}">
                                            <span class="next-item" data-slide="warehouse_6"></span>
                                        </c:if>
                                    </div>
                                    <c:if test="${fn:length(pagewarehouse6.listSlide) > 1}">
                                        <div class="body-thumbnail">
                                            <div class="thumbnail owl-carousel">
                                                <c:set var="n36" value="1"/>
                                                <c:forEach items="${pagewarehouse6.listSlide}" var="imgSlide">
                                                    <c:if test="${not empty imgSlide}" >
                                                        <c:if test="${n36 == 1}">
                                                            <c:set var="addActive36" value="active"/>
                                                        </c:if>
                                                        <div class="around">
                                                            <img data-img="${n36}" type-click="warehouse_6" class="img-thumbnail img-${n36} ${addActive36} warehouse_6" src="${pageContext.request.contextPath}/media/images/${imgSlide.slideimg}">
                                                        </div>
                                                        <c:set var="n36" value="${n36 + 1}"/>
                                                    </c:if>
                                                </c:forEach>
                                            </div>
                                        </div>
                                    </c:if>
                                </div>
                            </div>
                            <div class="content-right">
                                <c:if test="${not empty pagewarehouse6.content}">
                                    <h2>${pagewarehouse6.content}</h2>
                                </c:if>
                                <c:if test="${empty pagewarehouse6.content}">
                                    <h2>Autonomous Pallet Stacker</h2>
                                </c:if>
                                <p>KEY FEATURES</p>
                                <c:if test="${not empty pagewarehouse6.detailContent}">
                                    ${pagewarehouse6.detailContent}
                                </c:if>
                                <c:if test="${empty pagewarehouse6.content}">
                                    <ul>
                                        <li>LIDAR SLAM technology for accurate localisation and dynamic path planning for navigation within warehouse aisle</li>
                                        <li>Collision detection and avoidance</li>
                                        <li>Advanced pallet detection and recognition capability</li>
                                        <li>Pallet stacking from ground to shelf racks</li>
                                        <li>Autonomous battery charging</li>
                                        <li>Robotic Management System for task scheduling and on-demand operation</li>
                                    </ul>
                                </c:if>
                                <c:if test="${not empty pagewarehouse6.brochure}">
                                    <div class="download">
                                        <a href="/brochure/${pagewarehouse6.brochure}">Download brochure</a>
                                    </div>
                                </c:if>
                                <div class="watch-video"><a href="#" type-video="warehouse_6">Watch video</a> <a class="tech-spec-warehouse_6" href="#">View Tech Specs</a></div>
                            </div>
                        </div>
                            <div class="video">
                                <video id="player_warehouse_6" class="video-warehouse_2" controls width="100%">
                                    <source src="${pageContext.request.contextPath}/media/video/warehouse/warehouse.mp4" type="video/mp4">
                                </video>
                                <div class="play-button"><span></span></div>
                                <p class="play-video" >Watch video</p>
                            </div>
                        <span class="next-detail" type-move="warehouse"></span>
                    </div>
                    <div class ="spec-warehouse_6">
                        <h4>Autonomous Pallet Stacker <a class ="back-warehouse6" href="#">Back</a> </h4>
                        <table class="table-bordered">
                            <tbody>
                                <tr>
                                    <th colspan="3" class ="center header">TECHNICAL SPECIFICATIONS</th>
                                </tr>
                                <tr>
                                    <td>Payload</td>
                                    <td class ="center">kg</td>
                                    <td class ="center">1400</td>
                                </tr>
                                <tr>
                                    <td>Service Weight (Including Battery)</td>
                                    <td class ="center">kg</td>
                                    <td class ="center">1200</td>
                                </tr>
                                <tr>
                                    <td>Dimensions(LxWxH)</td>
                                    <td class ="center">mm</td>
                                    <td class ="center">2100 x 830 x 2200</td>
                                </tr>
                                <tr>
                                    <td>Fork Size(LxWxH)</td>
                                    <td class ="center">mm</td>
                                    <td class ="center">1150 x 190 x 60</td>
                                </tr>
                                <tr class ="colors">
                                    <td>Fork Spread (Width over forks)</td>
                                    <td class ="center">mm</td>
                                    <td class ="center">560<div>(690 availble)</div></td>
                                </tr>
                                <tr class ="colors">
                                    <td>Fork Lowered Height</td>
                                    <td class ="center">mm</td>
                                    <td class ="center">85</td>
                                </tr>
                                <tr class ="colors">
                                    <td>Fork Lifted Height</td>
                                    <td class ="center">mm</td>
                                    <td class ="center">4000</td>
                                </tr>
                                <tr>
                                    <td>Walking Speed</td>
                                    <td class ="center">kph</td>
                                    <td class ="center">6</td>
                                </tr>
                                <tr>
                                    <td>Gradaliblity</th>
                                    <td class ="center">%</td>
                                    <td class ="center">5</td>
                                </tr>
                                <tr>
                                    <td>Turning Radius</td>
                                    <td class ="center">mm</td>
                                    <td class ="center">1564</td>
                                </tr>
                                <tr>
                                    <td>Operating Noise Level</td>
                                    <td class ="center">dBA</td>
                                    <td class ="center">75</td>
                                </tr>
                                <tr class ="colors">
                                    <td>Motor Controller</td>
                                    <td class ="center">Type</td>
                                    <td class ="center">Low Voltage AC</td>
                                </tr>
                                <tr class ="colors">
                                    <td>Voltage</td>
                                    <td class ="center">V</td>
                                    <td class ="center">24</td>
                                </tr>
                                <tr class ="colors">
                                    <td>Battery Capacity</td>
                                    <td class ="center">VAH</td>
                                    <td class ="center">110</td>
                                </tr>
                                <tr class ="colors">
                                    <td>Change Time (24V30A Charger)</td>
                                    <td class ="center">Hr</td>
                                    <td class ="center">4</td>
                                </tr>
                                <tr class ="colors">
                                    <td>Change Time (24V90A Charger)</td>
                                    <td class ="center">Hr</td>
                                    <td class ="center">1.5</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="detail-footer footer">
                        <span class="back detail-back" num-video="warehouse_6" type-back="warehouse"></span>
                        <span class="scroll-down">scroll down</span>
                    </div>
                </div>
            </div>
            <!-- END WAREHOUSE -->

            <!-- RMS -->

            <div class="item rms">
                <div class="detail-header header">
                    <a href="${pageContext.request.contextPath}/" class="logo-back-home"><img class="logo" src="${pageContext.request.contextPath}/assets/logo.png"></a>
                    <a href="#" class="menu"></a>
                </div>
                <div class="detail-content">
                    <div class="view">
                        <div class="content-left">
                            <div>
                                <img class="service-img main" src="${pageContext.request.contextPath}/assets/detail_rms.png">
                            </div>
                        </div>
                        <div class="content-right">
                            <c:if test="${empty robotic.name}">
                                <h2>Robotics Management System</h2>
                            </c:if>
                            <c:if test="${not empty robotic.name}">
                                <h2>${robotic.name}</h2>
                            </c:if>
                            <p>DESCRIPTION</p>
                            <c:if test="${empty robotic.description}">
                                <h5>Harnessing the power of technology and smart engineering, every STROBO autonomous solution is integrated with ST Engineering’s proprietary Robotics Management System (RMS) which allows seamless operation and fleet management in existing infrastructure.</h5>
                            </c:if>
                            <c:if test="${not empty robotic.description}">
                                <h5>${robotic.description}</h5>
                            </c:if>
                            <p>KEY FEATURES</p>
                            <c:if test="${not empty robotic.keyfeatures}">
                                ${robotic.keyfeatures}
                            </c:if>
                            <c:if test="${ empty robotic.keyfeatures}">
                                <ul>
                                    <li>Real-time platform status</li>
                                    <li>Fail-safe disabling</li>
                                    <li>Interface for mobility mode control</li>
                                    <li>Interface for payload control</li>
                                    <li>Manned to unmanned switching</li>
                                    <li>Linked to existing infrastructure</li>
                                    <li>Collaborative networking</li>
                                    <li>Wireless transmission Cybersecure</li>
                                </ul>
                            </c:if>
                        </div>
                    </div>
                </div>
                <div class="detail-footer footer">
                    <span class="back detail-back" num-video="rms" type-back="warehouse"></span>
                    <span class="scroll-down">scroll down</span>
                </div>
            </div>
            <!-- END RMS -->

        </div>
        <div class="all-footers footer">
            <p class="footer">COPYRIGHT @2018 ST ENGINEERING LAND SYSTEMS LTD. | ALL RIGHTS RESERVED</p>
        </div>
    </div>
</body>
</html>