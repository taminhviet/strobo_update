<%--
  Created by IntelliJ IDEA.
  User: mykavalli
  Date: 24/12/2018
  Time: 10:45 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="${pageContext.request.contextPath}/images/favicon.png" type="image/png" sizes="16x16">
    <title>Manufacturing</title>

    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/Meta_Serif.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/Lato.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/slick.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/fullpage.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/jquery.scrollbar.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/my.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/manufacturing.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/st_general.css" />

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

    <script type="text/javascript" src="${pageContext.request.contextPath}/js/strobo/scrolloverflow.js"></script>
    <script src="${pageContext.request.contextPath}/js/strobo/scrolloverflow.js"></script>
    <script src="${pageContext.request.contextPath}/js/strobo/fullpage.js"></script>
    <script src="${pageContext.request.contextPath}/js/strobo/slick.js"></script>
    <script src="${pageContext.request.contextPath}/js/strobo/owl.carousel.js"></script>
    <script src="${pageContext.request.contextPath}/js/strobo/my.js"></script>
</head>
<body class="body-item manufacturing">
<div class="menu-body">
    <img src="${pageContext.request.contextPath}/assets/menu_black_close.png" class="close-menu">
    <ul>
        <c:forEach items="${menuDefault}" var="menu">
            <c:if test="${menu.url == '#home'}">
                <li><a class="menu-item" data-menu="home" href="${pageContext.request.contextPath}/">${menu.value}</a></li>
            </c:if>
            <c:if test="${menu.url == '#about'}">
                <li><a class="menu-item" data-menu="about" href="${pageContext.request.contextPath}/#about-us">${menu.value}</a></li>
            </c:if>
            <c:if test="${menu.url == '#core'}">
                <li><a class="menu-item" data-menu="core" href="${pageContext.request.contextPath}/technology">${menu.value}</a></li>
            </c:if>
        </c:forEach>
        <li><a  class="menu-item solutions" data-menu="solutions" href="${pageContext.request.contextPath}/services">Our Solutions</a>
            <ol class="submenu">
                <c:forEach items="${menuDefault}" var="menu">
                    <c:if test="${menu.url == '#seaport'}">
                        <li><a class="sub-item" data-menu="seaport" href="${pageContext.request.contextPath}/services/seaport">${menu.value}</a></li>
                    </c:if>
                    <c:if test="${menu.url == '#manufacturing'}">
                        <li><a class="sub-item active" data-menu="manufacturing" href="${pageContext.request.contextPath}/services/manufacturing">${menu.value}</a></li>
                    </c:if>
                    <c:if test="${menu.url == '#warehouse'}">
                        <li><a class="sub-item" data-menu="warehouse" href="${pageContext.request.contextPath}/services/warehouse">${menu.value}</a></li>
                    </c:if>
                    <c:if test="${menu.url == '#airport'}">
                        <li><a class="sub-item" data-menu="airport" href="${pageContext.request.contextPath}/services/airport">${menu.value}</a></li>
                    </c:if>
                </c:forEach>
            </ol>
        </li>

        <c:forEach items="${menuNew}" var="menu">
            <c:if test="${menu.status == true}">
                <li><a class="menu-item" href="${pageContext.request.contextPath}/${menu.url}">${fn:toUpperCase(menu.value)}</a></li>
            </c:if>
        </c:forEach>

        <c:forEach items="${menuDefault}" var="menu">
            <c:if test="${menu.url == '#contact'}">
                <li><a class="menu-item" data-menu="contact" href="${pageContext.request.contextPath}/contact">${menu.value}</a></li>
            </c:if>
        </c:forEach>
    </ul>
    <img class="st-engineering" src="${pageContext.request.contextPath}/assets/ST_engineering.png">
</div>
<div class="body-content manufacturing">
    <div class="item-content active">
        <div class="all-item manufacturing active">
            <div class="manufacturing-header header">
                <a href="${pageContext.request.contextPath}/" class="logo-back-home"><img class="logo" src="${pageContext.request.contextPath}/assets/logo.png"></a>
                <c:if test="${ not empty pagemanufacturing.pageContent}">
                    <p>${pagemanufacturing.pageContent}</p>
                </c:if>
                <c:if test="${ empty pagemanufacturing.pageContent}">
                    <p>Comprehensive end-to-end automation solutions to improve processes and efficiency, helping customers reduce waste, investment and manpower requirements.</p>
                </c:if>
                <a href="#" class="menu"></a>
            </div>