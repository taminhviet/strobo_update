<%--
  Created by IntelliJ IDEA.
  User: My Kavalli
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<html>
<head>
    <title>
        <c:if test="${empty genericPage.title}" >STrobo</c:if>
        <c:if test="${not empty genericPage.title}" >${genericPage.title}</c:if>
    </title>

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="keywords" content="${genericPage.keyword}">
    <meta name="description" content="${genericPage.meta}">
    <title>${genericPage.title}</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/home.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/contact.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/fonts.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/custom.css" />
    <script src="${pageContext.request.contextPath}/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/strobo/home.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" ></script>
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/images/favicon.png" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/st_general.css" />

</head>
<body class="page page-custom">
    <div class="menu-body">
        <img src="${pageContext.request.contextPath}/assets/menu_black_close.png" class="close-menu">
        <div class="menu-content">
            <div class="content">
        <ul>
            <c:forEach items="${menuDefault}" var="menu">
                <c:if test="${menu.url == '#home'}">
                    <li><a class="menu-item" data-menu="home" href="${pageContext.request.contextPath}/">${menu.value}</a></li>
                </c:if>
                <c:if test="${menu.url == '#about'}">
                    <li><a class="menu-item" data-menu="about" href="${pageContext.request.contextPath}/#about-us">${menu.value}</a></li>
                </c:if>
                <c:if test="${menu.url == '#core'}">
                    <li><a class="menu-item" data-menu="core" href="${pageContext.request.contextPath}/technology">${menu.value}</a></li>
                </c:if>
            </c:forEach>
            <li><a class="menu-item solutions" data-menu="solutions" href="${pageContext.request.contextPath}/services">Our Solutions</a>
                <ol class="submenu">
                    <c:forEach items="${menuDefault}" var="menu">
                        <c:if test="${menu.url == '#seaport'}">
                            <li><a class="sub-item" data-menu="seaport" href="${pageContext.request.contextPath}/services/seaport">${menu.value}</a></li>
                        </c:if>
                        <c:if test="${menu.url == '#manufacturing'}">
                            <li><a class="sub-item" data-menu="manufacturing" href="${pageContext.request.contextPath}/services/manufacturing">${menu.value}</a></li>
                        </c:if>
                        <c:if test="${menu.url == '#warehouse'}">
                            <li><a class="sub-item" data-menu="warehouse" href="${pageContext.request.contextPath}/services/warehouse">${menu.value}</a></li>
                        </c:if>
                        <c:if test="${menu.url == '#airport'}">
                            <li><a class="sub-item" data-menu="airport" href="${pageContext.request.contextPath}/services/airport">${menu.value}</a></li>
                        </c:if>
                    </c:forEach>
                </ol>
            </li>

            <c:forEach items="${menuNew}" var="menu">
            <c:if test="${menu.status == true}">
                <li><a class="menu-item" href="${pageContext.request.contextPath}/${menu.url}">${fn:toUpperCase(menu.value)}</a></li>
            </c:if>
            </c:forEach>

            <c:forEach items="${menuDefault}" var="menu">
                <c:if test="${menu.url == '#contact'}">
                    <li><a class="menu-item" data-menu="contact" href="${pageContext.request.contextPath}/contact">${menu.value}</a></li>
                </c:if>
            </c:forEach>
            <li><a class="menu-item" data-menu="contact" href="${pageContext.request.contextPath}/news">News</a></li>
        <li><img class="st-engineering" src="${pageContext.request.contextPath}/assets/ST_engineering.png"></li>
        </ul>
    </div>
    </div>
    </div>
    <div class="content custom-content">
        <div class="custom">
            <div class="container">
                <div class="row">
                    ${genericPage.pageContent}
                </div>
            </div>
        </div>
    </div>
    <div class="footer footer-contactpage">
        <div class="wrap">
            <div class="site-info">
                <p>STROBO 2018 | All Copyrights Reserved</p>
            </div>
        </div>
    </div>
</body>
</html>
