<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <%--<!-- <meta name="keywords" content="${about.keyword}"> -->--%>
    <%--<!-- <meta name="description" content="${about.meta}"> -->--%>
    <link rel="icon" href="${pageContext.request.contextPath}/images/favicon.png" type="image/png" sizes="16x16">
    <title>About Us</title>

    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/Meta_Serif.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/Lato.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/slick.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/fullpage.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/jquery.scrollbar.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/my.css" />
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

    <script type="text/javascript" src="${pageContext.request.contextPath}/js/strobo/scrolloverflow.js"></script>
    <script src="${pageContext.request.contextPath}/js/strobo/scrolloverflow.js"></script>
    <script src="${pageContext.request.contextPath}/js/strobo/fullpage.js"></script>
    <script src="${pageContext.request.contextPath}/js/strobo/slick.js"></script>
    <script src="${pageContext.request.contextPath}/js/strobo/my.js"></script>
</head>
<body>
<div class="menu-body">
    <img src="${pageContext.request.contextPath}/assets/menu_black_close.png" class="close-menu">
    <div class="menu-content">
            <div class="content">
    <ul>
        <c:forEach items="${menuDefault}" var="menu">
            <c:if test="${menu.url == '#home'}">
                <li><a target="_blank" class="menu-item" data-menu="home" href="${pageContext.request.contextPath}/">${menu.value}</a></li>
            </c:if>
            <c:if test="${menu.url == '#about'}">
                <li><a target="_blank" class="menu-item" data-menu="about" href="${pageContext.request.contextPath}/aboutus">${menu.value}</a></li>
            </c:if>
            <c:if test="${menu.url == '#core'}">
                <li><a target="_blank" class="menu-item" data-menu="core" href="${pageContext.request.contextPath}/technology">${menu.value}</a></li>
            </c:if>
        </c:forEach>
        <li><a  class="menu-item solutions" data-menu="solutions" href="#service">Our Solutions</a>
            <ol class="submenu">
                <c:forEach items="${menuDefault}" var="menu">
                    <c:if test="${menu.url == '#seaport'}">
                        <li><a target="_blank" class="sub-item" data-menu="seaport" href="${pageContext.request.contextPath}/service/seaport">${menu.value}</a></li>
                    </c:if>
                    <c:if test="${menu.url == '#manufacturing'}">
                        <li><a target="_blank" class="sub-item" data-menu="manufacturing" href="${pageContext.request.contextPath}/service/manufacturing">${menu.value}</a></li>
                    </c:if>
                    <c:if test="${menu.url == '#warehouse'}">
                        <li><a target="_blank" class="sub-item" data-menu="warehouse" href="${pageContext.request.contextPath}/service/warehouse">${menu.value}</a></li>
                    </c:if>
                    <c:if test="${menu.url == '#airport'}">
                        <li><a target="_blank" class="sub-item" data-menu="airport" href="${pageContext.request.contextPath}/service/airport">${menu.value}</a></li>
                    </c:if>
                </c:forEach>
            </ol>
        </li>

        <c:forEach items="${menuNew}" var="menu">
            <c:if test="${menu.status == true}">
                <li><a target="_blank" class="menu-item" href="${pageContext.request.contextPath}/${menu.url}">${fn:toUpperCase(menu.value)}</a></li>
            </c:if>
        </c:forEach>

        <c:forEach items="${menuDefault}" var="menu">
            <c:if test="${menu.url == '#contact'}">
                <li><a target="_blank" class="menu-item" data-menu="contact" href="${pageContext.request.contextPath}/contact">${menu.value}</a></li>
            </c:if>
        </c:forEach>
        <li><a class="menu-item" data-menu="contact" href="${pageContext.request.contextPath}/news">News</a></li>
	    <li><img class="st-engineering" src="${pageContext.request.contextPath}/assets/ST_engineering.png"></li>
    </ul>
    </div>
    </div>
</div>
    <div class="about-page" id="aboutPage">
        <div class="about-header header">
            <a href="#home" class="logo-back-home">
                <img src="assets/logo_white.png" class="logo">
            </a>
            <img src="assets/menu_white.png" class="menu">
        </div>
        <div class="about-content">
            <div class="item content-first">
                <div class="details about">
                    <div class="title">
                        <p class="text-vertical">${about.title1}</p>
                    </div>
                    <div class="heading">
                        <h2 class="head-title">${about.heading1}</h2>
                    </div>
                    <div class="description">
                        <p class="detail">${about.description1}</p>
                        <p class="detail">${about.description2}</p>
                    </div>
                </div>
            </div>
            <div class="item content-seconds">
                <div class="details company">
                    <div class="title">
                        <p class="text-vertical" >${about.title2}</p>
                    </div>
                    <div class="heading">
                        <h2 class="head-title">${about.heading2}</h2>
                    </div>
                    <div class="description">
                        <p class="detail">${about.description3}</p>
                        <p class="detail">${about.description4}</p>
                    </div>
                    <div class="visit-site">
                        <button class="button" type="button">${about.visit}</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="about-footer footer">
            <p class="footer">COPYRIGHT @2018 ST ENGINEERING LAND SYSTEMS LTD. | ALL RIGHTS RESERVED</p>
        </div>
    </div>
</body>
</html>