<%--
  Created by IntelliJ IDEA.
  User: My Kavalli
  Date: 04/09/2018
  Time: 15:44 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Error</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/node_modules/mdi/css/materialdesignicons.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/node_modules/perfect-scrollbar/dist/css/perfect-scrollbar.min.css">
    <!-- endinject -->
    <!-- plugin css for this page -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/node_modules/icheck/skins/all.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/node_modules/dropify/dist/css/dropify.min.css">
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/custom.css">
    <!-- endinject -->
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/images/favicon.png" />
</head>
<body>
    <div class="http-error">
        <div class="container">
            <a class="grid-margin" href="${pageContext.request.contextPath}/"><img src="${pageContext.request.contextPath}/assets/logo.png"></a>
            <h1 class="text-warning text-center">${config.error_head}</h1>
            <p class="message-text text-center">${config.error_content}</p>
            <a class="btn btn-primary" href="${pageContext.request.contextPath}/">Home page</a>
        </div>
    </div>
</body>
</html>
