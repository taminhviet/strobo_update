<%--
  Created by IntelliJ IDEA.
  User: MyKavalli
  Date: 11/10/2018
  Time: 14:27 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="keywords" content="${homePage.keyword}">
    <meta name="description" content="${homePage.meta}">
    <link rel="icon" href="${pageContext.request.contextPath}/images/favicon.png" type="image/png" sizes="16x16">
    <c:if test="${empty homePage.title}" >
        <title>STrobo</title>
    </c:if>
    <c:if test="${not empty homePage.title}" >
        <title>${homePage.title}</title>
    </c:if>

    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/Meta_Serif.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/Lato.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/slick.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/fullpage.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/jquery.scrollbar.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/my.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/mobile.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/T_custom.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/st_general.css" />
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

    <script type="text/javascript" src="${pageContext.request.contextPath}/js/strobo/scrolloverflow.js"></script>
    <script src="${pageContext.request.contextPath}/js/strobo/scrolloverflow.js"></script>
    <script src="${pageContext.request.contextPath}/js/strobo/fullpage.js"></script>
    <script src="${pageContext.request.contextPath}/js/strobo/slick.js"></script>
    <script src="${pageContext.request.contextPath}/js/strobo/my.js"></script>
</head>
<body>
<div class="strobo">
    <div class="menu-body">
        <img src="${pageContext.request.contextPath}/assets/menu_black_close.png" class="close-menu">
        <div class="menu-content">
            <div class="content">
        <ul>
            <c:forEach items="${menuDefault}" var="menu">
                <c:if test="${menu.url == '#home'}">
                    <li><a class="menu-item" data-menu="home" href="#home">${menu.value}</a></li>
                </c:if>
                <c:if test="${menu.url == '#about'}">
                    <li><a class="menu-item" data-menu="about" href="${pageContext.request.contextPath}/#about-us">${menu.value}</a></li>
                </c:if>
                <c:if test="${menu.url == '#core'}">
                    <li><a class="menu-item" data-menu="core" href="${pageContext.request.contextPath}/technology">${menu.value}</a></li>
                </c:if>
            </c:forEach>
            <li><a  class="menu-item solutions" data-menu="solutions" href="${pageContext.request.contextPath}/services">Our Solutions</a>
                <ol class="submenu">
                    <c:forEach items="${menuDefault}" var="menu">
                        <c:if test="${menu.url == '#seaport'}">
                            <li><a class="sub-item" data-menu="seaport" href="${pageContext.request.contextPath}/services/seaport">${menu.value}</a></li>
                        </c:if>
                        <c:if test="${menu.url == '#manufacturing'}">
                            <li><a class="sub-item" data-menu="manufacturing" href="${pageContext.request.contextPath}/services/manufacturing">${menu.value}</a></li>
                        </c:if>
                        <c:if test="${menu.url == '#warehouse'}">
                            <li><a class="sub-item" data-menu="warehouse" href="${pageContext.request.contextPath}/services/warehouse">${menu.value}</a></li>
                        </c:if>
                        <c:if test="${menu.url == '#airport'}">
                            <li><a class="sub-item" data-menu="airport" href="${pageContext.request.contextPath}/services/airport">${menu.value}</a></li>
                        </c:if>
                    </c:forEach>
                </ol>
            </li>

            <c:forEach items="${menuNew}" var="menu">
                <c:if test="${menu.status == true}">
                    <li><a class="menu-item" href="${pageContext.request.contextPath}/${menu.url}">${fn:toUpperCase(menu.value)}</a></li>
                </c:if>
            </c:forEach>

            <c:forEach items="${menuDefault}" var="menu">
                <c:if test="${menu.url == '#contact'}">
                    <li><a class="menu-item" data-menu="contact" href="${pageContext.request.contextPath}/contact">${menu.value}</a></li>
                </c:if>
            </c:forEach>
            <li><a class="menu-item" data-menu="contact" href="${pageContext.request.contextPath}/news">News</a></li>
        <li><img class="st-engineering" src="${pageContext.request.contextPath}/assets/ST_engineering.png"></li>
        </ul>
    </div>
    </div>
    </div>
    <div class="mobile-header">
        <img src="${pageContext.request.contextPath}/assets/logo.png" class="logo">
        <a href="#" class="menu"></a>
    </div>
    <c:set var="num_service" value="1" />
    <c:forEach items="${services}" var="service">
        <c:if test="${num_service == 1}">
            <c:set var="type_service" value="seaport" />
            <c:set var="link_seaport" value="${service.linkvideo}" />
            <c:set var="title_seaport" value="${service.titleService}" />
        </c:if>
        <c:if test="${num_service == 2}">
            <c:set var="type_service" value="manufacturing" />
            <c:set var="link_manufacturing" value="${service.linkvideo}" />
            <c:set var="title_manufacturing" value="${service.titleService}" />
        </c:if>
        <c:if test="${num_service == 3}">
            <c:set var="type_service" value="warehouse" />
            <c:set var="link_warehouse" value="${service.linkvideo}" />
            <c:set var="title_warehouse" value="${service.titleService}" />
        </c:if>
        <c:if test="${num_service == 4}">
            <c:set var="type_service" value="airport" />
            <c:set var="link_airport" value="${service.linkvideo}" />
            <c:set var="title_airport" value="${service.titleService}" />
        </c:if>
        <div class="video-intro ${type_service}" data-video-intro="${num_service}">
            <div class="video-content">
                <span class="prev-video"></span>
                <div class="video-detail">
                <img src="assets/menu_white_close.png" class="close-video-intro">
                    <video da-vi="${num_service}" id="video_${num_service}" controls class="popup-video" width="100%">
                        <source src="${pageContext.request.contextPath}/media/video/${type_service}/${service.linkvideo}" type="video/mp4">
                    </video>
                </div>
                <span class="next-video"></span>
            </div>
        </div>
        <c:set var="num_service" value="${num_service + 1}" />
    </c:forEach>
    <div class="item" id="homefull">
        <div class="section" id="homePage">
            <c:if test="${empty homePage.file}">
                <video autoplay="autoplay" data-autoplay muted loop id="home_video_bg"" class="home-video-bg">
                    <source src="${pageContext.request.contextPath}/media/video/home/strobo_home.MP4" type="video/mp4">
                </video>
            </c:if>
            <c:if test="${not empty homePage.file}">
                <video autoplay="autoplay" data-autoplay muted loop id="home_video_bg" class="home-video-bg">
                    <source src="${pageContext.request.contextPath}/media/video/home/${homePage.file}" type="video/mp4">
                </video>
            </c:if>
            <div class="video-home">
                <div class="popup-content">
                    <div class="content">
                        <div class="video-detail">
                        <img src="assets/menu_white_close.png" class="close-video">
                            <video controls autoplay="autoplay" loop id="home_video_popup">
                                <source src="${pageContext.request.contextPath}/media/video/home/home_light_box.mp4" type="video/mp4">
                            </video>
                        </div>
                    </div>
                </div>
            </div>
            <div class="home-header">
                <img src="${pageContext.request.contextPath}/assets/logo.png" alt="" class="logo" style="position: absolute;">
                <a href="#" class="menu"></a>
            </div>
            <div class="home-content">
                <img class="home-logo" src="${pageContext.request.contextPath}/assets/logo_white.png">
                <c:if test="${ empty homePage.heading1}" >
                    <h2>Future-Ready Autonomous Solutions</h2>
                </c:if>
                <c:if test="${not empty homePage.heading1}" >
                    <h2>${homePage.heading1}</h2>
                </c:if>
                <c:if test="${ empty homePage.buttonLabel}" >
                    <button type="button" id="play_video" class="button btn-play-video">WATCH VIDEO</button>
                </c:if>
                <c:if test="${not empty homePage.buttonLabel}" >
                    <button type="button" id="play_video" class="button btn-play-video">${homePage.buttonLabel}</button>
                </c:if>
            </div>
            <div class="home-footer">
                <a href="#service" class="skip">SKIP</a>
                <a href="#service" class="next"></a>
            </div>
        </div>
        <div class="section" id="servicePage">
            <div class="service-header">
                <a class="logo" href="${pageContext.request.contextPath}/"><span id="logo_service"></span></a>
                <a href="#" class="menu"></a>
            </div>
            <c:forEach items="${services}" var="service">
            </c:forEach>
            <div class="service-content">
                <div class="service-seaport service-item services-1" data-service="1" type-service="seaport">
                    <a href="${pageContext.request.contextPath}/services/seaport" type-service="/services/seaport" class="go-all"></a>
                    <img src="${pageContext.request.contextPath}/assets/seaport.png" type-service="seaport">
                    <div class="nomal seaport">
                        <c:if test="${empty title_seaport}" >
                            <h2 class="title" type-service="seaport">Seaport</h2>
                        </c:if>
                        <c:if test="${not empty title_seaport}" >
                            <h2 class="title" type-service="seaport">${title_seaport}</h2>
                        </c:if>
                        <span class="video" data-type-video="seaport">watch video</span>
                    </div>
                </div>
                <div class="service-manufacturing service-item services-2" data-service="2" type-service="manufacturing">
                    <a href="${pageContext.request.contextPath}/services/manufacturing" type-service="/services/manufacturing" class="go-all"></a>
                    <img src="${pageContext.request.contextPath}/assets/detail_manufacturing_1.png" type-service="manufacturing">
                    <div class="nomal manufacturing">
                        <c:if test="${empty title_manufacturing}" >
                            <h2 class="title" type-service="manufacturing"> Manufacturing</h2>
                        </c:if>
                        <c:if test="${not empty title_manufacturing}" >
                            <h2 class="title" type-service="manufacturing">${title_manufacturing}</h2>
                        </c:if>
                        <span class="video" data-type-video="manufacturing">watch video</span>
                    </div>
                </div>
                <div class="service-warehouse service-item services-3" data-service="3" type-service="warehouse">
                    <a href="${pageContext.request.contextPath}/services/warehouse" type-service="/services/warehouse" class="go-all"></a>
                    <img src="${pageContext.request.contextPath}/assets/warehouse.png" type-service="warehouse">
                    <div class="nomal warehouse">
                        <c:if test="${empty title_warehouse}" >
                            <h2 class="title" type-service="warehouse">Warehouse</h2>
                        </c:if>
                        <c:if test="${not empty title_warehouse}" >
                            <h2 class="title" type-service="warehouse">${title_warehouse}</h2>
                        </c:if>
                        <span class="video" data-type-video="warehouse">watch video</span>
                    </div>
                </div>
                <div class="service-airport service-item services-4" data-service="4" type-service="airport">
                    <a href="${pageContext.request.contextPath}/services/airport" type-service="/services/airport" class="go-all"></a>
                    <img src="${pageContext.request.contextPath}/assets/airport.png" type-service="airport">
                    <div class="nomal airport">
                        <c:if test="${empty title_airport}" >
                            <h2 class="title" type-service="airport">Airport</h2>
                        </c:if>
                        <c:if test="${not empty title_airport}" >
                            <h2 class="title" type-service="airport">${title_airport}</h2>
                        </c:if>
                        <span class="video" data-type-video="airport">watch video</span>
                    </div>
                </div>
            </div>
            <div class="service-footer">
                <span class="prev prev-service"></span >
                <span class="next next-service"></span >
            </div>
        </div>
        <div class="section" id="aboutPage">
            <div class="about-header header">
                <a href="#home" class="logo-back-home">
                    <img src="assets/logo_white.png" class="logo">
                </a>
                <img src="assets/menu_white.png" class="menu">
            </div>
            <div class="about-content">
                <div class="item content-first">
                    <div class="details about">
                        <div class="title">
                            <p class="text-vertical">${about.title1}</p>
                        </div>
                        <div class="heading">
                            <h2 class="head-title">${about.heading1}</h2>
                        </div>
                        <div class="description">
                            <p class="detail">${about.description1}</p>
                            <p class="detail">${about.description2}</p>
                        </div>
                    </div>
                </div>
                <div class="item content-seconds">
                    <div class="details company">
                        <div class="title">
                            <p class="text-vertical" >${about.title2}</p>
                        </div>
                        <div class="heading">
                            <h2 class="head-title">${about.heading2}</h2>
                        </div>
                        <div class="description">
                            <p class="detail">${about.description3}</p>
                            <p class="detail">${about.description4}</p>
                        </div>
                        <div class="visit-site">
                            <button class="button about-st" type="button">${about.visit}</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="about-footer footer">
                <p class="footer">COPYRIGHT @2018 ST ENGINEERING LAND SYSTEMS LTD. | ALL RIGHTS RESERVED</p>
            </div>
        </div>
    </div>
</div>
</body>
</html>