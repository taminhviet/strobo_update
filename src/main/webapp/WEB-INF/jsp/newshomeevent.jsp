<%--
  Created by IntelliJ IDEA.
  User: mykavalli
  Date: 24/12/2018
  Time: 10:44 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="${pageContext.request.contextPath}/images/favicon.png" type="image/png" sizes="16x16">
    <title>Event</title>

    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/owl.carousel.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/Meta_Serif.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/Lato.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/slick.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/fullpage.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/jquery.scrollbar.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/my.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/st_general.css" />
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/rose.css" />

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

    <script type="text/javascript" src="${pageContext.request.contextPath}/js/strobo/scrolloverflow.js"></script>
    <script src="${pageContext.request.contextPath}/js/strobo/scrolloverflow.js"></script>
    <script src="${pageContext.request.contextPath}/js/strobo/fullpage.js"></script>
    <script src="${pageContext.request.contextPath}/js/strobo/slick.js"></script>
    <script src="${pageContext.request.contextPath}/js/strobo/owl.carousel.js"></script>
    <script src="${pageContext.request.contextPath}/js/strobo/my.js"></script>
	<script src="${pageContext.request.contextPath}/js/strobo/rose.js"></script>
</head>
<body class="body-item news">
<div class="menu-body">
    <img src="${pageContext.request.contextPath}/assets/menu_black_close.png" class="close-menu">
    <div class="menu-content">
            <div class="content">
    <ul>
        <c:forEach items="${menuDefault}" var="menu">
            <c:if test="${menu.url == '#home'}">
                <li><a class="menu-item" data-menu="home" href="${pageContext.request.contextPath}/">${menu.value}</a></li>
            </c:if>
            <c:if test="${menu.url == '#about'}">
                <li><a class="menu-item" data-menu="about" href="${pageContext.request.contextPath}/#about-us">${menu.value}</a></li>
            </c:if>
            <c:if test="${menu.url == '#core'}">
                <li><a class="menu-item" data-menu="core" href="${pageContext.request.contextPath}/technology">${menu.value}</a></li>
            </c:if>
        </c:forEach>
        <li><a  class="menu-item solutions" data-menu="solutions" href="${pageContext.request.contextPath}/services">Our Solutions</a>
            <ol class="submenu">
                <c:forEach items="${menuDefault}" var="menu">
                    <c:if test="${menu.url == '#seaport'}">
                        <li><a class="sub-item" data-menu="seaport" href="${pageContext.request.contextPath}/services/seaport">${menu.value}</a></li>
                    </c:if>
                    <c:if test="${menu.url == '#manufacturing'}">
                        <li><a class="sub-item" data-menu="manufacturing" href="${pageContext.request.contextPath}/services/manufacturing">${menu.value}</a></li>
                    </c:if>
                    <c:if test="${menu.url == '#warehouse'}">
                        <li><a class="sub-item" data-menu="warehouse" href="${pageContext.request.contextPath}/services/warehouse">${menu.value}</a></li>
                    </c:if>
                    <c:if test="${menu.url == '#airport'}">
                        <li><a class="sub-item" data-menu="airport" href="${pageContext.request.contextPath}/services/airport">${menu.value}</a></li>
                    </c:if>
                </c:forEach>
            </ol>
        </li>

        <c:forEach items="${menuNew}" var="menu">
            <c:if test="${menu.status == true}">
                <li><a class="menu-item" href="${pageContext.request.contextPath}/${menu.url}">${fn:toUpperCase(menu.value)}</a></li>
            </c:if>
        </c:forEach>

        <c:forEach items="${menuDefault}" var="menu">
            <c:if test="${menu.url == '#contact'}">
                <li><a class="menu-item" data-menu="contact" href="${pageContext.request.contextPath}/contact">${menu.value}</a></li>
            </c:if>
        </c:forEach>
        <li><a class="menu-item active" data-menu="contact" href="${pageContext.request.contextPath}/news">News</a></li>
    <li><img class="st-engineering" src="${pageContext.request.contextPath}/assets/ST_engineering.png"></li>
    </ul>
</div>
</div>
</div>
    <div class="body-content news event">
        <div class="manufacturing-header header">
            <a href="${pageContext.request.contextPath}/" class="logo-back-home">
                <img class="logo" src="${pageContext.request.contextPath}/assets/logo.png">
            </a>
            <a href="#" class="menu"></a>
        </div>
			<div class="news-content">
				<div class="container">
					<div class="events-contian">
						<div class="col-info">
							<h2 class="title">${newsEvent.title}</h2>
							<div class="content">${newsEvent.description}</div>
						</div>
						<div class="col-gallery">
							<div class="row-image">
							<c:if test="${not empty gallery}">
								<c:forEach items="${gallery}" var = "gallery">
									<div class="img"><div class="image-container"><img src="${pageContext.request.contextPath}/assets/${gallery}"></div></div>
								</c:forEach>
							</c:if>
							</div>
						</div>
						<div class="events-triggle">
							<div class="events-gallery owl-carousel">
								<c:if test="${not empty gallery}">
									<c:forEach items="${gallery}" var = "gallery">
										<div class="img"><img src="${pageContext.request.contextPath}/assets/${gallery}"></div>
									</c:forEach>
								</c:if>
							</div>
							<span class="close"></span>
						</div>
					</div>
				</div>
			</div>
        <div class="all-footers footer">
            <p class="footer">COPYRIGHT @2018 ST ENGINEERING LAND SYSTEMS LTD. | ALL RIGHTS RESERVED</p>
        </div>
    </div>
</body>
</html>
