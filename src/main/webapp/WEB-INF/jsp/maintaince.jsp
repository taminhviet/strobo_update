<%--
  Created by IntelliJ IDEA.
  User: My Kavali
  Date: 07/09/2018
  Time: 16:31 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>STrobo Maintaince Mode</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/node_modules/mdi/css/materialdesignicons.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/node_modules/perfect-scrollbar/dist/css/perfect-scrollbar.min.css">
    <!-- endinject -->
    <!-- plugin css for this page -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/node_modules/icheck/skins/all.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/node_modules/dropify/dist/css/dropify.min.css">
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/node_modules/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/custom.css">
    <!-- endinject -->
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/images/favicon.png" />
</head>
<body>
<div class="row">
    <div class="col-lg-12 text-center">
        <div class="grid-margin">
            <img src="${pageContext.request.contextPath}/images/logo.png">
        </div>
        <div class="content">
            <h2>${maintain.content}</h2>
        </div>
    </div>
</div>
</body>
</html>
