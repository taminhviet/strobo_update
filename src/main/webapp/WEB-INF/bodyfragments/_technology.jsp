<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
</head>
<body>
	<form id="formTechnology" name="formTechnology"
		action="/admin/savetechnology" enctype="multipart/form-data"
		method="post">
		<input type="hidden" name="technologyName" id="technologyName"
			value="technologyName">
		<div class="row grid-margin">
			<div class="col-12">
				<div class="card">
					<h2 class="card-header">Page Technology</h2>
					<div class="card-body">
						<div class="row">
							<div class="col-lg-6 col-sm-6 col-md-12">
								<label for="title">Title</label>
								<input type="text" class="form-control" name="title" id="title" placeholder="Title" value="${technology.title}">
								<label for="title">Heading1</label>
								<input type="text" class="form-control" name="heading1" id="heading1" placeholder="heading1" value="${technology.heading1}">
								<label for="title">Heading2</label>
								<input type="text" class="form-control" name="heading2" id="heading2" placeholder="heading2" value="${technology.heading2}">
							</div>
							<div class="col-lg-6 col-sm-6 col-md-12">
								<label for="title">Description1</label>
								<textarea class="form-control" type="text" name="description1" placeholder="Description1" rows="3">${technology.description1}</textarea>
								<label for="title">Description2</label>
								<textarea class="form-control" type="text" name="description2" placeholder="Description2" rows="3">${technology.description2}</textarea>
							</div>
						</div>
						<p class="text-center mt-4 mb-4">
							<button class="btn btn-primary" type="submit" id="saveformTechnology">Save</button>
						</p>
					</div>
				</div>
			</div>
		</div>
	</form>
	<c:if test="${not empty lsFeatures}">
		<div class="edit-feature">
			<c:forEach items="${lsFeatures}" var="feature">
			<form id="formfeature" name="formfeature" action="/admin/updatefeatureimage"
				enctype="multipart/form-data" method="POST">
				<input type="hidden" name ="id" id="id" value="${feature.id}">
				<div class="row grid-margin">
					<div class="col-12">
						<div class="card">
							<div class="card-body">
								<div class="row">
									<div class="col-lg-6 col-sm-6 col-md-12">
										<label for="featurename">Feature name</label>
										<input type="text" class="form-control" name="featurename" id="featurename" placeholder="Feature Name" value="${feature.featurename}" required >
									</div>
									<div class="col-lg-6 col-sm-6 col-md-12">
										<label for="image">Feature image</label>
										<input type="file" id="featureimage" class="dropify form-control" data-default-file="${pageContext.request.contextPath}/media/images/${feature.featureimage}" name="featureimage">
									</div>
								</div>
								<p class="text-center mt-4 mb-4">
									<button class="btn btn-primary" type="submit" id="saveFeature" name ="update">Update</button>
									<button class="btn btn-danger" type="submit" id="deleteFeature" name ="delete">Delete</button>
								</p>
							</div>
						</div>
					</div>
				</div>
			</form>
			</c:forEach>
		</div>
	</c:if>
	<div class="add-feature">
		<form id="formfeature" name="formfeature" action="/admin/featureimage"
			enctype="multipart/form-data" method="post">
			<div class="row grid-margin">
				<div class="col-12">
					<div class="card">
						<div class="card-body">
							<div class="row">
								<div class="col-lg-6 col-sm-6 col-md-12">
									<label for="featurename">Feature name</label>
									<input type="text" class="form-control" name="featurename" id="featurename" placeholder="Feature Name" value="">
								</div>
								<div class="col-lg-6 col-sm-6 col-md-12">
									<label for="image">Feature image</label>
									<input type="file" id="featureimage" class="dropify form-control" name="featureimage">
								</div>
							</div>
							<p class="text-center mt-4 mb-4">
								<button class="btn btn-primary" type="submit" id="saveFeature">Save</button>
							</p>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
	<div class="row col-12">
		<button class="btn ml-0 mr-0 btn-sm btn-success btn-add-feature" id-input="0"
			type="button">
			<i class="mdi mdi-plus"></i>
		</button>
	</div>
</body>
</html>