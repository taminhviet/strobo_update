<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<head>
<style type="text/css">

</style>
</head>
<body>
	<c:if test="${success == true}">
	<div class="alert alert-success">
		<strong>Welldone!</strong> Update successfully!
	</div>
	</c:if>
	<c:if test="${error == true}">
	<div class="alert alert-danger">
		<strong>Update failed, slide name is existed!</strong>
	</div>
	</c:if>
	<input type="hidden" name="success" id ="success" value="${success}">
	<input type="hidden" name="pagenamera" id ="pagenamera" value="${pagenamera}">
	<div class="row">
		<div class="container">
			<span class="choose">Choose Page</span>

			<div class="dropdown">
				<div class="select">
					<span>Page Airport</span> <i class="fa fa-chevron-down"></i>
				</div>
				<ul class="dropdown-menu">
					<li id="airport">Page Airport</li>
					<li id="seaport">Page Seaport</li>
					<li id="manufacturing">Page Manufacturing</li>
					<li id="warehouse">Page Warehouse</li>
				</ul>
			</div>

			<span class="msg"></span>
		</div>
	</div>
	<div id="pageAirport" class="formInputData">
		<jsp:include page="/WEB-INF/bodyfragments/_icAirport.jsp" />
	</div>
	<div id="pageManufacturing" class="formInputData">
		<jsp:include page="/WEB-INF/bodyfragments/_icManufacturing.jsp" />
	</div>
	<div id="pageWarehouse" class="formInputData">
		<jsp:include page="/WEB-INF/bodyfragments/_icWarehouse.jsp" />
	</div>
	<div id="pageSeaport" class="formInputData">
		<jsp:include page="/WEB-INF/bodyfragments/_icSeaport.jsp" />
	</div>

	<script type="text/javascript">
		$(document).ready(function() {
			var success = $('#success').attr('value');
			if(success == "true"){
				$('.alert').css('display', 'block');
				var pagenamera = $('#pagenamera').attr('value');
				var collapsepage = "collapse" + pagenamera;
				if(pagenamera.indexOf("pageairport") != -1){
					$('.dropdown').find('span').text("Page Airport");
					$('#pageAirport').css('display', 'block');
					$('#pageManufacturing').css('display', 'none');
					$('#pageWarehouse').css('display', 'none');
					$('#pageSeaport').css('display', 'none');
					$('#'+collapsepage).collapse('show');
				}
				if(pagenamera.indexOf("pageseaport") != -1){
					$('.dropdown').find('span').text("Page Seaport");
					$('#pageAirport').css('display', 'none');
					$('#pageManufacturing').css('display', 'none');
					$('#pageWarehouse').css('display', 'none');
					$('#pageSeaport').css('display', 'block');
					$('#'+collapsepage).collapse('show');
				}
				if(pagenamera.indexOf("pagemanufacturing") != -1){
					$('.dropdown').find('span').text("Page Manufacturing");
					$('#pageAirport').css('display', 'none');
					$('#pageManufacturing').css('display', 'block');
					$('#pageWarehouse').css('display', 'none');
					$('#pageSeaport').css('display', 'none');
					$('#'+collapsepage).collapse('show');
				}
				if(pagenamera.indexOf("pagewarehouse") != -1){
					$('.dropdown').find('span').text("Page Warehouse");
					$('#pageAirport').css('display', 'none');
					$('#pageManufacturing').css('display', 'none');
					$('#pageWarehouse').css('display', 'block');
					$('#pageSeaport').css('display', 'none');
					$('#'+collapsepage).collapse('show');
				}
			}else {
				$('.alert').css('display', 'none');
				$('#pageAirport').css('display', 'block');
				$('#pageManufacturing').css('display', 'none');
				$('#pageWarehouse').css('display', 'none');
				$('#pageSeaport').css('display', 'none');
			}
		});

		/*Dropdown Menu*/
		$('.dropdown').click(function() {
			$(this).attr('tabindex', 1).focus();
			$(this).toggleClass('active');
			$(this).find('.dropdown-menu').slideToggle(300);
		});
		$('.dropdown').focusout(function() {
			$(this).removeClass('active');
			$(this).find('.dropdown-menu').slideUp(300);
		});
		$('.dropdown .dropdown-menu li').click(
				function() {
					$(this).parents('.dropdown').find('span').text(
							$(this).text());
					$(this).parents('.dropdown').find('input').attr('value',
							$(this).attr('id'));
				});
		/*End Dropdown Menu*/

		$('.dropdown-menu li').click(function() {
			var id = $(this).attr('id');
			if (id == "airport") {
				$('#pageAirport').css('display', 'block');
				$('#pageManufacturing').css('display', 'none');
				$('#pageWarehouse').css('display', 'none');
				$('#pageSeaport').css('display', 'none');
			}
			if (id == "manufacturing") {
				$('#pageAirport').css('display', 'none');
				$('#pageManufacturing').css('display', 'block');
				$('#pageWarehouse').css('display', 'none');
				$('#pageSeaport').css('display', 'none');
			}
			if (id == "warehouse") {
				$('#pageAirport').css('display', 'none');
				$('#pageManufacturing').css('display', 'none');
				$('#pageWarehouse').css('display', 'block');
				$('#pageSeaport').css('display', 'none');
			}
			if (id == "seaport") {
				$('#pageAirport').css('display', 'none');
				$('#pageManufacturing').css('display', 'none');
				$('#pageWarehouse').css('display', 'none');
				$('#pageSeaport').css('display', 'block');
			}
		});
	</script>
</body>
</html>