<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:if test="${success == true}">
    <div class="alert alert-success">
        <strong>Welldone!</strong> Update successfully!
    </div>
</c:if>
<h1 class="page-title">Service</h1>
<c:set var="num_service" value="1" />
<c:forEach items="${listService}" var="items">
    <form method="POST" enctype="multipart/form-data">
        <input type="hidden" name="id" value="${items.id}">
        <div class="row grid-margin">
            <div class="col-12">
                <div class="card">
                    <h2 class="mb-0 card-header set-accordion">
                        <a class="c" id="head_title_${items.id}" data-toggle="collapse" data-parent="#accordion" href="#collapse${items.id}" aria-expanded="false" aria-controls="collapse${items.id}">
                            <c:if test="${not empty items.titleService}" >
                                ${items.titleService}
                            </c:if>
                            <c:if test="${empty items.titleService}" >
                                Service ${num_service}
                            </c:if>
                        </a>
                    </h2>
                    <div id="collapse${items.id}" class="collapse" role="tabpanel" aria-labelledby="heading${items.id}">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-6 col-sm-6 col-md-12">
                                    <div class="form-group form-inline">
                                        <div class="col-lg-3 col-sm-6 col-md-12">
                                            <label for="title_${items.id}">Title</label>
                                        </div>
                                        <div class="col-lg-9 col-sm-6 col-md-12">
                                            <input type="text" name="titleService" id="title_${items.id}" class="form-control title_input" data-heading="head_title_${items.id}" placeholder="Title" value="${items.titleService}" required>
                                        </div>
                                    </div>
                                    <div class="form-group form-inline">
                                        <div class="col-lg-3 col-sm-6 col-md-12">
                                            <label for="link_${items.id}">Video</label>
                                        </div>
                                        <div class="col-lg-9 col-sm-6 col-md-12">
                                            <input type="file" class="dropify form-control" id="linkvideo_${items.id}" name="backgroundvideo" data-default-file="${pageContext.request.contextPath}/media/video/seaport/${items.linkvideo}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-6 col-md-12">
                                    <label for="image_${items.id}">Image</label>
                                    <input type="file" class="dropify form-control" id="image_${items.id}" name="multipartFile" data-default-file="${pageContext.request.contextPath}/media/images/img_service/${items.imageService}">
                                </div>
                            </div>
                            <p class="text-center mb-0 mt-card"><button class="btn btn-primary" type="submit">Save</button></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <c:set var="num_service" value="${num_service + 1}" />
</c:forEach>
