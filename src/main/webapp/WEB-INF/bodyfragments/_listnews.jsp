<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link
	href="${pageContext.request.contextPath}/css/bootstrap/bootstrap.min.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/css/bootstrap/dataTables.bootstrap.min.css"
	rel="stylesheet">
	<link
	href="${pageContext.request.contextPath}/css/bootstrap/tablenews.css"
	rel="stylesheet">
</head>
<body>
<div class="row grid-margin">
	<div class="col-12">
		<div class="card">
			<h2 class="card-header">News & Events</h2>
			<div class="container">
				<table id="tablenews"
						class="table table-striped table-bordered table-sm"
						cellspacing="0" width="100%">
						<thead>
							<tr>
								<th class="th-sm"> Id <i class="fa fa-sort float-right" aria-hidden="true"></i></th>
								<th class="th-sm"> Title <i class="fa fa-sort float-right" aria-hidden="true"></i></th>
<!-- 								<th class="th-sm"> Feature Image <i class="fa fa-sort float-right" aria-hidden="true"></i></th> -->
								<th class="th-sm"> Created Date <i class="fa fa-sort float-right" aria-hidden="true"></i></th>
								<th class="th-sm"> Type <i class="fa fa-sort float-right" aria-hidden="true"></i></th>
								<th class="th-sm"> Action <i class="fa fa-sort float-right" aria-hidden="true"></i></th>
							</tr>
						</thead>
						<tbody>
							<c:if test="${not empty listNews}">
								<c:forEach items="${listNews}" var="news">
									<tr>
										<td>${news.id}</td>
										<td>${news.title}</td>
<%-- 										<td><img src="${pageContext.request.contextPath}/assets/${news.feature_image}"></td> --%>
										<td>${news.date_create}</td>
										<td>
											<c:if test="${news.type == 1}">Event</c:if>
											<c:if test="${news.type == 2}">News</c:if>
											<c:if test="${news.type == 3}">Press Release</c:if>
										</td>
										<td>
											<form id="formnews" name="formnews"
												action="/admin/deletenews" method="POST">
												<input type="hidden" name = "id" value ="${news.id}">
												<button class="btn btn-danger" type="submit"
														value="delete" name="delete">Delete</button>
											</form>
										</td>
									</tr>
								</c:forEach>
							</c:if>
						</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script src="${pageContext.request.contextPath}/js/strobo/tablenews.js"></script>
	<script
		src="${pageContext.request.contextPath}/js/strobo/bootstrap.min.js"></script>
	<script src="${pageContext.request.contextPath}/js/strobo/bootstable.js"></script>
	<script
		src="${pageContext.request.contextPath}/js/strobo/jquery.dataTables.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/js/strobo/dataTables.bootstrap.min.js"></script>
</body>
</html>