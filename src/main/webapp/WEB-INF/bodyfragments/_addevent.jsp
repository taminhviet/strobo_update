<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<head>
<link
	href="${pageContext.request.contextPath}/css/bootstrap/tablenews.css"
	rel="stylesheet">
</head>
<body>
<form action="/admin/saveevent" method="post" enctype="multipart/form-data"
	class="row col-12">
	<input type="hidden" name="type" value="1">
	<div class="col-12">
		<div class="form-group form-inline">
			<label>Title</label> <input type="text" name="title"
				class="form-control" required/>
		</div>
	</div>
	<div class="col-6">
		<div class="form-group form-inline">
			<label>Feature Image</label> <input type="file"
				class="form-control dropify" name="feature" required  />
		</div>
	</div>
	<div class="col-6">
		<div class="form-group form-inline">
			<label>Short Description</label>
			<textarea class="form-control" name="short_des" rows="5"></textarea>
		</div>
	</div>
	<div class="col-12">
		<div class="row">
		    <div class="col-sm-offset-2 col-sm-8"><br><br>
		<div class="form-group">
		<div class="input-group">
		  
		 </div><!-- group -->
		 </div><!-- form-group -->
		      <div class="form-group">
		<div class="input-group">
		  <input type="text" class="form-control" readonly>
		<div class="input-group-btn">
		  <span class="fileUpload btn btn-info">
		      <span class="upl" id="uploadMultiple">Upload Gallery</span>
		      <input type="file" name ="files" class="upload up" id="up" onchange="readURL(this);" multiple/>
		    </span><!-- btn-orange -->
		 </div><!-- btn -->
		 </div><!-- group -->
		 </div><!-- form-group -->
		      </div>
		    </div>
	</div>
	<div class="col-12">
		<div class="form-group form-inline">
			<label>Description</label>
			<textarea class="form-control" id="tinyMceExample" name="description"
				rows="5"></textarea>
		</div>
	</div>
	
	<div class="mt-2 mb-2 col-12" align="center">
		<button class="btn btn-primary" type="submit">Save</button>
	</div>
</form>
<script src="${pageContext.request.contextPath}/js/strobo/tablenews.js"></script>
</body>
