<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<body>
	<c:if test="${success == true}">
		<div class="alert alert-success">
			<strong>Welldone!</strong> Update successfully!
		</div>
	</c:if>
	<c:if test="${error == true}">
		<div class="alert alert-danger">
			<strong>Update failed, Feature name is existed!</strong> 
		</div>
	</c:if>
	<input type="hidden" name="success" id ="success" value="${success}">
	<input type="hidden" name="pagenamera" id ="pagenamera" value="${pagenamera}">
	<div class="row">
		<div class="container">
			<span class="choose">Choose Page</span>

			<div class="dropdown">
				<div class="select">
					<span>Page Core Technology</span> <i class="fa fa-chevron-left"></i>
				</div>
				<ul class="dropdown-menu">
					<li id="technology">Page Core Technology</li>
					<li id="robotic">Page Robotic</li>
					<li id="about">Page About us</li>
				</ul>
			</div>

			<span class="msg"></span>
		</div>
	</div>
	<div id="pageTechnology" class="formInputData">
		<jsp:include page="/WEB-INF/bodyfragments/_technology.jsp" />
	</div>
	<div id="pageRobotic" class="formInputData">
		<jsp:include page="/WEB-INF/bodyfragments/_robotic.jsp" />
	</div>
	<div id="pageAbout" class="formInputData">
		<jsp:include page="/WEB-INF/bodyfragments/_aboutus.jsp" />
	</div>
	<script type="text/javascript">
	$(document).ready(function() {
		var success = $('#success').attr('value');
		$('.add-feature').css('display', 'none');
		if(success == "true"){
			var pagenamera = $('#pagenamera').attr('value');
			if(pagenamera.indexOf("technologyName") != -1){
				$('.dropdown').find('span').text("Page Core Technology");
				$('#pageTechnology').css('display', 'block');
				$('#pageRobotic').css('display', 'none');
				$('#pageAbout').css('display', 'none');
			}
			if(pagenamera.indexOf("roboticname") != -1){
				$('.dropdown').find('span').text("Page Robotic");
				$('#pageTechnology').css('display', 'none');
				$('#pageRobotic').css('display', 'block');
				$('#pageAbout').css('display', 'none');
			}
			if(pagenamera.indexOf("aboutname") != -1){
				$('.dropdown').find('span').text("Page About");
				$('#pageTechnology').css('display', 'none');
				$('#pageRobotic').css('display', 'none');
				$('#pageAbout').css('display', 'block');
			}
		}else {
			$('#pageTechnology').css('display', 'block');
			$('#pageRobotic').css('display', 'none');
			$('#pageAbout').css('display', 'none');
		}
	});
	/*Dropdown Menu*/
	$('.dropdown').click(function() {
		$(this).attr('tabindex', 1).focus();
		$(this).toggleClass('active');
		$(this).find('.dropdown-menu').slideToggle(300);
	});
	$('.dropdown').focusout(function() {
		$(this).removeClass('active');
		$(this).find('.dropdown-menu').slideUp(300);
	});
	$('.dropdown .dropdown-menu li').click(
			function() {
				$(this).parents('.dropdown').find('span').text(
						$(this).text());
				$(this).parents('.dropdown').find('input').attr('value',
						$(this).attr('id'));
			});
	/*End Dropdown Menu*/
	$('.dropdown-menu li').click(function() {
		var id = $(this).attr('id');
		if (id == "technology") {
			$('#pageTechnology').css('display', 'block');
			$('#pageRobotic').css('display', 'none');
			$('#pageAbout').css('display', 'none');
		}
		if(id == "robotic" ){
			$('#pageTechnology').css('display', 'none');
			$('#pageRobotic').css('display', 'block');
			$('#pageAbout').css('display', 'none');
		}
		if(id == "about" ){
			$('#pageTechnology').css('display', 'none');
			$('#pageRobotic').css('display', 'none');
			$('#pageAbout').css('display', 'block');
		}
	});
	</script>
</body>
</html>