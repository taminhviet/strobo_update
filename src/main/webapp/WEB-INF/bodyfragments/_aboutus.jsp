<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
</head>
<body>
	<form id="formAbout" name="formAbout" action="/admin/saveabout"
		enctype="multipart/form-data" method="post">
		<input type="hidden" name="aboutname" id="aboutname" value="aboutname">
		<div class="row grid-margin">
			<div class="col-12">
				<div class="card">
					<h2 class="card-header">Page About us</h2>
					<div class="card-body">
						<div class="row">
							<div class="col-lg-6 col-sm-6 col-md-12">
								<label for="title">Title1</label>
								<input type="text" class="form-control" name="title1" id="title1" placeholder="Title1" value="${about.title1}">
								<label for="heading1">Heading1</label>
								<input type="text" class="form-control" name="heading1" id="heading1" placeholder="Heading1" value="${about.heading1}">
								<label for="title">Title2</label>
								<input type="text" class="form-control" name="title2" id="title2" placeholder="Title2" value="${about.title2}">
								<label for="heading2">Heading2</label>
								<input type="text" class="form-control" name="heading2" id="heading2" placeholder="Heading2" value="${about.heading2}">
								<label for="visit">Button Label</label>
								<input type="text" class="form-control" name="visit" id="visit" placeholder="Visit" value="${about.visit}">
							</div>
							<div class="col-lg-6 col-sm-6 col-md-12">
								<label for="description">Description1</label>
								<textarea class="form-control" type="text" name="description1" placeholder="Description1" rows="4">${about.description1}</textarea>
								<label for="heading2">Description2</label>
								<textarea class="form-control" type="text" name="description2" placeholder="Description2" rows="4">${about.description2}</textarea>
								<label for="description3">Description3</label>
								<textarea class="form-control" type="text" name="description3" placeholder="Description3" rows="4">${about.description3}</textarea>
								<label for="description4">Description4</label>
								<textarea class="form-control" type="text" name="description4" placeholder="Description4" rows="4">${about.description4}</textarea>
							</div>
						</div>
					</div>
					<p class="text-center">
						<button class="btn btn-primary" type="submit" id="saveAbout">Save</button>
					</p>
				</div>
			</div>
		</div>
	</form>
</body>
</html>