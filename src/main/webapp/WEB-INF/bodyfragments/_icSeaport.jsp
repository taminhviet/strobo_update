<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<style type="text/css">
.hide {
	display: none;
}
</style>
</head>
<body>
	<form id="formSeaport" name="formSeaport" action="/admin/secondlayer" enctype="multipart/form-data" method="post">
		<div class="row grid-margin">
			<div class="col-12">
				<div class="card">
					<h2 class="card-header">Page Seaport</h2>
					<div class="card-body">
						<div class="row">
							<div class="col-lg-6 col-sm-6 col-md-12">
								<input type="hidden" name="pagename" id="pagename" value="pageseaport">
								<label for="description">Description</label>
								<textarea class="form-control" name="pagecontent" id="pagecontent" placeholder="Description" rows="8">${pageseaport.pageContent}</textarea>
							</div>
							<div class="col-lg-6 col-sm-6 col-md-12">
								<label for="image">Image</label>
								<input type="file" id="fileImageLayer2" class="dropify form-control" data-default-file="${pageContext.request.contextPath}/media/images/${pageseaport.pageName}.png" name="fileImageLayer2">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<p class="text-center">
			<button class="btn btn-primary" type="submit" id="savepageSeaport">Save</button>
		</p>
	</form>

	<div class="row grid-margin">
		<div class="col-12">
			<div class="card">
				<h2 class="mb-0 card-header set-accordion">
					<c:if test="${not empty pageseaport1.content}">
						<a class="c" data-parent="#accordion" aria-expanded="false"
							href="#collapsepageseaport1" data-toggle="collapse">${pageseaport1.content}</a>
					</c:if>
					<c:if test="${empty pageseaport1.content}">
						<a class="c" data-parent="#accordion" aria-expanded="false"
							href="#collapsepageseaport1" data-toggle="collapse">Page
							Seaport1</a>
					</c:if>
				</h2>
				<div id="collapsepageseaport1" class="collapse">
					<form id="formImageContentSeaport1" name="formImageContentSeaport1"
						action="/admin/saveimagecontent" enctype="multipart/form-data"
						method="post">
						<div class="card-body">
							<div class="row">
								<div class="col-lg-6 col-sm-6 col-md-12">
									<input type="checkbox" name="hasslide" id="hasslideSeaport1" checked class="hide" />
									<input type="hidden" name="imagename" id="imagename" value="pageseaport1">
									<label for="title">Title</label>
									<input type="text" class="form-control" name="content" id="pagecontent" placeholder="Title" value="${pageseaport1.content}">
								</div>
								<div class="col-lg-6 col-sm-6 col-md-12">
									<label for="title">Main Image</label> <input type="file" class="dropify form-control" name="fileImageLayer3" id="fileImageLayer3" data-default-file="${pageContext.request.contextPath}/media/images/${pageseaport1.pageName}.png">
								</div>
								<div class="col-lg-6 col-sm-6 col-md-12">
									<div class="col-12">
										<label for="brochurefile">Brochure</label> <input
											type="file" name="brochurefile" id="brochurefile"
											data-default-file="${pageContext.request.contextPath}/documents/brochures/${pageseaport1.brochure}" class="dropify form-control"
											accept="">
									</div>
								</div>
								<!-- 								<div class="col-lg-6 col-sm-6 col-md-12"> -->
								<!-- 									<div class="col-12"> -->
								<!-- 										<label for="backgroundvideo">Background video</label> <input -->
								<!-- 											type="file" name="backgroundvideo" id="backgroundvideo" -->
								<%-- 											data-default-file="${pageContext.request.contextPath}/media/video/${pageseaport1.pageName}" class="dropify form-control" --%>
								<!-- 											accept="video/mp4"> -->
								<!-- 									</div> -->
								<!-- 								</div> -->
							</div>
							
							<div class="row grid-margin">
								<div class="col-12">
									<label for="tinyMceExample">Key Features</label>
									<textarea class="form-control" name="detailcontent" id="tinyMceExample" rows="3">${pageseaport1.detailContent}</textarea>
								</div>
							</div>
						</div>
						<p class="text-center">
							<button class="btn btn-primary" type="submit" id="saveformImageContentSeaport1">Save</button>
						</p>
					</form>
					<c:if test="${not empty pageseaport1.listSlide}">
						<div id="hasSlideGroupSeaport1" class="slideImage4 col-12">
							<c:forEach items="${pageseaport1.listSlide}" var="slide">
								<form id="formslideseaport1" class=" col-12" name="formslideseaport1" action="/admin/updateslide" enctype="multipart/form-data" method="POST">
									<input type="hidden" name="id" id="id" value="${slide.id}">
									<input type="hidden" name="formname" id="formname" value="${slide.slidepage}">
									<div class="row grid-margin">
										<div class="col-12">
											<div class="row grid-margin">
												<div class="col-lg-6 col-sm-6 col-md-12">
													<label for="Slide">Slide name</label>
													<input type="text" class="form-control" name="slidename" id="slidename" placeholder="Slide Name" value="${slide.slidename}" disabled="disabled">
												</div>
												<div class="col-lg-6 col-sm-6 col-md-12">
													<label for="image">Slide image</label>
													<input type="file" id="slideimage" class="dropify form-control" data-default-file="${pageContext.request.contextPath}/media/images/${slide.slideimg}" name="slideimage">
												</div>
											</div>
											<p class="text-center mb-4 mt-4">
												<button class="btn btn-primary" type="submit" id="saveSlide" name="update">Update</button>
												<button class="btn btn-danger" type="submit" id="deleteSlide" name="delete">Delete</button>
											</p>
										</div>
									</div>
								</form>
							</c:forEach>
						</div>
					</c:if>
					<div class="add-slide col-12" id="add-slide-seaport1">
						<form id="formslide" class=" col-12" name="formslide" action="/admin/saveslide" enctype="multipart/form-data" method="post">
							<input type="hidden" name="formname" id="formname" value="pageseaport1">
							<div class="row grid-margin">
								<div class="col-12">
									<div class="row grid-margin">
										<div class="col-lg-6 col-sm-6 col-md-12">
											<label for="slide">Slide name</label>
											<input type="text" class="form-control" name="slidename" id="slidename" placeholder="Slide Name" value="" required>
										</div>
										<div class="col-lg-6 col-sm-6 col-md-12">
											<label for="image">Slide image</label>
											<input type="file" id="slideimage" class="dropify form-control" data-default-file="" name="slideimage">
										</div>
									</div>
									<p class="text-center mb-4 mt-4">
										<button class="btn btn-primary" type="submit" id="saveSlide">Save</button>
									</p>
								</div>
							</div>
						</form>
					</div>
					<div class="col-12 mb-4">
						<button class="btn btn-sm btn-success btn-add-slide" title = "Add new slide" type="button"
							id="add-slide-seaport1">
							<i class="mdi mdi-plus"></i>
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row grid-margin">
		<div class="col-12">
			<div class="card">
				<h2 class="mb-0 card-header set-accordion">
					<c:if test="${not empty pageseaport2.content}">
						<a class="c" data-parent="#accordion" aria-expanded="false"
							href="#collapsepageseaport2" data-toggle="collapse">${pageseaport2.content}</a>
					</c:if>
					<c:if test="${empty pageseaport2.content}">
						<a class="c" data-parent="#accordion" aria-expanded="false"
							href="#collapsepageseaport2" data-toggle="collapse">Page
							Seaport2</a>
					</c:if>
				</h2>
				<div id="collapsepageseaport2" class="collapse">
					<form id="formImageContentSeaport2" name="formImageContentSeaport2"
						action="/admin/saveimagecontent" enctype="multipart/form-data"
						method="post">
						<div class="card-body">
							<div class="row">
								<div class="col-lg-6 col-sm-6 col-md-12">
									<input type="hidden" name="imagename" id="imagename"  value="pageseaport2">
									<input type="checkbox" name="hasslide" id="hasslideSeaport2" class="hide" checked />
									<label for="title">Title</label>
									<input type="text" class="form-control" name="content" id="pagecontent" placeholder="Title" value="${pageseaport2.content}">
								</div>
								<div class="col-lg-6 col-sm-6 col-md-12">
									<label for="title">Main Image</label> <input type="file" class="dropify form-control" name="fileImageLayer3" id="fileImageLayer3" data-default-file="${pageContext.request.contextPath}/media/images/${pageseaport2.pageName}.png">
								</div>
								<div class="col-lg-6 col-sm-6 col-md-12">
									<div class="col-12">
										<label for="brochurefile">Brochure</label> <input
											type="file" name="brochurefile" id="brochurefile"
											data-default-file="${pageContext.request.contextPath}/documents/brochures/${pageseaport2.brochure}" class="dropify form-control"
											accept="">
									</div>
								</div>
								<!-- 								<div class="col-lg-6 col-sm-6 col-md-12"> -->
								<!-- 									<div class="col-12"> -->
								<!-- 										<label for="backgroundvideo">Background video</label> <input -->
								<!-- 											type="file" name="backgroundvideo" id="backgroundvideo" -->
								<%-- 											data-default-file="${pageContext.request.contextPath}/media/video/${pageseaport2.pageName}" class="dropify form-control" --%>
								<!-- 											accept="video/mp4"> -->
								<!-- 									</div> -->
								<!-- 								</div> -->
							</div>
							
							<div class="row grid-margin">
								<div class="col-12">
									<label for="tinyMceExample">Key Features</label>
									<textarea class="form-control" name="detailcontent" id="tinyMceExample" rows="3">${pageseaport2.detailContent}</textarea>
								</div>
							</div>
						</div>
						<p class="text-center">
							<button class="btn btn-primary" type="submit" id="saveformImageContentSeaport2">Save</button>
						</p>
					</form>
					<c:if test="${not empty pageseaport2.listSlide}">
						<div id="hasSlideGroupSeaport2" class="slideImage4 col-12">
							<c:forEach items="${pageseaport2.listSlide}" var="slide">
								<form id="formslideseaport2" name="formslideseaport2" action="/admin/updateslide" enctype="multipart/form-data" method="POST" class="col-12">
									<input type="hidden" name="id" id="id" value="${slide.id}">
									<input type="hidden" name="formname" id="formname" value="${slide.slidepage}">
									<div class="row grid-margin">
										<div class="col-12">
											<div class="row grid-margin">
												<div class="col-lg-6 col-sm-6 col-md-12">
													<label for="Slide">Slide name</label>
													<input type="text" class="form-control" name="slidename" id="slidename" placeholder="Slide Name" value="${slide.slidename}" disabled="disabled">
												</div>
												<div class="col-lg-6 col-sm-6 col-md-12">
													<label for="image">Slide image</label>
													<input type="file" id="slideimage" class="dropify form-control" data-default-file="${pageContext.request.contextPath}/media/images/${slide.slideimg}" name="slideimage">
												</div>
											</div>
											<p class="text-center mb-4 mt-4">
												<button class="btn btn-primary" type="submit"
													id="saveSlide" name="update">Update</button>
												<button class="btn btn-danger" type="submit"
													id="deleteSlide" name="delete">Delete</button>
											</p>
										</div>
									</div>
								</form>
							</c:forEach>
						</div>
					</c:if>
					<div class="add-slide col-12" id="add-slide-seaport2">
						<form id="formslide" name="formslide" action="/admin/saveslide" enctype="multipart/form-data" method="post" class="col-12">
							<input type="hidden" name="formname" id="formname" value="pageseaport2">
							<div class="row grid-margin">
								<div class="col-12">
									<div class="row grid-margin">
										<div class="col-lg-6 col-sm-6 col-md-12">
											<label for="slide">Slide name</label>
											<input type="text" class="form-control" name="slidename" id="slidename" placeholder="Slide Name" value="" required>
										</div>
										<div class="col-lg-6 col-sm-6 col-md-12">
											<label for="image">Slide image</label>
											<input type="file" id="slideimage" class="dropify form-control" name="slideimage">
										</div>
									</div>
									<p class="text-center mb-4 mt-4">
										<button class="btn btn-primary" type="submit" id="saveSlide">Save</button>
									</p>
								</div>
							</div>
						</form>
					</div>
					<div class="col-12 mb-4">
						<button class="btn btn-sm btn-success btn-add-slide" title = "Add new slide" type="button" id="add-slide-seaport2">
							<i class="mdi mdi-plus"></i>
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row grid-margin">
		<div class="col-12">
			<div class="card">
				<h2 class="mb-0 card-header set-accordion">
					<c:if test="${not empty pageseaport3.content}">
						<a class="c" data-parent="#accordion" aria-expanded="false"
							href="#collapsepageseaport3" data-toggle="collapse">${pageseaport3.content}</a>
					</c:if>
					<c:if test="${empty pageseaport3.content}">
						<a class="c" data-parent="#accordion" aria-expanded="false"
							href="#collapsepageseaport3" data-toggle="collapse">Page
							Seaport3</a>
					</c:if>
				</h2>
				<div id="collapsepageseaport3" class="collapse">
					<form id="formImageContentSeaport3" name="formImageContentSeaport3"
						action="/admin/saveimagecontent" enctype="multipart/form-data"
						method="post">
						<div class="card-body">
							<div class="row">
								<div class="col-lg-6 col-sm-6 col-md-12">
									<input type="hidden" name="imagename" id="imagename" value="pageseaport3">
									<input type="checkbox" name="hasslide" id="hasslideSeaport3" checked class="hide">
									<label for="title">Title</label>
									<input type="text" class="form-control" name="content" id="pagecontent" placeholder="Title" value="${pageseaport3.content}">
								</div>
								<div class="col-lg-6 col-sm-6 col-md-12">
									<label for="title">Main Image</label>
									<input type="file" class="dropify form-control" name="fileImageLayer3" id="fileImageLayer3" data-default-file="${pageContext.request.contextPath}/media/images/${pageseaport3.pageName}.png">
								</div>
								<div class="col-lg-6 col-sm-6 col-md-12">
									<div class="col-12">
										<label for="brochurefile">Brochure</label> <input
											type="file" name="brochurefile" id="brochurefile"
											data-default-file="${pageContext.request.contextPath}/documents/brochures/${pageseaport3.brochure}" class="dropify form-control"
											accept="">
									</div>
								</div>
								<!-- 								<div class="col-lg-6 col-sm-6 col-md-12"> -->
								<!-- 									<div class="col-12"> -->
								<!-- 										<label for="backgroundvideo">Background video</label> <input -->
								<!-- 											type="file" name="backgroundvideo" id="backgroundvideo" -->
								<%-- 											data-default-file="${pageContext.request.contextPath}/media/video/${pageseaport3.pageName}" class="dropify form-control" --%>
								<!-- 											accept="video/mp4"> -->
								<!-- 									</div> -->
								<!-- 								</div> -->
							</div>
							
							<div class="row grid-margin">
								<div class="col-12">
									<label for="tinyMceExample">Key Features</label>
									<textarea class="form-control" name="detailcontent"
										id="tinyMceExample" rows="3">${pageseaport3.detailContent}</textarea>
								</div>
							</div>
						</div>
						<p class="text-center">
							<button class="btn btn-primary" type="submit"
								id="saveformImageContentSeaport3">Save</button>
						</p>
					</form>
					<c:if test="${not empty pageseaport3.listSlide}">
						<div id="hasSlideGroupSeaport3" class="slideImage4 col-12">
							<c:forEach items="${pageseaport3.listSlide}" var="slide">
								<form id="formslideseaport3" name="formslideseaport3" action="/admin/updateslide" enctype="multipart/form-data" method="POST" class="col-12">
									<input type="hidden" name="id" id="id" value="${slide.id}">
									<input type="hidden" name="formname" id="formname" value="${slide.slidepage}">
									<div class="row grid-margin">
										<div class="col-12">
											<div class="row grid-margin">
												<div class="col-lg-6 col-sm-6 col-md-12">
													<label for="Slide">Slide name</label>
													<input type="text" class="form-control" name="slidename" id="slidename" placeholder="Slide Name" value="${slide.slidename}" disabled="disabled">
												</div>
												<div class="col-lg-6 col-sm-6 col-md-12">
													<label for="image">Slide image</label>
													<input type="file" id="slideimage" class="dropify form-control" data-default-file="${pageContext.request.contextPath}/media/images/${slide.slideimg}" name="slideimage">\
												</div>
											</div>
											<p class="text-center mb-4 mt-4">
												<button class="btn btn-primary" type="submit" id="saveSlide" name="update">Update</button>
												<button class="btn btn-danger" type="submit" id="deleteSlide" name="delete">Delete</button>
											</p>
										</div>
									</div>
								</form>
							</c:forEach>
						</div>
					</c:if>
					<div class="add-slide col-12" id="add-slide-seaport3">
						<form id="formslide" name="formslide" action="/admin/saveslide" enctype="multipart/form-data" method="post" class="col-12">
							<input type="hidden" name="formname" id="formname" value="pageseaport3">
							<div class="row grid-margin">
								<div class="col-12">
									<div class="row grid-margin">
										<div class="col-lg-6 col-sm-6 col-md-12">
											<label for="slide">Slide name</label>
											<input type="text" class="form-control" name="slidename" id="slidename" placeholder="Slide Name" value="" required>
										</div>
										<div class="col-lg-6 col-sm-6 col-md-12">
											<label for="image">Slide image</label>
											<input type="file" id="slideimage" class="dropify form-control" data-default-file="" name="slideimage">
										</div>
									</div>
									<p class="text-center mb-4 mt-4">
										<button class="btn btn-primary" type="submit" id="saveSlide">Save</button>
									</p>
								</div>
							</div>
						</form>
					</div>
					<div class="col-12 mb-4">
						<button class="btn btn-sm btn-success btn-add-slide" title = "Add new slide" type="button"
							id="add-slide-seaport3">
							<i class="mdi mdi-plus"></i>
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		var hasSlideGroupSeaport1 = document
				.getElementById("hasSlideGroupSeaport1");
		var hasSlideGroupSeaport2 = document
				.getElementById("hasSlideGroupSeaport2");
		var hasSlideGroupSeaport3 = document
				.getElementById("hasSlideGroupSeaport3");

		$(document).ready(function() {
			$('#add-slide-seaport1').css('display', 'none');
			$('#add-slide-seaport2').css('display', 'none');
			$('#add-slide-seaport3').css('display', 'none');
		});
		function hasSlideSeaport() {
			if (document.getElementById("hasslideSeaport1").checked) {
				hasSlideGroupSeaport1.style.display = "block";
			} else {
				hasSlideGroupSeaport1.style.display = "none";
			}

			if (document.getElementById("hasslideSeaport2").checked) {
				hasSlideGroupSeaport2.style.display = "block";
			} else {
				hasSlideGroupSeaport2.style.display = "none";
			}
			if (document.getElementById("hasslideSeaport3").checked) {
				hasSlideGroupSeaport3.style.display = "block";
			} else {
				hasSlideGroupSeaport3.style.display = "none";
			}
		}
	</script>
</body>
</html>