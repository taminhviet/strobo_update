<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:if test="${success == true}">
    <div class="alert alert-success">
        <strong>Welldone!</strong> Update successfully!
    </div>
</c:if>
<h1 class="page-title">Manage News Page</h1>
<jsp:include page="/WEB-INF/bodyfragments/_listnews.jsp" />
<div class="row grid-margin col-12">
    <button class="btn btn-sm btn-success btn-add-new" id-input="0" type="button" style=""><i class="mdi mdi-plus"></i></button>
</div>
<div class="row grid-margin news add-new">
    <div class="col-12">
        <div class="card">
            <h2 class="card-header">News Page <button class="btn btn-sm btn-danger btn-close-new" id-input="0" type="button" style=""><i class="mdi mdi-minus"></i></button></h2>
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <div class="form-group form-inline">
                            <select class="new_type form-control" name="type">
                                <option data-select="event" value="event">Events</option>
                                <option data-select="article" value="article">News Article</option>
                                <option data-select="press" value="press">Press Release</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row event">
                    <jsp:include page="/WEB-INF/bodyfragments/_addevent.jsp" />
                </div>
                <div class="row article">
                	<jsp:include page="/WEB-INF/bodyfragments/_addarticle.jsp" />
                </div>
                <div class="row press">
                	<jsp:include page="/WEB-INF/bodyfragments/_addpress.jsp" />
                </div>
            </div>
        </div>
    </div>
</div>
