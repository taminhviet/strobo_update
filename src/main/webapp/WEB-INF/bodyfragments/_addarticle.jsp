<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<head>
	<link
	href="${pageContext.request.contextPath}/css/bootstrap/tablenews.css"
	rel="stylesheet">
</head>
<body>
<form action="/admin/savearticle" method="post" enctype="multipart/form-data"
	class="row col-12">
	<input type="hidden" name="type" value="2"> 
	<div class="col-12">
		<div class="form-group form-inline">
			<label>Title</label> <input type="text" name="title"
				class="form-control" required/>
		</div>
	</div>
	<div class="col-6">
		<div class="form-group form-inline">
			<label>Feature Image</label> <input type="file"
				class="form-control dropify" name="feature" required  />
		</div>
	</div>
	<div class="col-6">
		<div class="form-group form-inline">
			<label>Short Description</label>
			<textarea class="form-control" name="short_des" rows="5"></textarea>
		</div>
	</div>
	<div class="col-12">
		<div class="form-group form-inline">
			<label>Description</label>
<!-- 			<textarea class="form-control" id="tinyMceExample" name="description" -->
<!-- 				rows="5"></textarea> -->
				<textarea name="description" id="description" cols="30" rows="30"></textarea>
				
				 <input name="image" type="file" id="upload" class="hidden" onchange="">
		</div>
	</div>
	
	<div class="mt-2 mb-2 col-12" align="center">
		<button class="btn btn-primary" type="submit">Save</button>
	</div>
</form>
<script src="${pageContext.request.contextPath}/js/strobo/tablenews.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	  tinymce.init({
	    selector: "textarea",
	    theme: "modern",
	    paste_data_images: true,
	    plugins: [
	      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
	      "searchreplace wordcount visualblocks visualchars code fullscreen",
	      "insertdatetime media nonbreaking save table contextmenu directionality",
	      "emoticons template paste textcolor colorpicker textpattern"
	    ],
	    toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
	    toolbar2: "print preview media | forecolor backcolor emoticons",
	    image_advtab: true,
	    file_picker_callback: function(callback, value, meta) {
	      if (meta.filetype == 'image') {
	        $('#upload').trigger('click');
	        $('#upload').on('change', function(editor) {
	          var file = this.files[0];
	          var data = new FormData();
              data.append('files', file);
              $.ajax({
                  url: '${pageContext.request.contextPath}/admin/images',
                  type: 'POST',
                  data: data,
                  enctype: 'multipart/form-data',
                  dataType : 'text',
                  processData: false, // Don't process the files
                  contentType: false, // Set content type to false as jQuery will tell the server its a query string request
                  success: function(data, textStatus, jqXHR) {
                	  tinyMCE.activeEditor.insertContent('<img class="content-img" src="' + data + '" data-mce-src="' + data + '" />');
                  },
                  error: function(jqXHR, textStatus, errorThrown) {
                  }
                });
	        });
	      }
	    },
	    templates: [{
	      title: 'Test template 1',
	      content: 'Test 1'
	    }, {
	      title: 'Test template 2',
	      content: 'Test 2'
	    }]
	  });
	});
</script>
</body>
