<%--
  Created by IntelliJ IDEA.
  User: My Kavalli
  Date: 07/09/2018
  Time: 10:40 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <title>Gerate Page Content</title>
</head>
<body>
<c:if test="${success == true}">
    <div class="alert alert-success">
        <strong>Welldone!</strong> Update successfully!
    </div>
</c:if>

<h1 class="page-title">Page Generate</h1>
<div class="row grid-margin">
    <div class="col-12"><button class="btn btn-addnew btn-success">Add New Page</button></div>
</div>
<form method="post" enctype="multipart/form-data" action="/admin/generate" class="form-addnew" >
    <div class="row grid-margin">
        <div class="col-12">
            <div class="card">
                <h2 class="mb-0 card-header set-accordion">
                    <a class="c add-new" id="head_title_0" data-toggle="collapse" data-parent="#accordion" href="#collapse0" aria-expanded="false" aria-controls="collapse0">
                        Add New Page
                    </a>
                </h2>
                <div id="collapse0" class="collapse" role="tabpanel" aria-labelledby="heading0">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-6 col-sm-6 col-md-12">
                                <div class="form-group form-inline">
                                    <div class="col-lg-3 col-sm-6 col-md-12">
                                        <label for="title">Title</label>
                                    </div>
                                    <div class="col-lg-9 col-sm-6 col-md-12">
                                        <input type="text" class="form-control" name="title" id="title" placeholder="Title" value="" required>
                                    </div>
                                </div>
                                <div class="form-group form-inline">
                                    <div class="col-lg-3 col-sm-6 col-md-12">
                                        <label for="pageName">Page Name</label>
                                    </div>
                                    <div class="col-lg-9 col-sm-6 col-md-12">
                                        <input type="text" class="form-control" name="pageName" id="pageName" placeholder="Page Name" value="">
                                    </div>
                                </div>
                                <div class="form-group form-inline">
                                    <div class="col-lg-3 col-sm-6 col-md-12">
                                        <label for="meta">Meta</label>
                                    </div>
                                    <div class="col-lg-9 col-sm-6 col-md-12">
                                        <textarea type="text" class="form-control" name="meta" rows="6" id="meta" placeholder="Meta" ></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-sm-6 col-md-12">
                                <div class="col-lg-12">
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input type="checkbox" name="status"   checked  class="form-check-input" >
                                            Enable
                                        </label>

                                    </div>
                                </div>
                                <div class="form-group form-inline">
                                    <div class="col-lg-3 col-sm-6 col-md-12">
                                        <label for="createdDate">Date Create</label>
                                    </div>
                                    <div class="col-lg-9 col-sm-6 col-md-12">
                                        <input type="text" class="form-control" id="createdDate" placeholder="Date Create" disabled value="">
                                    </div>
                                </div>
                                <div class="form-group form-inline">
                                    <div class="col-lg-3 col-sm-6 col-md-12">
                                        <label for="keyword">KeyWord</label>
                                    </div>
                                    <div class="col-lg-9 col-sm-6 col-md-12">
                                        <textarea type="text" class="form-control" name="keyword" rows="6" id="keyword" placeholder="Keyword"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="col-12">
                                    <div class="form-group form-inline">
                                        <label for="alias_title">Alias</label>
                                        <input required type="text" name="alias" id="alias_title" class="form-control" placeholder="Alias" value="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="col-12">
                                    <label for="tinyMceExample">Alias Content</label>
                                    <textarea type="text" name="pageContent" id="tinyMceExample" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
                        <p class="text-center mb-0 mt-card"><button class="btn btn-primary" type="submit">Save</button></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<!--Foreach page created-->
<c:forEach items="${listPage}" var="page">
<form method="post" enctype="multipart/form-data" action="/admin/generate" >
<input type="hidden" name="id" value="${page.id}">
    <div class="row grid-margin">
        <div class="col-12">
            <div class="card">
                <h2 class="mb-0 card-header set-accordion">
                    <a class="c" id="head_title_${page.id}" data-toggle="collapse" data-parent="#accordion" href="#collapse${page.id}" aria-expanded="false" aria-controls="collapse${page.id}">
                        ${page.title}
                    </a>
                    <span data-id ="${page.id}" class="btn btn-sm btn-danger btn-delete-generate fl-right" ><i class="fa fa-times"></i></span>
                    <a target="_blank" href="/${page.alias}" class="h5 fl-right text-info  mr-2 mb-0">Preview</a>
                </h2>
                <div id="collapse${page.id}" class="collapse" role="tabpanel" aria-labelledby="heading${page.id}">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-6 col-sm-6 col-md-12">
                                <div class="form-group form-inline">
                                    <div class="col-lg-3 col-sm-6 col-md-12">
                                        <label for="title">Title</label>
                                    </div>
                                    <div class="col-lg-9 col-sm-6 col-md-12">
                                        <input type="text" class="form-control title_input" data-heading="head_title_${page.id}" name="title" id="title" placeholder="Title" value="${page.title}" required>
                                    </div>
                                </div>
                                <div class="form-group form-inline">
                                    <div class="col-lg-3 col-sm-6 col-md-12">
                                        <label for="pageName">Page Name</label>
                                    </div>
                                    <div class="col-lg-9 col-sm-6 col-md-12">
                                        <input type="text" class="form-control" name="pageName" id="pageName" placeholder="Page Name" value="${page.pageName}">
                                    </div>
                                </div>
                                <div class="form-group form-inline">
                                    <div class="col-lg-3 col-sm-6 col-md-12">
                                        <label for="meta">Meta</label>
                                    </div>
                                    <div class="col-lg-9 col-sm-6 col-md-12">
                                        <textarea type="text" class="form-control" name="meta" rows="6" id="meta" placeholder="Meta" >${page.meta}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-sm-6 col-md-12">
                                <div class="col-lg-12">
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input type="checkbox" name="status"  <c:if test="${page.status == true}" > checked </c:if> class="form-check-input" >
                                            Enable
                                        </label>

                                    </div>
                                </div>
                                <div class="form-group form-inline">
                                    <div class="col-lg-3 col-sm-6 col-md-12">
                                        <label for="createdDate">Date Create</label>
                                    </div>
                                    <div class="col-lg-9 col-sm-6 col-md-12">
                                        <input type="text" class="form-control"  name="createdDate" id="createdDate" placeholder="Date Create" disabled value="${page.createdDate}">
                                    </div>
                                </div>
                                <div class="form-group form-inline">
                                    <div class="col-lg-3 col-sm-6 col-md-12">
                                        <label for="keyword">KeyWord</label>
                                    </div>
                                    <div class="col-lg-9 col-sm-6 col-md-12">
                                        <textarea type="text" class="form-control" name="keyword" rows="6" id="keyword" placeholder="Keyword">${page.keyword}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="col-12">
                                    <div class="form-group form-inline">
                                        <label for="alias_title">Alias</label>
                                        <input type="text" name="alias" id="alias_title" class="form-control" placeholder="Alias" value="${page.alias}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="col-12">
                                    <label for="tinyMceExample${page.id}">Alias Content</label>
                                    <textarea type="text" name="pageContent" id="tinyMceExample${page.id}" class="form-control">${page.pageContent}</textarea>
                                </div>
                            </div>
                        </div>
                        <p class="text-center mb-0 mt-card"><button class="btn btn-primary" type="submit">Save</button></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
</c:forEach>
</body>
</html>
