<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<style type="text/css">
.hide {
	display: none;
}
</style>
</head>
<body>
	<form id="formWarehouse" name="formWarehouse"
		action="/admin/secondlayer" enctype="multipart/form-data"
		method="post">
		<div class="row grid-margin">
			<div class="col-12">
				<div class="card">
					<h2 class="card-header">Page Warehouse</h2>
					<div class="card-body">
						<div class="row">
							<div class="col-lg-6 col-sm-6 col-md-12">
								<input type="hidden" name="pagename" id="pagename" value="pagewarehouse">
								<label for="description">Description</label>
								<input type="text" class="form-control" name="pagecontent" id="pagecontent" placeholder="Description" value="${pagewarehouse.pageContent}">
							</div>
							<div class="col-lg-6 col-sm-6 col-md-12">
								<label for="image">Image</label>
								<input type="file" id="fileImageLayer2" class="dropify form-control" data-default-file="${pageContext.request.contextPath}/media/images/${pagewarehouse.pageName}.png" name="fileImageLayer2">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<p class="text-center">
			<button class="btn btn-primary" type="submit" id="savepageWarehouse">Save</button>
		</p>
	</form>

	<div class="row grid-margin">
		<div class="col-12">
			<div class="card">
				<h2 class="mb-0 card-header set-accordion">
					<c:if test="${not empty pagewarehouse1.content}">
						<a class="c" data-parent="#accordion" aria-expanded="false"
							href="#collapsepagewarehouse1" data-toggle="collapse">${pagewarehouse1.content}</a>
					</c:if>
					<c:if test="${empty pagewarehouse1.content}">
						<a class="c" data-parent="#accordion" aria-expanded="false"
							href="#collapsepagewarehouse1" data-toggle="collapse">Page
							Warehouse1</a>
					</c:if>
				</h2>
				<div id="collapsepagewarehouse1" class="collapse">
					<form id="formImageContentWarehouse1"
						name="formImageContentWarehouse1" action="/admin/saveimagecontent"
						enctype="multipart/form-data" method="post">
						<div class="card-body">
							<div class="row">
								<div class="col-lg-6 col-sm-6 col-md-12">
									<input type="hidden" name="imagename" id="imagename" value="pagewarehouse1">
									<input type="checkbox" name="hasslide" id="hasslideWarehouse1" checked class="hide" />
									<label for="title">Title</label>
									<input type="text" class="form-control" name="content" id="pagecontent" placeholder="Title" value="${pagewarehouse1.content}">
								</div>
								<div class="col-lg-6 col-sm-6 col-md-12">
									<label for="title">Main Image</label> <input type="file"
										class="dropify form-control" name="fileImageLayer3"
										id="fileImageLayer3"
										data-default-file="${pageContext.request.contextPath}/media/images/${pagewarehouse1.pageName}.png">
								</div>
								<div class="col-lg-6 col-sm-6 col-md-12">
									<div class="col-12">
										<label for="brochurefile">Brochure</label> <input
											type="file" name="brochurefile" id="brochurefile"
											data-default-file="${pageContext.request.contextPath}/documents/brochures/${pagewarehouse1.brochure}" class="dropify form-control"
											accept="">
									</div>
								</div>
								<!-- 								<div class="col-lg-6 col-sm-6 col-md-12"> -->
								<!-- 									<div class="col-12"> -->
								<!-- 										<label for="backgroundvideo">Background video</label> <input -->
								<!-- 											type="file" name="backgroundvideo" id="backgroundvideo" -->
								<%-- 											data-default-file="${pageContext.request.contextPath}/media/video/${pagewarehouse1.pageName}" class="dropify form-control" --%>
								<!-- 											accept="video/mp4"> -->
								<!-- 									</div> -->
								<!-- 								</div> -->
							</div>
							
							<div class="row grid-margin">
								<div class="col-12">
									<label for="tinyMceExample">Key Features</label>
									<textarea class="form-control" type="text" name="detailcontent"
										id="tinyMceExample" rows="3">${pagewarehouse1.detailContent}</textarea>
								</div>
							</div>
						</div>
						<p class="text-center">
							<button class="btn btn-primary" type="submit"
								id="saveformImageContentWarehouse1">Save</button>
						</p>
					</form>
					<c:if test="${not empty pagewarehouse1.listSlide}">
						<div id="hasSlideGroupWarehouse1" class="slideImage4 col-12">
							<c:forEach items="${pagewarehouse1.listSlide}" var="slide">
								<form id="formslidewarehouse1" name="formslidewarehouse1"
									action="/admin/updateslide" enctype="multipart/form-data"
									method="POST" class="col-12">
									<input type="hidden" name="id" id="id" value="${slide.id}">
									<input type="hidden" name="formname" id="formname"
										value="${slide.slidepage}">
									<div class="row grid-margin">
										<div class="col-12">
											<div class="row grid-margin">
												<div class="col-lg-6 col-sm-6 col-md-12">
													<label for="Slide">Slide name</label>
													<input type="text" class="form-control" name="slidename" id="slidename" placeholder="Slide Name" value="${slide.slidename}" >
												</div>
												<div class="col-lg-6 col-sm-6 col-md-12">
													<label for="image">Slide image</label>
													<input type="file" id="slideimage" class="dropify form-control" data-default-file="${pageContext.request.contextPath}/media/images/${slide.slideimg}" name="slideimage">
												</div>
											</div>
											<p class="text-center mb-4 mt-4">
												<button class="btn btn-primary" type="submit"
													id="saveSlide" name="update">Update</button>
												<button class="btn btn-danger" type="submit"
													id="deleteSlide" name="delete">Delete</button>
											</p>
										</div>
									</div>
								</form>
							</c:forEach>
						</div>
					</c:if>
					<div class="add-slide col-12" id="add-slide-warehouse1">
						<form id="formslide" name="formslide" action="/admin/saveslide"
							enctype="multipart/form-data" method="post">
							<input type="hidden" name="formname" id="formname"
								value="pagewarehouse1">
							<div class="row grid-margin">
								<div class="col-12">
									<div class="row grid-margin">
										<div class="col-lg-6 col-sm-6 col-md-12">
											<label for="slide">Slide name</label>
											<input type="text" class="form-control" name="slidename" id="slidename" placeholder="Slide Name" value="" required>
										</div>
										<div class="col-lg-6 col-sm-6 col-md-12">
											<label for="image">Slide image</label>
											<input type="file" id="slideimage" class="dropify form-control" data-default-file="" name="slideimage">
										</div>
									</div>
									<p class="text-center">
										<button class="btn btn-primary" type="submit" id="saveSlide">Save</button>
									</p>
								</div>
							</div>
						</form>
					</div>
					<div class="col-12 mb-4">
						<button class="btn btn-sm btn-success btn-add-slide" title = "Add new slide" title = "Add new slide" type="button"
							id="add-slide-warehouse1">
							<i class="mdi mdi-plus"></i>
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>


	<div class="row grid-margin">
		<div class="col-12">
			<div class="card">
				<h2 class="mb-0 card-header set-accordion">
					<c:if test="${not empty pagewarehouse2.content}">
						<a class="c" data-parent="#accordion" aria-expanded="false"
							href="#collapsepagewarehouse2" data-toggle="collapse">${pagewarehouse2.content}</a>
					</c:if>
					<c:if test="${empty pagewarehouse2.content}">
						<a class="c" data-parent="#accordion" aria-expanded="false"
							href="#collapsepagewarehouse2" data-toggle="collapse">Page
							Warehouse2</a>
					</c:if>
				</h2>
				<div id="collapsepagewarehouse2" class="collapse">
					<form id="formImageContentWarehouse2"
						name="formImageContentWarehouse2" action="/admin/saveimagecontent"
						enctype="multipart/form-data" method="post">
						<div class="card-body">
							<div class="row grid-margin">
								<div class="col-lg-6 col-sm-6 col-md-12">
									<input type="hidden" name="imagename" id="imagename" value="pagewarehouse2">
									<input type="checkbox" name="hasslide" id="hasslideWarehouse2" checked class="hide" />
									<label for="title">Title</label>
									<input type="text" class="form-control" name="content" id="pagecontent" placeholder="Title" value="${pagewarehouse2.content}">
								</div>
								<div class="col-lg-6 col-sm-6 col-md-12">
									<label for="title">Main Image</label> <input type="file"
										class="dropify form-control" name="fileImageLayer3"
										id="fileImageLayer3"
										data-default-file="${pageContext.request.contextPath}/media/images/${pagewarehouse2.pageName}.png">
								</div>
								<div class="col-lg-6 col-sm-6 col-md-12">
									<div class="col-12">
										<label for="brochurefile">Brochure</label> <input
											type="file" name="brochurefile" id="brochurefile"
											data-default-file="${pageContext.request.contextPath}/documents/brochures/${pagewarehouse2.brochure}" class="dropify form-control"
											accept="">
									</div>
								</div>
								<!-- 								<div class="col-lg-6 col-sm-6 col-md-12"> -->
								<!-- 									<div class="col-12"> -->
								<!-- 										<label for="backgroundvideo">Background video</label> <input -->
								<!-- 											type="file" name="backgroundvideo" id="backgroundvideo" -->
								<%-- 											data-default-file="${pageContext.request.contextPath}/media/video/${pagewarehouse2.pageName}" class="dropify form-control" --%>
								<!-- 											accept="video/mp4"> -->
								<!-- 									</div> -->
								<!-- 								</div> -->
							</div>
							
							<div class="row grid-margin">
								<div class="col-12">
									<label for="tinyMceExample">Key Features</label>
									<textarea class="form-control" type="text" name="detailcontent"
										id="tinyMceExample" rows="3">${pagewarehouse2.detailContent}</textarea>
								</div>
							</div>
						</div>
						<p class="text-center">
							<button class="btn btn-primary" type="submit"
								id="saveformImageContentWarehouse2">Save</button>
						</p>
					</form>
					<c:if test="${not empty pagewarehouse2.listSlide}">
						<div id="hasSlideGroupWarehouse2" class="slideImage4 col-12">
							<c:forEach items="${pagewarehouse2.listSlide}" var="slide">
								<form id="formslidewarehouse2" name="formslidewarehouse2"
									action="/admin/updateslide" enctype="multipart/form-data"
									method="POST" class="col-12">
									<input type="hidden" name="id" id="id" value="${slide.id}">
									<input type="hidden" name="formname" id="formname"
										value="${slide.slidepage}">
									<div class="row grid-margin">
										<div class="col-12">
											<div class="row grid-margin">
												<div class="col-lg-6 col-sm-6 col-md-12">
													<label for="Slide">Slide name</label>
													<input type="text" class="form-control" name="slidename" id="slidename" placeholder="Slide Name" value="${slide.slidename}" >
												</div>
												<div class="col-lg-6 col-sm-6 col-md-12">
													<label for="image">Slide image</label>
													<input type="file" id="slideimage" class="dropify form-control" data-default-file="${pageContext.request.contextPath}/media/images/${slide.slideimg}" name="slideimage">
												</div>
											</div>
											<p class="text-center mb-4 mt-4">
												<button class="btn btn-primary" type="submit"
													id="saveSlide" name="update">Update</button>
												<button class="btn btn-danger" type="submit"
													id="deleteSlide" name="delete">Delete</button>
											</p>
										</div>
									</div>
								</form>
							</c:forEach>
						</div>
					</c:if>
					<div class="add-slide col-12" id="add-slide-warehouse2">
						<form id="formslide" name="formslide" action="/admin/saveslide"
							enctype="multipart/form-data" method="post" class="col-12">
							<input type="hidden" name="formname" id="formname"
								value="pagewarehouse2">
							<div class="row grid-margin">
								<div class="col-12">
									<div class="row grid-margin">
										<div class="col-lg-6 col-sm-6 col-md-12">
											<label for="slide">Slide name</label>
											<input type="text" class="form-control" name="slidename" id="slidename" placeholder="Slide Name" value="" required>
										</div>
										<div class="col-lg-6 col-sm-6 col-md-12">
											<label for="image">Slide image</label>
											<input type="file" id="slideimage" class="dropify form-control" data-default-file="" name="slideimage">
										</div>
									</div>
									<p class="text-center">
										<button class="btn btn-primary" type="submit" id="saveSlide">Save</button>
									</p>
								</div>
							</div>
						</form>
					</div>
					<div class="col-12 mb-4">
						<button class="btn btn-sm btn-success btn-add-slide" title = "Add new slide" title = "Add new slide" type="button"
							id="add-slide-warehouse2">
							<i class="mdi mdi-plus"></i>
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row grid-margin">
		<div class="col-12">
			<div class="card">
				<h2 class="mb-0 card-header set-accordion">
					<c:if test="${not empty pagewarehouse3.content}">
						<a class="c" data-parent="#accordion" aria-expanded="false"
							href="#collapsepagewarehouse3" data-toggle="collapse">${pagewarehouse3.content}</a>
					</c:if>
					<c:if test="${empty pagewarehouse3.content}">
						<a class="c" data-parent="#accordion" aria-expanded="false"
							href="#collapsepagewarehouse3" data-toggle="collapse">Page
							Warehouse3</a>
					</c:if>
				</h2>
				<div id="collapsepagewarehouse3" class="collapse">
					<form id="formImageContentWarehouse3"
						name="formImageContentWarehouse3" action="/admin/saveimagecontent"
						enctype="multipart/form-data" method="post">
						<div class="card-body">
							<div class="row">
								<div class="col-lg-6 col-sm-6 col-md-12">
									<input type="hidden" name="imagename" id="imagename" value="pagewarehouse3">
									<input type="checkbox" name="hasslide" id="hasslideWarehouse3" checked class="hide">
									<label for="title">Title</label>
									<input type="text" class="form-control" name="content" id="pagecontent" placeholder="Title" value="${pagewarehouse3.content}">
								</div>
								<div class="col-lg-6 col-sm-6 col-md-12">
									<label for="title">Main Image</label> <input type="file"
										class="dropify form-control" name="fileImageLayer3"
										id="fileImageLayer3"
										data-default-file="${pageContext.request.contextPath}/media/images/${pagewarehouse3.pageName}.png">
								</div>
								<div class="col-lg-6 col-sm-6 col-md-12">
									<div class="col-12">
										<label for="brochurefile">Brochure</label> <input
											type="file" name="brochurefile" id="brochurefile"
											data-default-file="${pageContext.request.contextPath}/documents/brochures/${pagewarehouse3.brochure}" class="dropify form-control"
											accept="">
									</div>
								</div>
								<!-- 								<div class="col-lg-6 col-sm-6 col-md-12"> -->
								<!-- 									<div class="col-12"> -->
								<!-- 										<label for="backgroundvideo">Background video</label> <input -->
								<!-- 											type="file" name="backgroundvideo" id="backgroundvideo" -->
								<%-- 											data-default-file="${pageContext.request.contextPath}/media/video/${pagewarehouse3.pageName}" class="dropify form-control" --%>
								<!-- 											accept="video/mp4"> -->
								<!-- 									</div> -->
								<!-- 								</div> -->
							</div>
							
							<div class="row grid-margin">
								<div class="col-12">
									<label for="tinyMceExample">Key Features</label>
									<textarea class="form-control" type="text" name="detailcontent"
										id="tinyMceExample" rows="3">${pagewarehouse3.detailContent}</textarea>
								</div>
							</div>
						</div>
						<p class="text-center">
							<button class="btn btn-primary" type="submit"
								id="saveformImageContentWarehouse3">Save</button>
						</p>
					</form>
					<c:if test="${not empty pagewarehouse3.listSlide}">
						<div id="hasSlideGroupWarehouse3" class="slideImage4 col-12">
							<c:forEach items="${pagewarehouse3.listSlide}" var="slide">
								<form id="formslidewarehouse3" name="formslidewarehouse3"
									action="/admin/updateslide" enctype="multipart/form-data"
									method="POST" class="col-12">
									<input type="hidden" name="id" id="id" value="${slide.id}">
									<input type="hidden" name="formname" id="formname"
										value="${slide.slidepage}">
									<div class="row grid-margin">
										<div class="col-12">
											<div class="row grid-margin">
												<div class="col-lg-6 col-sm-6 col-md-12">
													<label for="Slide">Slide name</label>
													<input type="text" class="form-control" name="slidename" id="slidename" placeholder="Slide Name" value="${slide.slidename}" >
												</div>
												<div class="col-lg-6 col-sm-6 col-md-12">
													<label for="image">Slide image</label>
													<input type="file" id="slideimage" class="dropify form-control" data-default-file="${pageContext.request.contextPath}/media/images/${slide.slideimg}" name="slideimage">
												</div>
											</div>
											<p class="text-center mb-4 mt-4">
												<button class="btn btn-primary" type="submit"
													id="saveSlide" name="update">Update</button>
												<button class="btn btn-danger" type="submit"
													id="deleteSlide" name="delete">Delete</button>
											</p>
										</div>
									</div>
								</form>
							</c:forEach>
						</div>
					</c:if>
					<div class="add-slide col-12" id="add-slide-warehouse3">
						<form id="formslide" name="formslide" action="/admin/saveslide"
							enctype="multipart/form-data" method="post" class="col-12">
							<input type="hidden" name="formname" id="formname"
								value="pagewarehouse3">
							<div class="row grid-margin">
								<div class="col-12">
									<div class="row grid-margin">
										<div class="col-lg-6 col-sm-6 col-md-12">
											<label for="slide">Slide name</label>
											<input type="text" class="form-control" name="slidename" id="slidename" placeholder="Slide Name" value="" required>
										</div>
										<div class="col-lg-6 col-sm-6 col-md-12">
											<label for="image">Slide image</label>
											<input type="file" id="slideimage"
																class="dropify form-control" data-default-file=""
																name="slideimage">
										</div>
									</div>
									<p class="text-center">
										<button class="btn btn-primary" type="submit" id="saveSlide">Save</button>
									</p>
								</div>
							</div>
						</form>
					</div>
					<div class="col-12 mb-4">
						<button class="btn btn-sm btn-success btn-add-slide" title = "Add new slide" title = "Add new slide" type="button"
							id="add-slide-warehouse3">
							<i class="mdi mdi-plus"></i>
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row grid-margin">
		<div class="col-12">
			<div class="card">
				<h2 class="mb-0 card-header set-accordion">
					<c:if test="${not empty pagewarehouse4.content}">
						<a class="c" data-parent="#accordion" aria-expanded="false"
							href="#collapsepagewarehouse4" data-toggle="collapse">${pagewarehouse4.content}</a>
					</c:if>
					<c:if test="${empty pagewarehouse4.content}">
						<a class="c" data-parent="#accordion" aria-expanded="false"
							href="#collapsepagewarehouse4" data-toggle="collapse">Page
							Warehouse4</a>
					</c:if>
				</h2>
				<div id="collapsepagewarehouse4" class="collapse">
					<form id="formImageContentWarehouse4"
						name="formImageContentWarehouse4" action="/admin/saveimagecontent"
						enctype="multipart/form-data" method="post">
						<div class="card-body">
							<div class="row">
								<div class="col-lg-6 col-sm-6 col-md-12">
									<input type="hidden" name="imagename" id="imagename"
													placeholder="Page Name" value="pagewarehouse4">

									<input type="checkbox" name="hasslide"
										id="hasslideWarehouse4" checked  class="hide">
									<label for="title">Title</label>
									<input type="text" class="form-control" name="content"
												id="pagecontent" placeholder="Title"
												value="${pagewarehouse4.content}">
								</div>
								<div class="col-lg-6 col-sm-6 col-md-12">
									<label for="title">Main Image</label>
									<input type="file"
										class="dropify form-control" name="fileImageLayer3"
										id="fileImageLayer3"
										data-default-file="${pageContext.request.contextPath}/media/images/${pagewarehouse4.pageName}.png">
								</div>
								<div class="col-lg-6 col-sm-6 col-md-12">
									<div class="col-12">
										<label for="brochurefile">Brochure</label> <input
											type="file" name="brochurefile" id="brochurefile"
											data-default-file="${pageContext.request.contextPath}/documents/brochures/${pagewarehouse4.brochure}" class="dropify form-control"
											accept="">
									</div>
								</div>
								<!-- 								<div class="col-lg-6 col-sm-6 col-md-12"> -->
								<!-- 									<div class="col-12"> -->
								<!-- 										<label for="backgroundvideo">Background video</label> <input -->
								<!-- 											type="file" name="backgroundvideo" id="backgroundvideo" -->
								<%-- 											data-default-file="${pageContext.request.contextPath}/media/video/${pagewarehouse4.pageName}" class="dropify form-control" --%>
								<!-- 											accept="video/mp4"> -->
								<!-- 									</div> -->
								<!-- 								</div> -->
							</div>
							
							<div class="row grid-margin">
								<div class="col-12">
									<label for="tinyMceExample">Key Features</label>
									<textarea class="form-control" type="text" name="detailcontent"
										id="tinyMceExample" rows="3">${pagewarehouse4.detailContent}</textarea>
								</div>
							</div>
						</div>
						<p class="text-center">
							<button class="btn btn-primary" type="submit"
								id="saveformImageContentWarehouse4">Save</button>
						</p>
					</form>
					<c:if test="${not empty pagewarehouse4.listSlide}">
						<div id="hasSlideGroupWarehouse4" class="slideImage4 col-12">
							<c:forEach items="${pagewarehouse4.listSlide}" var="slide">
								<form id="formslidewarehouse4" name="formslidewarehouse4"
									action="/admin/updateslide" enctype="multipart/form-data"
									method="POST" class="col-12">
									<input type="hidden" name="id" id="id" value="${slide.id}">
									<input type="hidden" name="formname" id="formname"
										value="${slide.slidepage}">
									<div class="row grid-margin">
										<div class="col-12">
											<div class="row grid-margin">
												<div class="col-lg-6 col-sm-6 col-md-12">
													<label for="Slide">Slide name</label>
													<input type="text" class="form-control"
																	name="slidename" id="slidename"
																	placeholder="Slide Name" value="${slide.slidename}"
																	>
												</div>
												<div class="col-lg-6 col-sm-6 col-md-12">
													<label for="image">Slide image</label>
													<input type="file" id="slideimage"
																		class="dropify form-control"
																		data-default-file="${pageContext.request.contextPath}/media/images/${slide.slideimg}"
																		name="slideimage">
												</div>
											</div>
											<p class="text-center mt-4 mb-4">
												<button class="btn btn-primary" type="submit"
													id="saveSlide" name="update">Update</button>
												<button class="btn btn-danger" type="submit"
													id="deleteSlide" name="delete">Delete</button>
											</p>
										</div>
									</div>
								</form>
							</c:forEach>
						</div>
					</c:if>
					<div class="add-slide col-12" id="add-slide-warehouse4">
						<form id="formslide" name="formslide" action="/admin/saveslide"
							enctype="multipart/form-data" method="post" class="col-12">
							<input type="hidden" name="formname" id="formname"
								value="pagewarehouse4">
							<div class="row grid-margin">
								<div class="col-12">
									<div class="row grid-margin">
										<div class="col-lg-6 col-sm-6 col-md-12">
											<label for="slide">Slide name</label>
											<input type="text" class="form-control" name="slidename"
																id="slidename" placeholder="Slide Name" value=""
																>
										</div>
										<div class="col-lg-6 col-sm-6 col-md-12">
											<label for="image">Slide image</label>
											<input type="file" id="slideimage"
																class="dropify form-control" data-default-file=""
																name="slideimage">
										</div>
									</div>
									<p class="text-center">
										<button class="btn btn-primary" type="submit" id="saveSlide">Save</button>
									</p>
								</div>
							</div>
						</form>
					</div>
					<div class="col-12 mb-4">
						<button class="btn btn-sm btn-success btn-add-slide" title = "Add new slide" title = "Add new slide" type="button"
							id="add-slide-warehouse4">
							<i class="mdi mdi-plus"></i>
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row grid-margin">
		<div class="col-12">
			<div class="card">
				<h2 class="mb-0 card-header set-accordion">
					<c:if test="${not empty pagewarehouse5.content}">
						<a class="c" data-parent="#accordion" aria-expanded="false"
							href="#collapsepagewarehouse5" data-toggle="collapse">${pagewarehouse5.content}</a>
					</c:if>
					<c:if test="${empty pagewarehouse5.content}">
						<a class="c" data-parent="#accordion" aria-expanded="false"
							href="#collapsepagewarehouse5" data-toggle="collapse">Page
							Warehouse5</a>
					</c:if>
				</h2>
				<div id="collapsepagewarehouse5" class="collapse">
					<form id="formImageContentWarehouse5"
						name="formImageContentWarehouse5" action="/admin/saveimagecontent"
						enctype="multipart/form-data" method="post">
						<div class="card-body">
							<div class="row">
								<div class="col-lg-6 col-sm-6 col-md-12">
									<input type="hidden" name="imagename" id="imagename"
													placeholder="Page Name" value="pagewarehouse5">
									<input type="checkbox" name="hasslide"
												id="hasslideWarehouse5" checked  class="hide">
									<label for="title">Title</label>
									<input type="text" class="form-control" name="content"
												id="pagecontent" placeholder="Title"
												value="${pagewarehouse5.content}">
								</div>
								<div class="col-lg-6 col-sm-6 col-md-12">
									<label for="title">Main Image</label>
									<input type="file"
										class="dropify form-control" name="fileImageLayer3"
										id="fileImageLayer3"
										data-default-file="${pageContext.request.contextPath}/media/images/${pagewarehouse5.pageName}.png">
								</div>
								<div class="col-lg-6 col-sm-6 col-md-12">
									<div class="col-12">
										<label for="brochurefile">Brochure</label> <input
											type="file" name="brochurefile" id="brochurefile"
											data-default-file="${pageContext.request.contextPath}/documents/brochures/${pagewarehouse5.brochure}" class="dropify form-control"
											accept="">
									</div>
								</div>
								<!-- 								<div class="col-lg-6 col-sm-6 col-md-12"> -->
								<!-- 									<div class="col-12"> -->
								<!-- 										<label for="backgroundvideo">Background video</label> <input -->
								<!-- 											type="file" name="backgroundvideo" id="backgroundvideo" -->
								<%-- 											data-default-file="${pageContext.request.contextPath}/media/video/${pagewarehouse5.pageName}" class="dropify form-control" --%>
								<!-- 											accept="video/mp4"> -->
								<!-- 									</div> -->
								<!-- 								</div> -->
							</div>
							
							<div class="row grid-margin">
								<div class="col-12">
									<label for="tinyMceExample">Key Features</label>
									<textarea class="form-control" type="text" name="detailcontent"
										id="tinyMceExample" rows="3">${pagewarehouse5.detailContent}</textarea>
								</div>
							</div>
						</div>
						<p class="text-center">
							<button class="btn btn-primary" type="submit"
								id="saveformImageContentWarehouse5">Save</button>
						</p>
					</form>
					<c:if test="${not empty pagewarehouse5.listSlide}">
						<div id="hasSlideGroupWarehouse5" class="slideImage4 col-12">
							<c:forEach items="${pagewarehouse5.listSlide}" var="slide">
								<form id="formslidewarehouse5" name="formslidewarehouse5"
									action="/admin/updateslide" enctype="multipart/form-data"
									method="POST" class="col-12">
									<input type="hidden" name="id" id="id" value="${slide.id}">
									<input type="hidden" name="formname" id="formname"
										value="${slide.slidepage}">
									<div class="row grid-margin">
										<div class="col-12">
											<div class="row grid-margin">
												<div class="col-lg-6 col-sm-6 col-md-12">
													<label for="Slide">Slide name</label>
													<input type="text" class="form-control"
																		name="slidename" id="slidename"
																		placeholder="Slide Name" value="${slide.slidename}"
																		>
												</div>
												<div class="col-lg-6 col-sm-6 col-md-12">
													<label for="image">Slide image</label>
													<input type="file" id="slideimage"
																		class="dropify form-control"
																		data-default-file="${pageContext.request.contextPath}/media/images/${slide.slideimg}"
																		name="slideimage">
												</div>
											</div>
											<p class="text-center mt-4 mb-4">
												<button class="btn btn-primary" type="submit"
													id="saveSlide" name="update">Update</button>
												<button class="btn btn-danger" type="submit"
													id="deleteSlide" name="delete">Delete</button>
											</p>
										</div>
									</div>
								</form>
							</c:forEach>
						</div>
					</c:if>
					<div class="add-slide col-12" id="add-slide-warehouse5">
						<form id="formslide" name="formslide" action="/admin/saveslide"
							enctype="multipart/form-data" method="post" class="col-12">
							<input type="hidden" name="formname" id="formname"
								value="pagewarehouse5">
							<div class="row grid-margin">
								<div class="col-12">
									<div class="row grid-margin">
										<div class="col-lg-6 col-sm-6 col-md-12">
											<label for="slide">Slide name</label>
											<input type="text" class="form-control" name="slidename"
																id="slidename" placeholder="Slide Name" value=""
																>
										</div>
										<div class="col-lg-6 col-sm-6 col-md-12">
											<label for="image">Slide image</label>
											<input type="file" id="slideimage"
																class="dropify form-control" data-default-file=""
																name="slideimage">
										</div>
									</div>
									<p class="text-center">
										<button class="btn btn-primary" type="submit" id="saveSlide">Save</button>
									</p>
								</div>
							</div>
						</form>
					</div>
					<div class="col-12 mb-4">
						<button class="btn btn-sm btn-success btn-add-slide" title = "Add new slide" title = "Add new slide" type="button"
							id="add-slide-warehouse5">
							<i class="mdi mdi-plus"></i>
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>


	<div class="row grid-margin">
		<div class="col-12">
			<div class="card">
				<h2 class="mb-0 card-header set-accordion">
					<c:if test="${not empty pagewarehouse6.content}">
						<a class="c" data-parent="#accordion" aria-expanded="false"
							href="#collapsepagewarehouse6" data-toggle="collapse">${pagewarehouse6.content}</a>
					</c:if>
					<c:if test="${empty pagewarehouse6.content}">
						<a class="c" data-parent="#accordion" aria-expanded="false"
							href="#collapsepagewarehouse6" data-toggle="collapse">Page
							Warehouse6</a>
					</c:if>
				</h2>
				<div id="collapsepagewarehouse6" class="collapse">
					<form id="formImageContentWarehouse6"
						name="formImageContentWarehouse6" action="/admin/saveimagecontent"
						enctype="multipart/form-data" method="post">
						<div class="card-body">
							<div class="row">
								<div class="col-lg-6 col-sm-6 col-md-12">
									<input type="hidden" name="imagename" id="imagename" value="pagewarehouse6">
									<input type="checkbox" name="hasslide"
												id="hasslideWarehouse6" checked class="hide">
									<label for="title">Title</label>
									<input type="text" class="form-control" name="content"
												id="pagecontent" placeholder="Title"
												value="${pagewarehouse6.content}">
								</div>
								<div class="col-lg-6 col-sm-6 col-md-12">
									<label for="title">Main Image</label>
									<input type="file"
										class="dropify form-control" name="fileImageLayer3"
										id="fileImageLayer3"
										data-default-file="${pageContext.request.contextPath}/media/images/${pagewarehouse6.pageName}.png">
								</div>
								<div class="col-lg-6 col-sm-6 col-md-12">
									<div class="col-12">
										<label for="brochurefile">Brochure</label> <input
											type="file" name="brochurefile" id="brochurefile"
											data-default-file="${pageContext.request.contextPath}/documents/brochures/${pagewarehouse6.brochure}" class="dropify form-control"
											accept="">
									</div>
								</div>
								<!-- 								<div class="col-lg-6 col-sm-6 col-md-12"> -->
								<!-- 									<div class="col-12"> -->
								<!-- 										<label for="backgroundvideo">Background video</label> <input -->
								<!-- 											type="file" name="backgroundvideo" id="backgroundvideo" -->
								<%-- 											data-default-file="${pageContext.request.contextPath}/media/video/${pagewarehouse6.pageName}" class="dropify form-control" --%>
								<!-- 											accept="video/mp4"> -->
								<!-- 									</div> -->
								<!-- 								</div> -->
							</div>
							
							<div class="row grid-margin">
								<div class="col-12">
									<label for="tinyMceExample">Key Features</label>
									<textarea class="form-control" type="text" name="detailcontent"
										id="tinyMceExample" rows="3">${pagewarehouse6.detailContent}</textarea>
								</div>
							</div>
						</div>
						<p class="text-center">
							<button class="btn btn-primary" type="submit"
								id="saveformImageContentWarehouse6">Save</button>
						</p>
					</form>
					<c:if test="${not empty pagewarehouse6.listSlide}">
						<div id="hasSlideGroupWarehouse6" class="slideImage4 col-12">
							<c:forEach items="${pagewarehouse6.listSlide}" var="slide">
								<form id="formslidewarehouse6" name="formslidewarehouse6"
									action="/admin/updateslide" enctype="multipart/form-data"
									method="POST" class="col-12">
									<input type="hidden" name="id" id="id" value="${slide.id}">
									<input type="hidden" name="formname" id="formname"
										value="${slide.slidepage}">
									<div class="row grid-margin">
										<div class="col-12">
											<div class="row grid-margin">
												<div class="col-lg-6 col-sm-6 col-md-12">
													<label for="Slide">Slide name</label>
													<input type="text" class="form-control"
																		name="slidename" id="slidename"
																		placeholder="Slide Name" value="${slide.slidename}"
																		>
												</div>
												<div class="col-lg-6 col-sm-6 col-md-12">
													<label for="image">Slide image</label>
													<input type="file" id="slideimage"
																		class="dropify form-control"
																		data-default-file="${pageContext.request.contextPath}/media/images/${slide.slideimg}"
																		name="slideimage">
												</div>
											</div>
											<p class="text-center mb-4 mt-4">
												<button class="btn btn-primary" type="submit"
													id="saveSlide" name="update">Update</button>
												<button class="btn btn-danger" type="submit"
													id="deleteSlide" name="delete">Delete</button>
											</p>
										</div>
									</div>
								</form>
							</c:forEach>
						</div>
					</c:if>
					<div class="add-slide col-12" id="add-slide-warehouse6">
						<form id="formslide" name="formslide" action="/admin/saveslide"
							enctype="multipart/form-data" method="post" class="col-12">
							<input type="hidden" name="formname" id="formname"
								value="pagewarehouse6">
							<div class="row grid-margin">
								<div class="col-12">
									<div class="row grid-margin">
										<div class="col-lg-6 col-sm-6 col-md-12">
											<label for="slide">Slide name</label>
											<input type="text" class="form-control" name="slidename"
																id="slidename" placeholder="Slide Name" value=""
																>
										</div>
										<div class="col-lg-6 col-sm-6 col-md-12">
											<label for="image">Slide image</label>
											<input type="file" id="slideimage"
																class="dropify form-control" data-default-file=""
																name="slideimage">
										</div>
									</div>
								</div>
								<p class="text-center">
									<button class="btn btn-primary" type="submit" id="saveSlide">Save</button>
								</p>
							</div>
						</form>
					</div>
					<div class="col-12 mb-4">
						<button class="btn btn-sm btn-success btn-add-slide" title = "Add new slide" title = "Add new slide" type="button"
							id="add-slide-warehouse6">
							<i class="mdi mdi-plus"></i>
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		var hasSlideGroupWarehouse1 = document
				.getElementById("hasSlideGroupWarehouse1");
		var hasSlideGroupWarehouse2 = document
				.getElementById("hasSlideGroupWarehouse2");
		var hasSlideGroupWarehouse3 = document
				.getElementById("hasSlideGroupWarehouse3");
		var hasSlideGroupWarehouse4 = document
				.getElementById("hasSlideGroupWarehouse4");
		var hasSlideGroupWarehouse5 = document
				.getElementById("hasSlideGroupWarehouse5");
		var hasSlideGroupWarehouse6 = document
				.getElementById("hasSlideGroupWarehouse6");
		
		$(document).ready(function() {
			$('#add-slide-warehouse1').css('display', 'none');
			$('#add-slide-warehouse2').css('display', 'none');
			$('#add-slide-warehouse3').css('display', 'none');
			$('#add-slide-warehouse4').css('display', 'none');
			$('#add-slide-warehouse5').css('display', 'none');
			$('#add-slide-warehouse6').css('display', 'none');
		});
		function hasSlideWarehouse() {
			if (document.getElementById("hasslideWarehouse1").checked) {
				hasSlideGroupWarehouse1.style.display = "block";
			} else {
				hasSlideGroupWarehouse1.style.display = "none";
			}

			if (document.getElementById("hasslideWarehouse2").checked) {
				hasSlideGroupWarehouse2.style.display = "block";
			} else {
				hasSlideGroupWarehouse2.style.display = "none";
			}
			if (document.getElementById("hasslideWarehouse3").checked) {
				hasSlideGroupWarehouse3.style.display = "block";
			} else {
				hasSlideGroupWarehouse3.style.display = "none";
			}
			if (document.getElementById("hasslideWarehouse4").checked) {
				hasSlideGroupWarehouse4.style.display = "block";
			} else {
				hasSlideGroupWarehouse4.style.display = "none";
			}
			if (document.getElementById("hasslideWarehouse5").checked) {
				hasSlideGroupWarehouse5.style.display = "block";
			} else {
				hasSlideGroupWarehouse5.style.display = "none";
			}
			if (document.getElementById("hasslideWarehouse6").checked) {
				hasSlideGroupWarehouse6.style.display = "block";
			} else {
				hasSlideGroupWarehouse6.style.display = "none";
			}
		}
	</script>
</body>
</html>