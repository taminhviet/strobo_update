
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:if test="${success == true}" > <div class="alert alert-success"><strong>Welldone!</strong> Update successfully!</div> </c:if>
<h1 class="page-title">Contact Page</h1>
<form action="/admin/contactus"  method="post">
    <div class="row grid-margin">
        <div class="col-12">
            <div class="card">
                <h2 class="card-header">General Information</h2>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-6 col-sm-6 col-md-12">
                            <input type="hidden" name="status" value="true">
                            <label for="title">Title</label>
                            <input type="text" class="form-control" name="title" id="title" placeholder="Title" value="${contact.title}" required>
                            <label for="pageName">Page Name</label>
                            <input type="text" class="form-control" name="pageName" id="pageName" placeholder="Page Name" value="${contact.pageName}">
                            <label for="meta">Meta</label>
                            <textarea type="text" class="form-control" name="meta" rows="6" id="meta" placeholder="Meta" >${contact.meta}</textarea>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-md-12">
                            <label for="createdDate">Date Create</label>
                            <input type="text" class="form-control" id="createdDate" placeholder="Date Create" disabled value="${contact.createdDate}">
                            <label for="keyword">KeyWord</label>
                            <textarea type="text" class="form-control" name="keyword" rows="6" id="keyword" placeholder="Keyword" >${contact.keyword}</textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row grid-margin admin_contact">
        <div class="col-12">
            <div class="card">
                <h2 class="card-header">Contact Information</h2>
               <div class="card-body">
                    <div class="row grid-margin">
                        <div class="col-lg-6 col-sm-6 col-md-12">
                            <label for="email">Email</label>
                            <input type="text" class="form-control" name="email" id="email" placeholder="Email" value="${contact.email}">
                        </div>
                        <div class="col-lg-6 col-sm-6 col-md-12">
                            <label for="phone">Phone</label>
                            <input type="text" class="form-control" name="phone" id="phone" placeholder="Phone" value="${contact.phone}" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-sm-6 col-md-12">
                            <label for="address">Address Display</label>
                            <textarea class="form-control" name="address" id="address" placeholder="Address" rows="3">${contact.address}</textarea>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-md-12">
                            <label for="googleApi">Google Maps</label>
                            <textarea class="form-control" name="googleApi" id="googleApi" placeholder="Address" rows="3">${contact.googleApi}</textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <p class="text-center"><button class="btn btn-primary" type="submit">Save</button></p>
</form>
