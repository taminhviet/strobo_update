package com.aht.tb.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aht.tb.entity.HomePage;
import com.aht.tb.repository.HomePageRepository;

@Service
public class HomePageService {
	@Autowired
	private HomePageRepository homePageRepository;
	//Get Home Page;
	public List<HomePage> findHomePage(){
		List<HomePage> listHomePage = homePageRepository.findAll();
		return listHomePage;
	}
	//Save Home Page
	public boolean saveHomePage(HomePage homePage) {
		boolean success =false;
		try {
			homePageRepository.save(homePage);
			success=true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return success;
	}
}
