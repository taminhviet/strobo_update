package com.aht.tb.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aht.tb.entity.Technology;
import com.aht.tb.repository.TechnologyRepository;

@Service
public class TechnologyService {

    @Autowired
    TechnologyRepository technologyRepository;

    // get technology by name
    public Technology getTechnologyByName(String technologyName) {
        Technology technologySearch = technologyRepository
                .findBytechnologyName(technologyName);
        return technologySearch;
    }

    // save technology
    public void saveTechnology(Technology technology) {
        technologyRepository.saveAndFlush(technology);
    }

    // upadte technology
    public void updateTechnology(Technology technology) {
        technologyRepository.updateTechnology(technology.getTechnologyName(),
                technology.getTitle(), technology.getHeading1(),
                technology.getDescription1(), technology.getHeading2(),
                technology.getDescription2());
    }
}
