package com.aht.tb.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aht.tb.entity.Robotic;
import com.aht.tb.repository.RoboticRepository;

@Service
public class RoboticService {

    @Autowired
    RoboticRepository roboticRepository;
    
    //find by roboticname
    public Robotic findByroboticname(String roboticname){
        Robotic robotic = roboticRepository.findByroboticname(roboticname);
        return robotic;
    }
    
    //save data
    public void saveRobotic(Robotic robotic){
        roboticRepository.save(robotic);
    }
    
    //update data
    public void updateRobotic(Robotic robotic){
        roboticRepository.updateTechnology(robotic.getRoboticname(), robotic.getName(), robotic.getDescription(), robotic.getKeyfeatures());
    }
}
