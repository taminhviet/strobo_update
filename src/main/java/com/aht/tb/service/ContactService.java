package com.aht.tb.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aht.tb.entity.Contact;
import com.aht.tb.repository.ContactRepository;

@Service
public class ContactService {
	@Autowired
	private ContactRepository contactRepository;
	//GetContact
	public List<Contact> findContact(){
		List<Contact> listContact = contactRepository.findAll();
		return listContact;
	}
	//SaveContact 
	public boolean saveContact(Contact contact) {
		boolean success = false;
		try {
			contactRepository.save(contact);
			success=true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return success;
	}
}
