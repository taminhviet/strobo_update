package com.aht.tb.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aht.tb.entity.GGCaptcha;
import com.aht.tb.repository.GGCaptchaRepository;

@Service
public class GGCaptchaService {
	@Autowired
	private GGCaptchaRepository ggCaptchaRepository;

	// Get GGCaptcha
	public GGCaptcha getCaptcha() {
		if (!ggCaptchaRepository.findAll().isEmpty()) {
			return ggCaptchaRepository.findAll().get(0);
		} else {
			return null;
		}
	}

	// Save GGCaptcha
	public boolean saveCaptcha(GGCaptcha captcha) {
		try {
			captcha.setId(ggCaptchaRepository.findAll().get(0).getId());
			ggCaptchaRepository.save(captcha);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	//get all
	public List<GGCaptcha> getAll() {
		return ggCaptchaRepository.findAll();
	}
}
