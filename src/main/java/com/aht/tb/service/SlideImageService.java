package com.aht.tb.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aht.tb.entity.SlideImage;
import com.aht.tb.repository.SlideImageRepository;

@Service
public class SlideImageService {

    @Autowired
    SlideImageRepository slideImageRepository;

    // get by name
    public SlideImage getSlideImagebyName(String slideName) {
        SlideImage slideImage = slideImageRepository.findByslidename(slideName);
        return slideImage;
    }
    
    //find by ID
    public SlideImage getSlideImageById(int id){
        Optional<SlideImage> slideImage = slideImageRepository.findById(id);
        return slideImage.get();
    }
    
    //find by Slide page
    public List<SlideImage> findBySlidePage(String slidepage){
        return slideImageRepository.findByslidepage(slidepage);
    }

    // save data
    public void saveSlideImage(SlideImage slideImage) {
        slideImageRepository.saveAndFlush(slideImage);
    }
    
    //delete data
    public void deleteSlideImage(SlideImage slideimage){
        slideImageRepository.delete(slideimage);
    }

    // update data
    public void updateSlideImage(SlideImage slideImage) {
        slideImageRepository.updateImageContent(slideImage.getSlidename(), slideImage.getSlideimg(), slideImage.getSlidepage());
    }
}
