package com.aht.tb.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aht.tb.entity.ImageContent;
import com.aht.tb.repository.ImageContentRepository;

@Service
public class ImageContentService {

    @Autowired
    private ImageContentRepository imageContentRepository;

    // get image content by image name
    public ImageContent getImageContentbyName(String imageName) {
        ImageContent imageContent = imageContentRepository
                .findByimagename(imageName);
        return imageContent;
    }

    // save imagecontent
    public void saveImageContent(ImageContent imageContent) {
        imageContentRepository.saveAndFlush(imageContent);
    }

    // load all data
    public List<ImageContent> loadAllData() {
        return imageContentRepository.findAll();
    }

    // update data
    public void updateImageContent(ImageContent imageContent) {
        imageContentRepository.updateImageContent(imageContent.getImagename(),
                imageContent.getContent(),
                imageContent.isHasslide(), imageContent.getDetailcontent(), imageContent.getBrochure());
    }
}
