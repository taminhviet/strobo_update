package com.aht.tb.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.aht.tb.entity.NewsEvent;
import com.aht.tb.repository.EventRepository;

@Service
public class EventService {

	@Autowired
	EventRepository eventRepository;
	
	public void saveEvent(NewsEvent event){
		eventRepository.save(event);
	}
	
	public List<NewsEvent> getAllData(){
		return eventRepository.findAll();
	}
	
	public NewsEvent findbyId(int id){
		return eventRepository.findById(id).get();
	}
	
	public void deleteNews(NewsEvent newsEvent){
		eventRepository.delete(newsEvent);
	}
	
	public List<NewsEvent> getDataPaging(int pageNumber, int pageSize){
		Sort sortable = Sort.by("id").descending();
		Pageable pageable = PageRequest.of(pageNumber, pageSize, sortable);
		Page<NewsEvent> events = eventRepository.findAll(pageable);
		return events.getContent();
	}
	public long getRowCount(){
		return eventRepository.count();
	}
}
