package com.aht.tb.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aht.tb.entity.About;
import com.aht.tb.repository.AboutRepository;

@Service
public class AboutService {

    @Autowired
    AboutRepository aboutRepository;

    // find by name
    public About findByAboutName(String aboutname) {
        About about = aboutRepository.findByaboutname(aboutname);
        return about;
    }

    // save about
    public void saveAbout(About about) {
        aboutRepository.save(about);
    }

    // update about
    public void updateAbout(About about) {
        aboutRepository.updateAbout(about.getAboutname(), about.getTitle1(),
                about.getTitle2(), about.getHeading1(), about.getHeading2(),
                about.getVisit(), about.getDescription1(),
                about.getDescription2(), about.getDescription3(),
                about.getDescription4());
    }
}
