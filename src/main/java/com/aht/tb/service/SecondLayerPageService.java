package com.aht.tb.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aht.tb.entity.SecondLayerPage;
import com.aht.tb.repository.SecondLayerPageRepository;

@Service
public class SecondLayerPageService {
    @Autowired
    private SecondLayerPageRepository secondLayerPageRepository;

    // get second layer page by pageName
    public SecondLayerPage getSecondPagebyName(String pageName) {
        SecondLayerPage secondLayerPage = secondLayerPageRepository
                .findBypagename(pageName);
        return secondLayerPage;
    }

    // save data
    public void saveSecondLayerPage(SecondLayerPage secondLayerPage) {
        secondLayerPageRepository.saveAndFlush(secondLayerPage);
    }

    // update data
    public void updateSecondLayerPage(SecondLayerPage secondLayerPage) {
        secondLayerPageRepository.updatePage2(secondLayerPage.getPagename(),
                secondLayerPage.getPagecontent(),
                secondLayerPage.getP1layer2(), secondLayerPage.getP2layer2(),
                secondLayerPage.getP3layer2(), secondLayerPage.getP4layer2(),
                secondLayerPage.getP5layer2(), secondLayerPage.getP6layer2());
    }

    // load all data table secondLayerPage
    public List<SecondLayerPage> loadAllData() {
        List<SecondLayerPage> listall = new ArrayList<>();
        listall = secondLayerPageRepository.findAll();
        return listall;
    }
}
