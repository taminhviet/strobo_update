package com.aht.tb.config;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.util.FileCopyUtils;

import com.aht.tb.entity.User;
import com.aht.tb.repository.ConfigRepository;
import com.aht.tb.repository.UserRepository;
import com.aht.tb.service.ConfigService;


@Component
public class BackupData {
	@Value("${spring.datasource.url}")
	private String urlDb;
	@Autowired
	ConfigRepository configRepository;
	@Autowired
	ConfigService configService;
	@Autowired
	UserRepository userRepository;
	@Autowired
	private PasswordEncoder passwordEncoder;
	// link db Backup
	public String getFolderUpload(HttpServletRequest request) {
		String realPath = request.getServletContext().getRealPath("media");
		String uploadRootPath = realPath + File.separator + "db";
		File folderUpload = new File(uploadRootPath);
		if (!folderUpload.exists()) {
			folderUpload.mkdirs();
		}
		return folderUpload.toString();
	}

	// Zip data backup truyền vào thư mục nén và thư mục chứa file zip
	public void zipData(File inputDir, File outputZipFile) {
		// Tạo thư mục cha cho file đầu ra (output file).
		outputZipFile.getParentFile().mkdirs();

		String inputDirPath = inputDir.getAbsolutePath();
		byte[] buffer = new byte[1024];

		FileOutputStream fileOs = null;
		ZipOutputStream zipOs = null;
		try {

			List<File> allFiles = this.listChildFiles(inputDir);

			// Tạo đối tượng ZipOutputStream để ghi file zip.
			fileOs = new FileOutputStream(outputZipFile);
			//
			zipOs = new ZipOutputStream(fileOs);
			for (File file : allFiles) {
				String filePath = file.getAbsolutePath();
				// entryName: is a relative path.
				String entryName = filePath.substring(inputDirPath.length() + 1);

				ZipEntry ze = new ZipEntry(entryName);
				// Thêm entry vào file zip.
				zipOs.putNextEntry(ze);
				// Đọc dữ liệu của file và ghi vào ZipOutputStream.
				FileInputStream fileIs = new FileInputStream(filePath);

				int len;
				while ((len = fileIs.read(buffer)) > 0) {
					zipOs.write(buffer, 0, len);
				}
				fileIs.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Zip fail");
		} finally {
			closeQuite(zipOs);
			closeQuite(fileOs);
		}

	}

	private void closeQuite(OutputStream out) {
		try {
			out.close();
		} catch (Exception e) {
		}
	}

	// Phương thức này trả về danh sách các file,
	// bao gồm tất cả các file con, cháu,.. của thư mục đầu vào.
	private List<File> listChildFiles(File dir) throws IOException {
		List<File> allFiles = new ArrayList<File>();

		File[] childFiles = dir.listFiles();
		for (File file : childFiles) {
			if (file.isFile()) {
				allFiles.add(file);
			} else {
				List<File> files = this.listChildFiles(file);
				allFiles.addAll(files);
			}
		}
		return allFiles;
	}

	// unzip
	public void Unzip(String output, String input) {
		// Tạo thư mục Output nếu nó không tồn tại.
		File folder = new File(output);
		if (!folder.exists()) {
			folder.mkdirs();
		}
		// Tạo một buffer (Bộ đệm).
		byte[] buffer = new byte[1024];

		ZipInputStream zipIs = null;
		try {
			// Tạo đối tượng ZipInputStream để đọc file từ 1 đường dẫn (path).
			zipIs = new ZipInputStream(new FileInputStream(input));

			ZipEntry entry = null;
			// Duyệt từng Entry (Từ trên xuống dưới cho tới hết)
			while ((entry = zipIs.getNextEntry()) != null) {
				String entryName = entry.getName();
				// Cắt chuỗi nếu theo \\
				String[] entrySub = entryName.split("\\\\", 200);
				// Tạo src để lấy đường dẫn
				String src = output ;
				if (entrySub.length > 0) {
					for (int i = 0; i < entrySub.length-1; i++) {
						src += "\\" + entrySub[i];
						File file = new File(src);
						// Tạo folder
						if (!file.isDirectory()) {
							new File(src).mkdirs();
						}
					}
				}
				String outFileName = output + File.separator + entryName;
				// Tạo một Stream để ghi dữ liệu vào file.
				FileOutputStream fos = new FileOutputStream(outFileName);
				int len;
				// Đọc dữ liệu trên Entry hiện tại.
				while ((len = zipIs.read(buffer)) > 0) {
					fos.write(buffer, 0, len);
				}
				fos.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Unzip Fail");
		} finally {
			try {
				zipIs.close();
				//Xoa file backup.zip
				Path path = Paths.get(input);
				Files.deleteIfExists(path);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	// Download File zip
	public void downloadZip(HttpServletResponse response, String url) {
		try {
			File file = new File(url);
			byte[] data = FileUtils.readFileToByteArray(file);
			// Thiết lập thông tin trả về
			response.setContentType("application/octet-stream");
			response.setHeader("Content-disposition", "attachment; filename=" + file.getName());
			response.setContentLength(data.length);
			InputStream inputStream = new BufferedInputStream(new ByteArrayInputStream(data));
			FileCopyUtils.copy(inputStream, response.getOutputStream());
		} catch (Exception ex) {
			ex.printStackTrace();
			System.out.println("Download Fail");
		}
	}
	
	
	//Backup db
	public String backup(HttpServletRequest request) throws Exception {
		//Link file chua .sql
		Path pathBackup = Paths.get(this.getFolderUpload(request)+"/db.sql");
		Connection conn = DriverManager.getConnection(urlDb, "root", "");
		Statement stmt = conn.createStatement();
		stmt.executeQuery(String.format("SCRIPT TO '%s'", pathBackup));
		conn.close();
		return "backup";
	}

	//Restore db
	public String restoreDb(HttpServletRequest request) throws Exception {
		//Tim file db.sql
		Path pathBackup = Paths.get(this.getFolderUpload(request)+"/db.sql");
		Connection conn = DriverManager.getConnection(urlDb, "root", "");
		Statement stmt = conn.createStatement();
		stmt.executeUpdate("DROP ALL OBJECTS");
		stmt.execute(String.format("RUNSCRIPT FROM '"+pathBackup+"'"));
		//Del key
		configService.removeKey(request);
		//Default pass
		try {
			User user = userRepository.findByUsername("admin");
			user.setPassword(passwordEncoder.encode("admin"));
			userRepository.save(user);
		} catch (Exception e) {
			e.printStackTrace();
		}
		//Xoa file Db.sql
		Files.delete(pathBackup);
		stmt.close();
		conn.close();
		return "restore";
	}
}
