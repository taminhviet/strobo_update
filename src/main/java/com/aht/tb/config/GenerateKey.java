package com.aht.tb.config;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;

@Component
public class GenerateKey {

	private static final String ALPHA_NUMERIC_STRING = "abcdefghiklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

	public String randomKey(int count) {
		StringBuilder builder = new StringBuilder();
		while (count-- != 0) {
			int character = (int) (Math.random() * ALPHA_NUMERIC_STRING.length());
			builder.append(ALPHA_NUMERIC_STRING.charAt(character));
		}
		return builder.toString();
	}

	// Check mail
	public boolean checkMail(String fromEmail, String fromPassword, String toEmail) {
		boolean success = false;
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");

		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(fromEmail, fromPassword);
			}
		});

		try {
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(fromPassword));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail));
			message.setSubject("Successfully added Gmail");
			message.setText("Successfully added Gmail");
			Transport.send(message);
			success = true;
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
		return success;
	}

	// Send Mail
	public boolean sendEmail(String fromEmail, String fromPassword, String toEmail, String key, String getURLBase) {
		boolean success = false;
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");

		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(fromEmail, fromPassword);
			}
		});

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(fromPassword));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail));
			message.setSubject("The key to login");
			String html = "This is the key to login:    " + key
					+ "\n<a style=\"border: 1px solid green; border-radius: 3px; padding: 5px; display: inline-block; cursor: pointer; color: green;\" href='"
					+ getURLBase + "/admin/login/" + key + "'>Login Now</a>";
			message.setContent(html, "text/html");
			Transport.send(message);
			success = true;
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
		return success;
	}

	public String getURLBase(HttpServletRequest request) throws MalformedURLException {

		URL requestURL = new URL(request.getRequestURL().toString());
		String port = requestURL.getPort() == -1 ? "" : ":" + requestURL.getPort();
		return requestURL.getProtocol() + "://" + requestURL.getHost() + port;

	}
}
