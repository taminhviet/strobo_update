package com.aht.tb.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.aht.tb.entity.ConfigPage;
import com.aht.tb.service.ConfigService;

@Controller
public class MyExceptionController implements ErrorController {
	@Autowired
	private ConfigService configService;

	@Override
	public String getErrorPath() {
		return "/error";
	}

	@RequestMapping(value = "/error")
	public String handleError(Model model) {
		List<ConfigPage> listConfigPage = configService.findConfigPage();
		if (listConfigPage.size() > 0) {
			model.addAttribute("config", listConfigPage.get(0));
		} else {
			model.addAttribute("config", null);
		}
		return "error404";
	}
}
