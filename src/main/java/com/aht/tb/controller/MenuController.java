package com.aht.tb.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.aht.tb.entity.GenericPage;
import com.aht.tb.entity.Menu;
import com.aht.tb.service.GenericPageService;
import com.aht.tb.service.MenuService;

@Controller
public class MenuController {
	@Autowired
	private MenuService menuService;

	@Autowired
	private GenericPageService genericPageService;

	public boolean success = false;
	
	public boolean flag = false;

	// Show all menu
	@RequestMapping(value = "/admin/menu", method = RequestMethod.GET)
	private String getMenu(Model model) {

		// Show all menu
		List<Menu> menuNew = menuService.getMenuByType("new");
		if (menuNew == null || menuNew.isEmpty()) {
			model.addAttribute("menuNew", null);
		} else {
			model.addAttribute("menuNew", menuNew);
		}

		List<Menu> menuDefault = menuService.getMenuByType("default");
		if (menuDefault == null || menuDefault.isEmpty()) {
			model.addAttribute("menuDefault", null);
		} else {
			model.addAttribute("menuDefault", menuDefault);
		}

		model.addAttribute("flag", flag);
		model.addAttribute("success", success);
		flag=false;
		

		List<String> urlGenericPage = new ArrayList<>();
		for (GenericPage gp : genericPageService.getAllGenericPage()) {
			urlGenericPage.add(gp.getAlias());
		}

		List<String> urlUseds = new ArrayList<>();
		for (Menu mn : menuNew) {
			if (!mn.getUrl().isEmpty()) {
				urlUseds.add(mn.getUrl());
			}
		}

		List<String> urlFrees = new ArrayList<>();
		for (String url : urlGenericPage) {
			if (!urlUseds.contains(url)) {
				urlFrees.add(url);
			}
		}

		model.addAttribute("urlFrees", urlFrees);
		model.addAttribute("menuNew", menuNew);
		model.addAttribute("menuDefault", menuDefault);
		model.addAttribute("urlUseds", urlUseds);

		return "menuPage";
	}

	// Delete menu
	@RequestMapping(value = "/admin/menu/delete/{id}", method = RequestMethod.POST)
	public String deleteMenuById(@PathVariable("id") int id, Model model) {
		success = menuService.deleteById(id);
		flag = true;
		return "redirect:/admin/menu";
	}

	// Save and Update Menu
	@RequestMapping(value = "/admin/menu", method = RequestMethod.POST)
	public String saveMenu(@ModelAttribute() Menu menu, Model model) {
		success = menuService.saveMenu(menu);
		flag = true;
		return "redirect:/admin/menu";
	}
}
