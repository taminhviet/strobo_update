package com.aht.tb.controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.view.RedirectView;

import com.aht.tb.entity.HomePage;
import com.aht.tb.entity.MyFile;
import com.aht.tb.service.HomePageService;
import com.aht.tb.utils.FileUtils;

@Controller
public class HomePageController {
	// Xu ly duong dan

	public File getFolderUpload(HttpServletRequest request) {
		String realPath = request.getServletContext().getRealPath("media");
		String uploadRootPath = realPath + File.separator + "video" + File.separator + "home";
		File folderUpload = new File(uploadRootPath);
		if (!folderUpload.exists()) {
			folderUpload.mkdirs();
		}
		return folderUpload;
	}

	@Autowired
	private HomePageService homePageService;

	// View Home Page Config
	@RequestMapping(value = { "/admin/home" }, method = RequestMethod.GET)
	public String dohomePage(Model model, HttpServletRequest request) {

		List<HomePage> listHomePage = homePageService.findHomePage();
		if (listHomePage.size() > 0) {
			model.addAttribute("homePage", listHomePage.get(0));
		} else {
			model.addAttribute("homePage", null);
		}
		// Get Files In Folder
		File[] listFiles = this.getFolderUpload(request).listFiles();
		model.addAttribute("listFiles", listFiles);
		return "homePage";
	}

	// Update Home Page Config
	@RequestMapping(value = { "/admin/home" }, method = RequestMethod.POST)
	public String saveHomePage(@ModelAttribute() HomePage homePage, @ModelAttribute("multipartFile") MyFile myFile,
			Model model, HttpServletRequest request) {
		// Upload File
		MultipartFile multipartFile = myFile.getMultipartFile();
		String fileName = multipartFile.getOriginalFilename();
		if(fileName != null && !fileName.isEmpty()){
		    FileUtils.saveVideo(request, fileName, multipartFile, "home");
		}
//		try {
//			File file = new File(this.getFolderUpload(request), fileName);
//			multipartFile.transferTo(file);
//		} catch (Exception e) {
//			// e.printStackTrace();
//		}
		List<HomePage> listHomepage = homePageService.findHomePage();
		if (listHomepage.size() > 0) {
			homePage.setId(listHomepage.get(0).getId());
			// Set CreateDate
			if (listHomepage.get(0).getCreatedDate() == null || listHomepage.get(0).getCreatedDate() == "") {
				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				Date date = new Date();
				String day = dateFormat.format(date);
				homePage.setCreatedDate(day);
			} else {
				homePage.setCreatedDate(listHomepage.get(0).getCreatedDate());
			}
			// Kiem tra ten file
			if (fileName.length() > 0) {
				homePage.setFile(fileName);
			} else {
				// Kiá»ƒm tra file cÃ³ trong thu má»¥c khÃ´ng
				File[] listFiles = this.getFolderUpload(request).listFiles();
				boolean check = false;
				for (File file : listFiles) {
					// Náº¿u file cÃ³ tá»“n táº¡i tráº£ vá»� true
					if (file.getName().equals(homePage.getFile())) {
						check = true;
					}
					// náº¿u cÃ³ file sáº½ tráº£ vá»� true
				}
				if (check == false) {
					homePage.setFile(null);
				}
			}
			/*
			 * if (fileName.length() > 0) { if(homePage.getFile() == null) {
			 * homePage.setFile(fileName); } else {
			 * homePage.setFile(listHomepage.get(0).getFile()); } }
			 */
			// ---------
		} else {
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date date = new Date();
			String day = dateFormat.format(date);
			homePage.setCreatedDate(day);
		}
		// save va thong bao ket qua
		boolean success = homePageService.saveHomePage(homePage);
		// Get Files In Folder
		File[] listFiles = this.getFolderUpload(request).listFiles();
		model.addAttribute("listFiles", listFiles);
		model.addAttribute("success", success);
		return "homePage";
	}

	// Xoa video trong thu muc roi chuyen huong /admin/home
	// Truyá»�n vÃ o biáº¿n fileDel theo phÆ°Æ¡ng thá»©c post
	@RequestMapping(value = "/admin/home/delete/{filename}", method = RequestMethod.POST)
	public RedirectView delFile(@PathVariable("filename") String name, HttpServletRequest request) {
		Path path = Paths.get(this.getFolderUpload(request) + "/" + name);
		try {
			Files.deleteIfExists(path);
		} catch (IOException e) {

		}
		RedirectView rw = new RedirectView();
		rw.setUrl("/admin/home");
		return rw;
	}

}
