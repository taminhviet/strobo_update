package com.aht.tb.controller;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import com.aht.tb.config.BackupData;
import com.aht.tb.entity.ConfigPage;
import com.aht.tb.entity.MyFile;
import com.aht.tb.service.ConfigService;
import com.aht.tb.service.GGCaptchaService;
import com.aht.tb.service.UserService;
import com.aht.tb.service.NewsService;
import com.aht.tb.entity.NewsEvent;

@Controller
public class BaseController {
	@Autowired
	UserService userService;
	@Autowired
	ConfigService configService;

//	@Autowired
//	NewsService newsPage;

	@Autowired
	private GGCaptchaService captchaService;

	// BackupData
	@Autowired
	BackupData backupData;
	// Xu ly duong dan

	public String getFolderUpload(HttpServletRequest request, String folder) {
		// Lay duong dan chinh xac cua folder
		String realPath = request.getServletContext().getRealPath(folder);
		File folderUpload = new File(realPath);
		if (!folderUpload.exists()) {
			folderUpload.mkdirs();
		}
		return realPath;
	}


	@RequestMapping(value = { "/admin/news" }, method = RequestMethod.POST)
	public String newsAdd(@RequestParam(name = "title") String title,
						  @RequestParam(name = "short_des") String short_des,
						  @RequestParam(name = "description") String description,
						  @RequestParam(name = "feature") String feature, Model model) {
		System.out.println(title);
		System.out.println(description);
		System.out.println(feature);
		return "news";
	}

	@RequestMapping(value = { "/admin/newspage" }, method = RequestMethod.GET)
	public String newspage(Model model) {
		return "newspage";
	}

	@RequestMapping(value = { "/admin/eventspage" }, method = RequestMethod.GET)
	public String eventspage(Model model) {
		return "eventspage";
	}

	@RequestMapping(value = "/admin/login/{alias}", method = RequestMethod.GET)
	public String getLogin(Model model, @PathVariable("alias") String alias, HttpServletRequest request) {
		if (alias.length() > 0) {
			if (configService.checkKey(alias, request)) {

				boolean status = captchaService.getCaptcha().isStatus();
				String siteKey = captchaService.getCaptcha().getSiteKey();
				if (status && siteKey != null) {
					model.addAttribute("status", status);
					model.addAttribute("site_key", siteKey);
				}
				return "login";
			}
		}
		return "error";

	}

	@RequestMapping(value = "/admin/backup", method = RequestMethod.GET)
	public String backupData(HttpServletRequest request, HttpServletResponse response) {
		// zipdata
		try {
			File inputDir = new File(this.getFolderUpload(request, "media"));
			backupData.backup(request);
			DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd-HH_mm");
			Date date = new Date();
			String day = dateFormat.format(date);
			File outputZipFile = new File(this.getFolderUpload(request, "backup") + "/backup" + day + ".zip");
			backupData.zipData(inputDir, outputZipFile);
			// Download .zip
			String url = this.getFolderUpload(request, "backup") + "/backup" + day + ".zip";
			backupData.downloadZip(response, url);
			// Xoa file backup.zip
			File delFile = new File(this.getFolderUpload(request, "backup") + "/backup" + day + ".zip");
			if (delFile.exists()) {
				delFile.delete();
			}
			// Xoa file db.sql
			File delDbSql = new File(this.getFolderUpload(request, "media") + "/db/db.sql");
			if (delDbSql.exists()) {
				delDbSql.delete();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@RequestMapping(value = "/admin/restore", method = RequestMethod.POST)
	public String restore(HttpServletRequest request, Model model, @ModelAttribute("multipartFile") MyFile myFile) {
		// Upload file
		MultipartFile multipartFile = myFile.getMultipartFile();
		String fileName = multipartFile.getOriginalFilename();
		if (fileName.length() > 0) {
			try {

				File file = new File(this.getFolderUpload(request, "backup") + "/backup.zip");
				multipartFile.transferTo(file);
			} catch (Exception e) {
				// e.printStackTrace();
			}
			//
			String input = this.getFolderUpload(request, "backup") + "/backup.zip";
			String output = this.getFolderUpload(request, "media");
			backupData.Unzip(output, input);
			try {
				backupData.restoreDb(request);
				model.addAttribute("restoreSuccess", true);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			model.addAttribute("restoreSuccess", false);
		}
		// Get Data
		List<ConfigPage> listConfigPage = configService.findConfigPage();
		if (listConfigPage.size() > 0) {
			model.addAttribute("config", listConfigPage.get(0));
		}
		return "configPage";
	}

	@RequestMapping(value = { "/admin/profile" }, method = RequestMethod.GET)
	public String profilePage(Model model) {
		model.addAttribute("user", userService.getCurrentUserLogin());
		return "profilePage";
	}

	@RequestMapping(value = { "/login" }, method = RequestMethod.GET)
	public String errorLogin(Model model) {
		return "errorlogin";
	}
}
