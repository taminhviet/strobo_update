package com.aht.tb.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.aht.tb.entity.FullSecondLayerPage;
import com.aht.tb.entity.FullThirdLayerPage;
import com.aht.tb.entity.ImageContent;
import com.aht.tb.entity.SecondLayerPage;
import com.aht.tb.entity.SlideImage;
import com.aht.tb.service.ImageContentService;
import com.aht.tb.service.SecondLayerPageService;
import com.aht.tb.service.SlideImageService;
import com.aht.tb.utils.Constants;
import com.aht.tb.utils.FileUtils;

@Controller
public class SecondLayerController {

    private static final Logger logger = Logger
            .getLogger(SecondLayerController.class);
    @Autowired
    SecondLayerPageService secondLayerPageService;

    @Autowired
    ImageContentService imageContentService;

    @Autowired
    SlideImageService slideImageService;

    @RequestMapping(value = { "/loadfulldata" }, method = RequestMethod.GET)
    private String testAPI(Model model) throws Exception {
        List<SecondLayerPage> listSecondLayerPages = secondLayerPageService
                .loadAllData();
        List<ImageContent> listImageContent = imageContentService.loadAllData();
        List<FullSecondLayerPage> listFullSecondLayerPages = new ArrayList<FullSecondLayerPage>();
        // add all data second layer
        if (!listSecondLayerPages.isEmpty()) {
            for (Iterator<SecondLayerPage> iterator = listSecondLayerPages
                    .iterator(); iterator.hasNext();) {
                SecondLayerPage secondLayerPage = (SecondLayerPage) iterator
                        .next();
                FullSecondLayerPage fullSecondLayerPage = new FullSecondLayerPage();
                fullSecondLayerPage.setPageName(secondLayerPage.getPagename());
//                String mainImg = FileUtils.encodeImage(secondLayerPage
//                        .getLayer2mainimg());
                fullSecondLayerPage.setPageContent(secondLayerPage
                        .getPagecontent());
                String p1L1 = secondLayerPage.getP1layer2();
                String p2L1 = secondLayerPage.getP2layer2();
                String p3L1 = secondLayerPage.getP3layer2();
                String p4L1 = secondLayerPage.getP4layer2();
                String p5L1 = secondLayerPage.getP5layer2();
                String p6L1 = secondLayerPage.getP6layer2();
                fullSecondLayerPage.setP1(p1L1);
                fullSecondLayerPage.setP2(p2L1);
                fullSecondLayerPage.setP3(p3L1);
                fullSecondLayerPage.setP4(p4L1);
                fullSecondLayerPage.setP5(p5L1);
                fullSecondLayerPage.setP6(p6L1);
//                fullSecondLayerPage.setMainImg(mainImg);
                listFullSecondLayerPages.add(fullSecondLayerPage);
            }
        }

        // add image and content for each position
        if (!listFullSecondLayerPages.isEmpty()) {
            for (FullSecondLayerPage fullSecondLayerPage : listFullSecondLayerPages) {
                String pageLayer2Name = fullSecondLayerPage.getPageName();
                switch (pageLayer2Name) {
                case Constants.PAGE_AIRPORT:
                    fullSecondLayerPage = setPositionContent(
                            fullSecondLayerPage, listImageContent,
                            Constants.PAGE_AIRPORT);
                    model.addAttribute(Constants.PAGE_AIRPORT,
                            fullSecondLayerPage);
                    break;
                case Constants.PAGE_MANUFACTURING:
                    fullSecondLayerPage = setPositionContent(
                            fullSecondLayerPage, listImageContent,
                            Constants.PAGE_MANUFACTURING);
                    model.addAttribute(Constants.PAGE_MANUFACTURING,
                            fullSecondLayerPage);
                    break;
                case Constants.PAGE_WARE_HOUSE:
                    fullSecondLayerPage = setPositionContent(
                            fullSecondLayerPage, listImageContent,
                            Constants.PAGE_WARE_HOUSE);
                    model.addAttribute(Constants.PAGE_WARE_HOUSE,
                            fullSecondLayerPage);
                    break;
                case Constants.PAGE_SEAPORT:
                    fullSecondLayerPage = setPositionContent(
                            fullSecondLayerPage, listImageContent,
                            Constants.PAGE_SEAPORT);
                    model.addAttribute(Constants.PAGE_SEAPORT,
                            fullSecondLayerPage);
                    break;
                default:
                    break;
                }
            }
        }
        // set content for third layer page
        if (!listImageContent.isEmpty()) {
            for (ImageContent imageContent : listImageContent) {
                FullThirdLayerPage fullThirdLayerPage = new FullThirdLayerPage();
                String imagecontentPagename = imageContent.getImagename();
                fullThirdLayerPage.setPageName(imagecontentPagename);
                fullThirdLayerPage.setPageTitle(imagecontentPagename);
                fullThirdLayerPage.setContent(imageContent.getContent());
                fullThirdLayerPage.setDetailContent(imageContent
                        .getDetailcontent());
//                String mainImg = FileUtils.encodeImage(imageContent
//                        .getMainimg());
//                fullThirdLayerPage.setMainImg(mainImg);
                boolean hasSlide = imageContent.isHasslide();
                // include setting display slide
                if (hasSlide) {
                    SlideImage slideImage = slideImageService
                            .getSlideImagebyName(imagecontentPagename);
//                    if (slideImage != null) {
//                        fullThirdLayerPage.setImgSlide1(slideImage
//                                .getSlideimg1());
//                        fullThirdLayerPage.setImgSlide2(slideImage
//                                .getSlideimg2());
//                        fullThirdLayerPage.setImgSlide3(slideImage
//                                .getSlideimg3());
//                        fullThirdLayerPage.setImgSlide4(slideImage
//                                .getSlideimg4());
//                    }
                }
                model.addAttribute(imagecontentPagename, fullThirdLayerPage);
            }
        }

        return "redirect:/index";
    }

    /**
     * get data by page name
     * 
     * @param pageid
     * @param model
     * @return
     */
    @RequestMapping(value = { "/firstSection/1/secondLayerPage/{pageid}" }, method = RequestMethod.GET)
    private String getSecondLayerPage(@PathVariable("pageid") Integer pageid,
            Model model) {
        logger.debug("Start loading page id: " + pageid);
        String pageName = Constants.EMPTY_STRING;
        // get Page name by page ID
        switch (pageid) {
        case 1:
            pageName = Constants.PAGE_AIRPORT;
            break;
        case 2:
            pageName = Constants.PAGE_SEAPORT;
            break;
        case 3:
            pageName = Constants.PAGE_WARE_HOUSE;
            break;
        case 4:
            pageName = Constants.PAGE_MANUFACTURING;
            break;
        default:
            break;
        }
        SecondLayerPage secondLayerPage = secondLayerPageService
                .getSecondPagebyName(pageName);
        FullSecondLayerPage fullSecondLayerPage = new FullSecondLayerPage();
        fullSecondLayerPage.setPageName(pageName);
        fullSecondLayerPage.setPageTitle(pageName);
        fullSecondLayerPage.setMainImg("");
        if (secondLayerPage != null) {
            // 1st position in page
            String p1Layer2 = secondLayerPage.getP1layer2();
            if (p1Layer2 != null) {
                Map<String, byte[]> mapContentImage1 = getFixedImageContent(p1Layer2);
                if (mapContentImage1 != null) {
                    for (Map.Entry<String, byte[]> entry : mapContentImage1
                            .entrySet()) {
                        fullSecondLayerPage.setP1Content(entry.getKey());
                        fullSecondLayerPage.setP1Image(FileUtils
                                .encodeImage(entry.getValue()));
                    }
                }
            }

            // 2nd position in page
            String p2Layer2 = secondLayerPage.getP2layer2();
            if (p2Layer2 != null) {
                Map<String, byte[]> mapContentImage2 = getFixedImageContent(p1Layer2);
                if (mapContentImage2 != null) {
                    for (Map.Entry<String, byte[]> entry : mapContentImage2
                            .entrySet()) {
                        fullSecondLayerPage.setP2Content(entry.getKey());
                        fullSecondLayerPage.setP2Image(FileUtils
                                .encodeImage(entry.getValue()));
                    }
                }
            }

            // 3rd position in page
            String p3Layer2 = secondLayerPage.getP3layer2();
            if (p3Layer2 != null) {
                Map<String, byte[]> mapContentImage3 = getFixedImageContent(p1Layer2);
                if (mapContentImage3 != null) {
                    for (Map.Entry<String, byte[]> entry : mapContentImage3
                            .entrySet()) {
                        fullSecondLayerPage.setP3Content(entry.getKey());
                        fullSecondLayerPage.setP3Image(FileUtils
                                .encodeImage(entry.getValue()));
                    }
                }
            }

            // 4th position in page
            String p4Layer2 = secondLayerPage.getP4layer2();
            if (p4Layer2 != null) {
                Map<String, byte[]> mapContentImage4 = getFixedImageContent(p1Layer2);
                if (mapContentImage4 != null) {
                    for (Map.Entry<String, byte[]> entry : mapContentImage4
                            .entrySet()) {
                        fullSecondLayerPage.setP4Content(entry.getKey());
                        fullSecondLayerPage.setP4Image(FileUtils
                                .encodeImage(entry.getValue()));
                    }
                }
            }

            // 5th position in page
            String p5Layer2 = secondLayerPage.getP5layer2();
            if (p5Layer2 != null) {
                Map<String, byte[]> mapContentImage5 = getFixedImageContent(p1Layer2);
                if (mapContentImage5 != null) {
                    for (Map.Entry<String, byte[]> entry : mapContentImage5
                            .entrySet()) {
                        fullSecondLayerPage.setP5Content(entry.getKey());
                        fullSecondLayerPage.setP5Image(FileUtils
                                .encodeImage(entry.getValue()));
                    }
                }
            }

            // 6th position in page
            String p6Layer2 = secondLayerPage.getP6layer2();
            if (p6Layer2 != null) {
                Map<String, byte[]> mapContentImage6 = getFixedImageContent(p1Layer2);
                if (mapContentImage6 != null) {
                    for (Map.Entry<String, byte[]> entry : mapContentImage6
                            .entrySet()) {
                        fullSecondLayerPage.setP6Content(entry.getKey());
                        fullSecondLayerPage.setP6Image(FileUtils
                                .encodeImage(entry.getValue()));
                    }
                }
            }

        }
        model.addAttribute(fullSecondLayerPage);
        return "secondlayerpage";
    }

    @RequestMapping(value = { "/admin/secondlayer" }, method = RequestMethod.GET)
    private String getImageContentPage(Model model) {
        model.addAttribute("success", false);
        return "detailservice";
    }

    /**
     * 
     * @param secondLayerPage
     * @param model
     * @return
     * @throws IOException
     */
    @RequestMapping(value = { "/admin/secondlayer" }, method = RequestMethod.POST)
    private String saveSecondLayerPage(
            SecondLayerPage secondLayerPage,
            Model model,
            @RequestParam(value = "fileImageLayer2", required = false) MultipartFile myFile,
            RedirectAttributes ra, javax.servlet.http.HttpServletRequest request) throws Exception {
        String pageName = secondLayerPage.getPagename();
        String pageContent = secondLayerPage.getPagecontent();
        logger.debug("save page name: " + pageName);
        byte[] layer2mainimg = null;
        if (myFile != null && myFile.getBytes().length > 0) {
            layer2mainimg = myFile.getBytes();
            FileUtils.saveFolderImagesPath(request, pageName, layer2mainimg);
        }
        SecondLayerPage newSecondLayerPage = new SecondLayerPage();
        newSecondLayerPage.setPagename(pageName);
        newSecondLayerPage.setPagecontent(pageContent);
//        newSecondLayerPage.setLayer2mainimg(layer2mainimg);
        newSecondLayerPage.setP1layer2(pageName + "1");
        newSecondLayerPage.setP2layer2(pageName + "2");
        newSecondLayerPage.setP3layer2(pageName + "3");
        if (Constants.PAGE_AIRPORT.equalsIgnoreCase(pageName)
                || Constants.PAGE_WARE_HOUSE.equalsIgnoreCase(pageName)) {
            newSecondLayerPage.setP4layer2(pageName + "4");
            newSecondLayerPage.setP5layer2(pageName + "5");
        }
        if (Constants.PAGE_WARE_HOUSE.equalsIgnoreCase(pageName)) {
            newSecondLayerPage.setP6layer2(pageName + "6");
        }
        // check exist in database
        SecondLayerPage existPage = secondLayerPageService
                .getSecondPagebyName(pageName);
        if (existPage != null) {
            // update data
            if (pageContent == null || pageContent.isEmpty()) {
                newSecondLayerPage.setPagecontent(existPage.getPagecontent());
            }
//            if (layer2mainimg == null) {
//                newSecondLayerPage.setLayer2mainimg(existPage
//                        .getLayer2mainimg());
//            }
            secondLayerPageService.updateSecondLayerPage(newSecondLayerPage);
        } else {
            // save data
            secondLayerPageService.saveSecondLayerPage(newSecondLayerPage);
        }
        ra.addAttribute("success", true);
        ra.addAttribute("pagename", pageName);
        return "redirect:/admin/detailservice";
    }

    // get small image content fixed
    private Map<String, byte[]> getFixedImageContent(String name) {
        logger.debug("get image content with name: " + name);
        ImageContent imageContent = imageContentService
                .getImageContentbyName(name);
        if (imageContent != null) {
            String content = imageContent.getContent();
//            byte[] layer3mainimg = imageContent.getMainimg();
            Map<String, byte[]> mapData = new HashMap<String, byte[]>();
//            mapData.put(content, layer3mainimg);
            return mapData;
        }
        return null;
    }

    // set content for image position
    private FullSecondLayerPage setPositionContent(
            FullSecondLayerPage fullSecondLayerPage,
            List<ImageContent> listImageContent, String pageType) {
        // set position 1
        String p1 = fullSecondLayerPage.getP1();
        if (p1 != null) {
            ImageContent ic1 = listImageContent
                    .stream()
                    .filter(ImageContent -> ImageContent.getImagename().equals(
                            p1)).findAny().orElse(null);
            if (ic1 != null) {
                fullSecondLayerPage.setP1Content(ic1.getContent());
//                String p1Image = FileUtils.encodeImage(ic1.getMainimg());
//                fullSecondLayerPage.setP1Image(p1Image);
            }
        }
        // set position 2
        String p2 = fullSecondLayerPage.getP2();
        if (p2 != null) {
            ImageContent ic2 = (ImageContent) listImageContent
                    .stream()
                    .filter(ImageContent -> ImageContent.getImagename().equals(
                            p2)).findAny().orElse(null);
            if (ic2 != null) {
                fullSecondLayerPage.setP2Content(ic2.getContent());
//                String p2Image = FileUtils.encodeImage(ic2.getMainimg());
//                fullSecondLayerPage.setP2Image(p2Image);
            }
        }
        // set position 3
        String p3 = fullSecondLayerPage.getP3();
        if (p3 != null) {
            ImageContent ic3 = (ImageContent) listImageContent
                    .stream()
                    .filter(ImageContent -> ImageContent.getImagename().equals(
                            p3)).findAny().orElse(null);
            if (ic3 != null) {
                fullSecondLayerPage.setP3Content(ic3.getContent());
//                String p3Image = FileUtils.encodeImage(ic3.getMainimg());
//                fullSecondLayerPage.setP3Image(p3Image);
            }
        }
        // only page airport and page ware house has pos4 and pos5
        if (Constants.PAGE_AIRPORT.equalsIgnoreCase(pageType)
                || Constants.PAGE_WARE_HOUSE.equalsIgnoreCase(pageType)) {
            // set position 4
            String p4 = fullSecondLayerPage.getP4();
            if (p4 != null) {
                ImageContent ic4 = (ImageContent) listImageContent
                        .stream()
                        .filter(ImageContent -> ImageContent.getImagename()
                                .equals(p4)).findAny().orElse(null);
                if (ic4 != null) {
                    fullSecondLayerPage.setP4Content(ic4.getContent());
//                    String p4Image = FileUtils.encodeImage(ic4.getMainimg());
//                    fullSecondLayerPage.setP4Image(p4Image);
                }
            }
            // set position 5
            String p5 = fullSecondLayerPage.getP5();
            if (p5 != null) {
                ImageContent ic5 = (ImageContent) listImageContent
                        .stream()
                        .filter(ImageContent -> ImageContent.getImagename()
                                .equals(p5)).findAny().orElse(null);
                if (ic5 != null) {
                    fullSecondLayerPage.setP5Content(ic5.getContent());
//                    String p5Image = FileUtils.encodeImage(ic5.getMainimg());
//                    fullSecondLayerPage.setP5Image(p5Image);
                }
            }

        }
        // only ware house has pos 6
        if (Constants.PAGE_WARE_HOUSE.equalsIgnoreCase(pageType)) {
            // set position 6
            String p6 = fullSecondLayerPage.getP6();
            if (p6 != null) {
                ImageContent ic6 = (ImageContent) listImageContent
                        .stream()
                        .filter(ImageContent -> ImageContent.getImagename()
                                .equals(p6)).findAny().orElse(null);
                if (ic6 != null) {
                    fullSecondLayerPage.setP6Content(ic6.getContent());
//                    String p6Image = FileUtils.encodeImage(ic6.getMainimg());
//                    fullSecondLayerPage.setP6Image(p6Image);
                }
            }
        }
        return fullSecondLayerPage;
    }
}
