package com.aht.tb.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.aht.tb.entity.User;
import com.aht.tb.service.UserService;

@Controller
public class UserController {
	@Autowired
	private UserService userService;
	
	@RequestMapping(value = "/admin/profile/changePassword", method = RequestMethod.POST)
	public String changePassword(@RequestParam(name = "old_password") String old_password,
			@RequestParam(name = "new_password") String new_password, Model model) {
		if (!userService.checkOldPassword(old_password)) {
			model.addAttribute("error", "Old password invalid");
			
			return "changePassword";
		}
		userService.changePassword(new_password);
		
		return "redirect:/admin/profile";
	}

	@RequestMapping(value = "/admin/profile/changePassword", method = RequestMethod.GET)
	public String changePassword() {
		return "changePassword";
	}
	
	@RequestMapping(value = "/admin/profile", method = RequestMethod.POST)
	public String updateUser(User user) {
		userService.updateUser(user);
		return "redirect:/admin/profile";
	}
}
