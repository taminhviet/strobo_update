package com.aht.tb.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.aht.tb.entity.GenericPage;
import com.aht.tb.service.GenericPageService;

@Controller
public class GenericPageController {
	@Autowired
	private GenericPageService genericPageService;

	// Get generic page
	@RequestMapping(value = { "/admin/generate" }, method = RequestMethod.GET)
	private String generatePage(Model model) {
		List<GenericPage> listGenericPage = genericPageService.getAllGenericPage();
		model.addAttribute("listPage", listGenericPage);
		return "generatePage";
	}

	// Save Genneric Page;
	@RequestMapping(value = { "/admin/generate" }, method = RequestMethod.POST)
	private String doSaveGenericPage(@ModelAttribute() GenericPage genericPage, Model model) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		String day = dateFormat.format(date);
		genericPage.setCreatedDate(day);
		// kiem tra alias khong duoc trung voi cac mien da co
		boolean success=false;
		boolean warning =false;
		if ("contact".equals(genericPage.getAlias()) || "service".equals(genericPage.getAlias())
				|| "index".equals(genericPage.getAlias())) {
			warning=true;
		} else {
			if(genericPageService.doSave(genericPage)) {
				success = true;
			}else {
				warning=true;
			}
		}
		model.addAttribute("warning", warning);
		model.addAttribute("success", success);
		// Get generic page
		List<GenericPage> listGenericPage = genericPageService.getAllGenericPage();
		model.addAttribute("listPage", listGenericPage);
		return "generatePage";
	}
	//Delete Generic Page
	@RequestMapping(value= {"/admin/generate/delete/{id}"},method = RequestMethod.POST)
	private String delGenericPage(@PathVariable("id") Integer id, Model model) {
		boolean success = genericPageService.doDel(id);
		model.addAttribute("success",success);
		List<GenericPage> listGenericPage = genericPageService.getAllGenericPage();
		model.addAttribute("listPage", listGenericPage);
		return "generatePage";
	}
}
