package com.aht.tb.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;

import lombok.Data;


@MappedSuperclass
@EntityListeners(Page.class)
@Data
public  abstract class Page implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Column(name="page_name")
	private String pageName;

	@Column
	private String title;

	@Column(length=1500)
	private String meta;

	@Column(length=1500)
	private String keyword;

	@Column(name="created_date",length=1500)
	private String createdDate;

	@Column
	private boolean status;

}
