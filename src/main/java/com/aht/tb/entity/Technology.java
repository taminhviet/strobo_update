package com.aht.tb.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name="technology")
@Data
public class Technology implements Serializable{

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;
    
    @Column(name="technologyName")
    private String technologyName;
    
    @Column(name="title")
    private String title;

    @Column(name="heading1")
    private String heading1;

    @Column(length = 5000,name="description1")
    private String description1;

    @Column(name="heading2")
    private String heading2;
    
    @Column(length = 5000,name="description2")
    private String description2;
}
