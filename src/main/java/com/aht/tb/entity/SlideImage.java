package com.aht.tb.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name ="slide_image")
@Data
public class SlideImage implements Serializable{

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;
    
    @Column(name="slidepage")
    private String slidepage;
    
    @Column(name ="slidename")
    private String slidename;
    
    @Column(name="slideimg")
    private String slideimg;
    
//    @Column(name="slideimg2")
//    private String slideimg2;
//    
//    @Column(name="slideimg3")
//    private String slideimg3;
//    
//    @Column(name="slideimg4")
//    private String slideimg4;
}
