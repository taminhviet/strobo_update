package com.aht.tb.entity;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name = "captcha")
public class GGCaptcha implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column
	private boolean status;

	@Column(name = "site_key")
	private String siteKey;

	public GGCaptcha() {
		super();
	}

	public GGCaptcha(int id, boolean status, String siteKey) {
		super();
		this.id = id;
		this.status = status;
		this.siteKey = siteKey;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getSiteKey() {
		return siteKey;
	}

	public void setSiteKey(String siteKey) {
		this.siteKey = siteKey;
	}

}
