package com.aht.tb.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Email;

import lombok.Data;

@Entity
@Data
public class ConfigPage {
	public static final String KEY_FILE = "/key.txt";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	@Column
	private boolean mode;
	
	@Column(length=2500)
	private String content;
	
	@Column(name="error_head",length=2500)
	private String error_head;
	
	@Column(name="error_content",length=2500)
	private String error_content;
	
	@Column(name="footer_content",length=2500)
	private String footer_content;
	
	@Column(name="email_from")
	@Email
	private String emailFrom;
	
	@Column(name="pass_email")
	private String passEmail;
	
	@Column(length=24)
	private String admin_key;
}
