package com.aht.tb.entity;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Menu")
public class Menu implements Serializable {

	private static final long serialVersionUID = 1L;

//	public static final List<String> URL_DEFAULT = Arrays.asList("/service", "/contact", "/index", "/service#seaport",
//			"/service#airport", "/service#warehourse", "/service#manufacturing");
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;

	@Column(name = "value", unique = true)
	private String value;

	@Column(name = "url", length = 1000)
	private String url;

	@Column(name = "status")
	private boolean status;

	@Column(name = "type", length = 50)
	private String type;

//	@Column(name = "check", length = 50)
//	private String check;
//
//	public String getCheck() {
//		return check;
//	}
//
//	public void setCheck(String check) {
//		this.check = check;
//	}

	public Menu() {
		super();
	}

	public Menu(String value, String url, boolean status, String type) {
		super();
		this.value = value;
		this.url = url;
		this.status = status;
		this.type = type;
	}

	public Menu(int id, String value, String url, boolean status, String type) {
		super();
		this.id = id;
		this.value = value;
		this.url = url;
		this.status = status;
		this.type = type;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
