package com.aht.tb.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
@Entity
@Table(name="second_layer_page")
@Data
public class SecondLayerPage implements Serializable{
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;
    
    @Column(name ="pagename")
    private String pagename;
    
    @Column(name ="pagecontent")
    private String pagecontent;
    
    @Column(name ="p1layer2")
    private String p1layer2;
    
    @Column(name ="p2layer2")
    private String p2layer2;
    
    @Column(name ="p3layer2")
    private String p3layer2;
    
    @Column(name ="p4layer2")
    private String p4layer2;
    
    @Column(name ="p5layer2")
    private String p5layer2;
    
    @Column(name ="p6layer2")
    private String p6layer2;
    
}
