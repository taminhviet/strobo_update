package com.aht.tb.utils;

public class Constants {

    public static final String EMPTY_STRING = "";
    
    public static final String ADMIN_USER_NAME = "userAdmin";
    
    public static final String ADMIN_USER_PASSWORD = "passwordAdmin";
    
    public static final String ADMIN_DEFAULT_AUTHEN = "admin";
    
    public static final String WATCH_VIDEO = "WATCH THE VIDEO";
    
    //page Name secondlayer
    public static final String PAGE_AIRPORT = "pageairport";
    public static final String PAGE_SEAPORT = "pageseaport";
    public static final String PAGE_WARE_HOUSE = "pagewarehouse";
    public static final String PAGE_MANUFACTURING = "pagemanufacturing";
    
    public static final String AIRPORT = "airport";
    public static final String SEAPORT = "seaport";
    public static final String WARE_HOUSE = "warehouse";
    public static final String MANUFACTURING = "manufacturing";
    
    //page Name thirdlayer
    public static final String PAGE_3RD_AIRPORT_1 = "pageairport1";
    public static final String PAGE_3RD_AIRPORT_2 = "pageairport2";
    public static final String PAGE_3RD_AIRPORT_3 = "pageairport3";
    public static final String PAGE_3RD_AIRPORT_4 = "pageairport4";
    public static final String PAGE_3RD_AIRPORT_5 = "pageairport5";
   
    public static final String PAGE_3RD_MANUFACTURING_1 = "pagemanufacturing1";
    public static final String PAGE_3RD_MANUFACTURING_2 = "pagemanufacturing2";
    public static final String PAGE_3RD_MANUFACTURING_3 = "pagemanufacturing3";
    
    public static final String PAGE_3RD_WARE_HOUSE_1 = "pagewarehouse1";
    public static final String PAGE_3RD_WARE_HOUSE_2 = "pagewarehouse2";
    public static final String PAGE_3RD_WARE_HOUSE_3 = "pagewarehouse3";
    public static final String PAGE_3RD_WARE_HOUSE_4 = "pagewarehouse4";
    public static final String PAGE_3RD_WARE_HOUSE_5 = "pagewarehouse5";
    public static final String PAGE_3RD_WARE_HOUSE_6 = "pagewarehouse6";
    
    public static final String PAGE_3RD_SEAPORT_1 = "pageseaport1";
    public static final String PAGE_3RD_SEAPORT_2 = "pageseaport2";
    public static final String PAGE_3RD_SEAPORT_3 = "pageseaport3";
    
    public static final String FEATURE_1 = "feature1";
    public static final String FEATURE_2 = "feature2";
    public static final String FEATURE_3 = "feature3";
    public static final String FEATURE_4 = "feature4";
    public static final String FEATURE_5 = "feature5";
    public static final String FEATURE_6 = "feature6";
    public static final String FEATURE_7 = "feature7";
    public static final String FEATURE_8 = "feature8";
    
    public static final String ROBOTIC_NAME = "roboticname";
    public static final String TECHNOLOGY_NAME = "technologyName";
    public static final String ABOUT_NAME = "aboutname";
    
    public static final String VIDEO = "video";
    public static final String SLIDE_1 = "slide1";
    public static final String SLIDE_2 = "slide2";
    public static final String SLIDE_3 = "slide3";
    public static final String SLIDE_4 = "slide4";
    
   public static final String FILE_MP4 = ".mp4";
   public static final String FILE_MP4_UPPER = ".MP4";
   
   public static final String DOT = ".";
   
   public static final int EVENT = 1;
   public static final int NEWS = 2;
   public static final int PRESS_RELEASE = 3;
   
   public static final String TINYMCE = "tinyMCE";
   public static final String SLASH = "/";
   public static final String FOLDER_ASSET = "assets";
   public static final String COMMA = ",";
   public static final String SPACE = " ";
   public static final String HYPHEN = "-";
}
