package com.aht.tb.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.aht.tb.entity.GGCaptcha;

@Repository
public interface GGCaptchaRepository extends JpaRepository<GGCaptcha, Integer> {
	
}
