package com.aht.tb.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.aht.tb.entity.SlideImage;

public interface SlideImageRepository extends JpaRepository<SlideImage, Integer>{
    
    SlideImage findByslidename(String slidename);
    List<SlideImage> findByslidepage(String slidepage);
    
    @Transactional
    @Modifying
    @Query("UPDATE SlideImage s SET s.slideimg = ?2, s.slidepage =?3  WHERE s.slidename = ?1")
    void updateImageContent(String imagename, String slideimg, String slidepage);
}
