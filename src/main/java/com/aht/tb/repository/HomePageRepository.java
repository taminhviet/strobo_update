package com.aht.tb.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.aht.tb.entity.HomePage;
@Repository
public interface HomePageRepository extends JpaRepository<HomePage, Integer>{
}
