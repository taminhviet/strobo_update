package com.aht.tb.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.aht.tb.entity.About;

public interface AboutRepository extends JpaRepository<About, Integer> {

    About findByaboutname(String aboutname);

    @Transactional
    @Modifying
    @Query("Update About a SET a.title1 = ?2, a.title2 = ?3, a.heading1 = ?4, a.heading2 = ?5, a.visit = ?6, a.description1 =?7, a.description2 =?8, a.description3 =?9, a.description4 =?10 WHERE a.aboutname = ?1")
    void updateAbout(String aboutname, String title1, String title2,
            String heading1, String heading2, String visit,
            String description1, String description2, String description3,
            String description4);
}
