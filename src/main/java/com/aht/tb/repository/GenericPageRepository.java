package com.aht.tb.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.aht.tb.entity.GenericPage;

@Repository
public interface GenericPageRepository extends JpaRepository<GenericPage, Integer>{

}
