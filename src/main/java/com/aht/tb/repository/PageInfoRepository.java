package com.aht.tb.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.aht.tb.entity.PageInfo;

@Repository
public interface PageInfoRepository extends JpaRepository<PageInfo, Integer>{
	
}
