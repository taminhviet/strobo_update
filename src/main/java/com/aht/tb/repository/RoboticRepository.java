package com.aht.tb.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.aht.tb.entity.Robotic;

public interface RoboticRepository extends JpaRepository<Robotic, Integer>{

    Robotic findByroboticname(String roboticname);
    
    @Transactional
    @Modifying
    @Query("UPDATE Robotic r SET r.name = ?2, r.description = ?3, r.keyfeatures = ?4  WHERE r.roboticname = ?1")
    void updateTechnology(String roboticname, String name, String description, String keyfeatures);
}
