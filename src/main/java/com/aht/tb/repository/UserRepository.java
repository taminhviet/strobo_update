package com.aht.tb.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.aht.tb.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Short>{
	User findByUsername(String username);
	User findById(int id);
	
	@Transactional
	@Modifying
	@Query("UPDATE User u SET u.email = ?2, u.address = ?3, u.phone = ?4, u.fullname = ?5 WHERE u.id = ?1")
	void updateUser(int id, String email, String address, String phone, String fullname);
	
	@Transactional
	@Modifying
	@Query("UPDATE User u SET u.password = ?2 WHERE u.id = ?1")
	void changePassword(int id, String newPassword);
}
