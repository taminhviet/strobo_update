package com.aht.tb.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.aht.tb.entity.Feature;

public interface FeatureRepository extends JpaRepository<Feature, Integer>{
    
    Feature findByfeaturename(String featurename);
    
    @Transactional
    @Modifying
    @Query("UPDATE Feature f SET f.featureimage = ?2 WHERE f.featurename = ?1")
    void updateFeature (String featurename, String featureimage);
}
