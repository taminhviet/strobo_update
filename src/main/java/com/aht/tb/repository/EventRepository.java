package com.aht.tb.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.aht.tb.entity.NewsEvent;

@Repository
public interface EventRepository extends JpaRepository<NewsEvent, Integer>{

}
