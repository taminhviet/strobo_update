
$(document).ready(function () {
    //VALIDATE EMAIL
    var timer = null;
    $('#email').keyup(function(){
        clearTimeout(timer);
        timer = setTimeout(doStuffEmail, 1000)
    });
    function doStuffEmail() {
        var email = $('#email').val();
        if ($.trim(email).length == 0) {
            $('#email').addClass('border-danger');
            $('.btn-save').addClass('point_e disabled');
            $('#invalid_mes').show();
        }
        else {
            if (validateEmail(email)){
                $('#email').removeClass('border-danger');
                $('.btn-save').removeClass('point_e disabled');
                $('#invalid_mes').hide();
            }
            else {
                $('#email').addClass('border-danger');
                $('.btn-save').addClass('point_e disabled');
                $('#invalid_mes').show();
            }
        }
    }
    function validateEmail(email) {
        var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        if (filter.test(email)) {
            return true;
        }
        else {
            return false;
        }
    }

    //VALIDATE USERNAME
    var timer = null;
    $('#username').keyup(function(){
        clearTimeout(timer);
        timer = setTimeout(doStuffUsername, 1000)
    });
    function doStuffUsername() {
        var username = jQuery('#username').val();
        var check = remove_unicode(username);
        document.getElementById("username").value = check;
    }
    function remove_unicode(str)
    {
        str= str.toLowerCase();
        str= str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g,"a");
        str= str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g,"e");
        str= str.replace(/ì|í|ị|ỉ|ĩ/g,"i");
        str= str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g,"o");
        str= str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g,"u");
        str= str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g,"y");
        str= str.replace(/đ/g,"d");
        str= str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'| |\"|\&|\#|\[|\]|~|$|_/g,"-");
        str= str.replace(/-+-/g,"-"); //thay thế 2- thành 1-
        str= str.replace(/^\-+|\-+$/g,"");
        return str;
    }

    //VALIDATE SERVICE PAGE
    $('.title_input').keyup(function (e) {
        e.preventDefault();
        var title = $(this).val();
        var heading = $(this).attr('data-heading');
        document.getElementById(heading).innerText = title
    });

    // VALIDATE GENERATE PAGE
    var timer = null;
    $('#alias_title').keyup(function(){
        clearTimeout(timer);
        timer = setTimeout(doStuffAlias, 1000)
    });
    function doStuffAlias() {
        var alias = jQuery('#alias_title').val();
        var check = remove_unicode(alias);
        document.getElementById("alias_title").value = check;
    }


    //DELETE FORM SERVICE
    $(".btn-delete-service").click(function () {
        var id = $(this).attr("data-id");
        deleteVar('service',id)
    });

    //DELETE FORM GENERATE PAGE
    $(".btn-delete-generate").click(function () {
        var id = $(this).attr("data-id");
        deleteVar('generate',id)
    });

    function deleteVar(action, id) {
        $.ajax({
            type: 'POST',
            url: '/admin/'+action+'/delete/'+id,
            data: {},
            success: function(){
                window.location.href = '/admin/'+action;
            },
            error: function(response){
                window.location.href = '/admin/home/error';
            }
        });
    }

    //ADD NEW FORM
    $(".btn-addnew").click(function () {
        $("form.form-addnew").show();
        $('a.c.add-new').trigger('click');
    });

    //EDIT MENU ADMIN
    $('.btn-edit-menu').click(function () {
        var id_input = $(this).attr("id-input");
        $('#btn-save-menu'+id_input).show();
        $('#btn-cancel-menu'+id_input).show();
        $('#btn-delete-menu'+id_input).show();
        $('#urlSelect'+id_input).show();
        $('#form_check'+id_input).show();
        $('#btn-edit-menu'+id_input).hide();
        $("#urlvalue"+id_input).removeAttr("disabled");
    });
    $('.btn-cancel-menu').click(function () {
        var id_input = $(this).attr("id-input");
        $('#btn-save-menu'+id_input).hide();
        $('#btn-cancel-menu'+id_input).hide();
        $('#btn-delete-menu'+id_input).hide();
        $('#form_check'+id_input).hide();
        $('#urlSelect'+id_input).hide();
        $('#btn-edit-menu'+id_input).show();
        $("#urlvalue"+id_input).attr("disabled", "disabled");
    });
    $('.btn-add-menu').click(function () {
        var id_input = $(this).attr("id-input");
        $('#btn-save-menux').show();
        $('#btn-cancel-menux').show();
        $('#urlSelectx').show();
        $('.add-menu').show();
        $(this).hide();
    });
    $('.btn-add-feature').click(function () {
    	$('.add-feature').show();
    	$(this).hide();
    });
    $('.btn-add-slide').click(function () {
    	var id = $(this).attr('id');
    	$('#'+ id).show();
    	$(this).hide();
    });
    $('#btn-cancel-menux').click(function () {
        $('#btn-save-menux').hide();
        $(this).hide();
        $('#urlSelectx').hide();
        $('.btn-add-menu').show();
        $('.add-menu').hide();
    });

    //DELETE MENU
    $(".btn-delete-menu").click(function () {
        deleteVar('menu', $(this).attr('id-input'))
    });

    //GENERATE KEY
    $('.btn-generate').click(function () {
        $('.config-popup').show();
        $.ajax ({
            type: 'POST',
            url: '/admin/config/key',
            data: {},
            success: function(){
                $('.text-warning.sending').hide();
                $('.text-success.sent').show();
                $('.btn-sendmail-ok').show();
            },
            error: function () {
                window.location.href = '/admin/home/error';
            }
        });
    });
    $('.btn-sendmail-ok').click(function () {
        window.location.href = '/admin/config';
    });
    //
    // $( ".service.col-sm-6.col-md-3" ).hover(
    //     function() {
    //         $( this ).addClass( "s-active" );
    //     }, function() {
    //         $( this ).removeClass( "s-active" );
    //     }
    // );


    // BACKUP DATA
    $('.btn-backup').click(function () {
        window.location.href = '/admin/backup';
    });
    // RESTORE DATA
    $('.btn-restore').click(function () {
        if ($(this).hasClass('btn-danger btn-cal')){
            $('#btn-backup').removeClass('disabled point_e').addClass('btn-backup');
            $('.block-restore').hide();
            $(this).addClass('btn-success').removeClass('btn-danger btn-cal');
            document.getElementById('btn-restore').innerText = "Restore Data";
        }
        else {
            $('#btn-backup').addClass('disabled point_e').removeClass('btn-backup');
            $('.block-restore').show();
            $(this).removeClass('btn-success').addClass('btn-danger btn-cal');
            document.getElementById('btn-restore').innerText = "Cancel";
        }
    });

    // AFTER RESTORE POPUP
    $('.btn-restore-ok').click(function () {
        $('.restore-popup').hide();
    });
});