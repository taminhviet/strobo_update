$(document).ready(function(){
	
	 $('.events-gallery').owlCarousel({
		items: 1,
		loop:false,
		nav: true,
		navText: [
		  "<i class='fa fa-angle-left' aria-hidden='true'></i>",
		  "<i class='fa fa-angle-right' aria-hidden='true'></i>"
		  ],
		dots: false 
	 });
	 var counter = 0;

	$('.row-image>.img').each(function(){
		$(this).attr('data-triggle',counter);
		counter++;
	});
	 $('.events-triggle').hide();
	$('.col-gallery .img').click(function(){
		var triggle = $(this).attr('data-triggle');
		$('.events-triggle').show();
		$('.events-triggle .events-gallery').trigger('to.owl.carousel', triggle);
	});
		$('.events-triggle .close').click(function(){
			$('.events-triggle').hide();
		});
	var userAgent, ieReg, ie;
	userAgent = window.navigator.userAgent;
	ieReg = /msie|Trident.*rv[ :]*11\./gi;
	ie = ieReg.test(userAgent);

	if(ie) {
	  $(".col-gallery .image-container").each(function () {
		var $container = $(this),
			imgUrl = $container.find("img").prop("src");
		if (imgUrl) {
		  $container.css("backgroundImage", 'url(' + imgUrl + ')').addClass("custom-object-fit");
		}
	  });
	}

})
