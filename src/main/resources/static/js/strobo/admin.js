jQuery(document).ready(function(){
    jQuery('.btn-add-new').click(function () {
        jQuery('.add-new').show();
    });

    jQuery('.btn-close-new').click(function () {
        jQuery('.add-new').hide();
    });
    jQuery('.new_type').change(function () {
        var option = this.options[this.selectedIndex].getAttribute("data-select");
        if (option == 'article') {
            jQuery('.row.event').hide();
            jQuery('.row.article').show();
            jQuery('.row.press').hide();
        }
        if (option == 'event') {
            jQuery('.row.event').show();
            jQuery('.row.article').hide();
            jQuery('.row.press').hide();
        }
        if (option == 'press') {
            jQuery('.row.event').hide();
            jQuery('.row.article').hide();
            jQuery('.row.press').show();
        }
    });
});