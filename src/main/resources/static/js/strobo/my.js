jQuery(document).ready(function(){
    var screen_width = jQuery(window).width();
    var screen_height = jQuery(window).height();
    setTimeout(setWH(screen_width, screen_height), 500);
    setTimeout(setWidthOneE(), 500);

    jQuery(window).resize(function() {
        var screen_width = jQuery(window).width();
        var screen_height = jQuery(window).height();
        setTimeout(setWH(screen_width, screen_height), 500);
        setTimeout(setWidthOneE(), 500);
    });
    function setWH(screen_width, screen_height){
        jQuery('.detail-content .view').css("min-height", screen_height);
        jQuery('.detail-content .video video').css("min-height", 'auto');
    }
    new fullpage('#homefull', {
        navigation: true,
        responsiveWidth: 769,
        anchors: ['home', 'service', 'about-us'],
        licenseKey: 'OPEN-SOURCE-GPLV3-LICENSE',
        scrollOverflow: true,
    });

    function setWidthOneE() {
        var width_set = jQuery('.item-content').width();
        jQuery('.all-footers.footer').css("width", width_set);
    }
    
    // ACTIVE SERVICE WHEN CLICK
    jQuery(".service-item").hover(
      function () {
       jQuery(this).trigger('click');
      }, 
      function () {
      }
    );
    jQuery('.service-item').click(function(e){
        e.preventDefault();
       jQuery('.service-item').removeClass('active');
        jQuery(this).addClass('active');
        var type = jQuery(this).attr('type');
        jQuery('.service-item .nomal .title').removeClass('active');
        jQuery('.service-item.active .nomal .title').addClass('active');
        jQuery('.service-item .go-all').removeClass('active');
        jQuery('.service-item.active .go-all').addClass('active');
        jQuery('.service-item img').removeClass('active');
        jQuery('.service-item.active img').addClass('active');
        if (jQuery('.service-seaport').hasClass('active')){
            jQuery("#logo_service").removeAttr('style');
            jQuery("#logo_service").css("background-image", "url('./assets/logo_white.png')");
        }
        else
        {
            jQuery("#logo_service").css("background-image", "url('./assets/logo.png')");
        }
    });

    // GO TO ALL ITEM OF SERVICE
    jQuery('.nomal .title').click(function(){
        var type_service = jQuery(this).attr('type-service');
        jQuery('.service-'+type_service+' .go-all.active').trigger('click');

    });

    jQuery('.go-all').click(function(){
        var link = jQuery(this).attr('type-service');
        window.location.href = location.protocol + '//' + location.host + link;

    });

    function goAllItem(service){
        jQuery('.service-header').hide();
        jQuery('.service-content').addClass('transition-up-hide').removeClass('active');
        jQuery('.service-footer').hide();
        jQuery('.item-content').addClass('active').removeClass('transition-down-hide');
        jQuery('.all-item').removeClass('active');
        jQuery('.all-item.'+service).addClass('active');
    }
    
    // GO BACK ALL SERVICE
    jQuery('.all-footer .back').click(function(e){
        e.preventDefault();
        var service = jQuery(this).attr('service-type');
        jQuery('.service-item').removeClass('active');
        jQuery('.service-'+service).addClass('active');
        jQuery('.all-item.active').removeClass('active');
        jQuery('.item-details.active').removeClass('active');
        jQuery('.item-details .item').removeClass('active');
        jQuery('.service-content').removeClass('transition-up-hide');

        jQuery('.item-content').removeClass('active');
        jQuery('.item-content').removeClass('transition-down').addClass('transition-down-hide');
        jQuery('.service-footer').show();
        jQuery('.service-header').show();
    });
    
    jQuery('.spec-warehouse_2').hide();
    jQuery('.tech-spec-warehouse_2').click(function(e){
        jQuery('.detail-content').hide();
        jQuery('.detail-footer').hide();
        jQuery('.spec-warehouse_2').show();
    });
    jQuery('.back-warehouse2').click(function(e){
        jQuery('.detail-content').show();
        jQuery('.detail-footer').show();
        jQuery('.spec-warehouse_2').hide();
    });
    jQuery('.spec-warehouse_3').hide();
    jQuery('.tech-spec-warehouse_3').click(function(e){
        jQuery('.detail-content').hide();
        jQuery('.detail-footer').hide();
        jQuery('.spec-warehouse_3').show();
    });
    jQuery('.back-warehouse3').click(function(e){
        jQuery('.detail-content').show();
        jQuery('.detail-footer').show();
        jQuery('.spec-warehouse_3').hide();
    });
    jQuery('.spec-warehouse_6').hide();
    jQuery('.tech-spec-warehouse_6').click(function(e){
        jQuery('.detail-content').hide();
        jQuery('.detail-footer').hide();
        jQuery('.spec-warehouse_6').show();
    });
    jQuery('.back-warehouse6').click(function(e){
        jQuery('.detail-content').show();
        jQuery('.detail-footer').show();
        jQuery('.spec-warehouse_6').hide();
    });

    // GO TO FIRST DETAIL
    jQuery('.first-detail').click(function(){
        var first_item = jQuery(this).attr('goto-for');
        jQuery('.'+first_item+' .image img').trigger('click');
    });

    //ABOUT GOTO ST
    jQuery('.button.about-st').click(function(){
        window.open('https://www.stengg.com/');
    });

    // GO TO DETAIL OF EACH ITEM SERVICE
    jQuery('.description p').click(function(){
        gotoDetail(jQuery(this).attr('data-img'), jQuery(this).attr('type-for'));
    });

    jQuery('.image img').click(function(){
        gotoDetail(jQuery(this).attr('data-img'), jQuery(this).attr('type-for'));
        jQuery('body').addClass('hide-overflow');
    });

    function gotoDetail(detail_item, type_for){
        jQuery('.item-details').addClass('active').removeClass('transition-down-hide');
        jQuery('.item-details .item').removeClass('active');
        jQuery('.item-details .item.'+detail_item).addClass('active');
        jQuery('.item-content').removeClass('active transition-down').addClass('transition-up-hide');
        jQuery('.item-details .item.'+detail_item+' .service-img.main').addClass('active');
        jQuery('.item-details .item.'+detail_item+' .img-1').trigger('click');

        jQuery('.for').removeClass('active');
        jQuery('.for-'+type_for).addClass('active');
    }
    
    // GO BACK ALL PRODUCT
    jQuery('.detail-footer .back').click(function(e){
        e.preventDefault();
        var num_video = jQuery(this).attr('num-video');
        jQuery('.item-details.active .item.active .service-img.main.active').removeClass('active');
        jQuery('.item-details').removeClass('active').addClass('transition-down-hide');
        jQuery('.item-content').addClass('active transition-down').removeClass('transition-up-hide');

        var video = document.getElementById('player_'+num_video);
        if (video !== null) {
            video.pause();
        }
        jQuery('.for').removeClass('active');
        jQuery('body').removeClass('hide-overflow');
    });

    //NEXT DETAIL 
    jQuery('.next-detail').click(function(){
        nextprevDetail('next',jQuery(this).attr('type-move'));
    });
    jQuery('.pre-detail').click(function(){
        nextprevDetail('prev',jQuery(this).attr('type-move'));
    });

    function nextprevDetail(action, type){
        var detail_active = 0;
        var d_stop = 0;
        var da_slick = '';
        jQuery('.for-'+type+' .item').each(function(){
            detail_active += 1;
            if (jQuery(this).hasClass('active')) {
                d_stop = detail_active;
                da_slick = jQuery(this).attr('da-slick');
            }
        });
        if (action == 'prev') {
            d_stop --;
        }
        if (action == 'next') {
            d_stop ++;
        }
        if (d_stop > detail_active) {
            var d_stop = 1;
        }
        if (d_stop < 1) {
            var d_stop = detail_active;
        }
        jQuery('.'+type+'_'+d_stop+' .image img').trigger('click');
        jQuery('.item-details .item').removeClass('active');
        jQuery('.item-details .item.'+type+'_'+d_stop).addClass('active');
        
    };

    //ACTIVE MENU 
    jQuery(".menu-item").hover(
        function () {
           jQuery(this).addClass('hover');
        }, 
        function () {
           jQuery(this).removeClass('hover');
        }
    );
    jQuery(".sub-item").hover(
        function () {
           jQuery(this).addClass('hover');
        }, 
        function () {
           jQuery(this).removeClass('hover');
        }
    );

    //SHOW MENU 
    jQuery('.menu').click(function(e){
        e.preventDefault();
        jQuery('.menu-body').addClass('show');
    });

    //HIDE MENU 
    jQuery('.close-menu').click(function(e){
        e.preventDefault();
        jQuery('.menu-body').removeClass('show');
    });

    //GO TO NEXT SERVICE
    jQuery('.prev-service').click(function(e){
       fourService('prev');
    });
    jQuery('.next-service').click(function(e){
       fourService('next');
    });

    function fourService(type) {
        var check = 0;
        jQuery('.service-item').each(function(){
            if (jQuery(this).hasClass('active')) {
                check = Number(check) + 1;
            }
        });
        if (check > 0) {
            var data_service = Number(jQuery('.service-item.active').attr('data-service'));
            jQuery('.service-item').removeClass('active');
            if (type == 'next') {
                data_service ++;
            }
            if (type == 'prev') {
                data_service --;
            }
            if (data_service > 4) {
                jQuery('.services-1').trigger('click');
            }
            if (data_service < 1) {
                jQuery('.services-4').trigger('click');
            }
            if (data_service >= 1 && data_service <= 4) {
                jQuery('.service-item.services-'+data_service).trigger('click');
            }
        }
        else {
            jQuery('.services-1').trigger('click');
        }
    }

    //GO TO CONTACT && CORE TECHNOLOGY
    jQuery('.menu-item').click(function(){
        var type_menu = jQuery(this).attr('data-menu');
        if (jQuery(this).hasClass('open')) {
            jQuery(this).removeClass('open');
        }
        else {
            jQuery(this).addClass('open');
        }
        if (type_menu != 'solutions') {
            jQuery('.menu-body').removeClass('show');
            jQuery(this).removeClass('toggle');
        }           
        jQuery('.menu-item').removeClass('active');
        jQuery(this).addClass('active');
        if (type_menu == 'solutions') {
            jQuery('.menu-item.solutions').removeClass('open');
        }

        if (type_menu == 'contact') {
            jQuery('#homefull').hide();
            jQuery('#our_company').hide();
            jQuery('.contact-page').show();
        }
        else {
            if (type_menu == 'core') {
                jQuery('#homefull').hide();
                jQuery('#our_company').hide();
                jQuery('.contact-page').hide();
                jQuery('.core-tech').show();
            }
            else {
                if (type_menu == 'solutions') {
                    // jQuery('.submenu').slideToggle('slow');
                    jQuery(this).removeClass('active');
                }
                else{
                    jQuery('#homefull').show();
                    jQuery('#our_company').hide();
                    jQuery('.contact-page').hide();
                    jQuery('.core-tech').hide();
                }
            }
        }
    });

    //LOGO BACK_HOME
    jQuery('.logo-back-home, .logo').click(function(){
        jQuery('#homefull').show();
        jQuery('#our_company').show();
        jQuery('.contact-page').hide();
        jQuery('.core-tech').hide();
    });

    //GO TO RMS PAGE
    jQuery('.go-rms').click(function(e){
        e.preventDefault();
        jQuery('.item-details').removeClass('transition-down-hide').addClass('active');
        jQuery('.item-details .item').removeClass('active');
        jQuery('.item.rms').addClass('active');
        jQuery('.item-content').removeClass('active transition-down').addClass('transition-up-hide');
        jQuery('body').addClass('hide-overflow');
    });

    //PLAY PAUSE VIDEO IN DETAIL
    jQuery('.play-button').on('click', function(e){
        e.preventDefault();
        jQuery(this).prev().trigger('click');
        
    });
    jQuery('video').hover(function(e){
        jQuery('.video-airport_1').attr('title','Click to pause/play video');
        jQuery('.video-seaport_1').attr('title','Click to pause/play video');
        jQuery('.video-manufacturing_1').attr('title','Click to pause/play video');
        jQuery('.video-warehouse_2').attr('title','Click to pause/play video');
    });
    jQuery('video').on('click', function(e){
        e.preventDefault();
        this.paused ? this.play() : this.pause();
        this.paused ? jQuery('.play-video').show() : jQuery('.play-video').hide();
        this.paused ? jQuery('.play-button').show() : jQuery('.play-button').hide();
    });

    //PLAY VIDEO FRONTEND
    jQuery('.nomal .video').click(function(e){
        e.preventDefault();
        var service = jQuery(this).attr('data-type-video');
        jQuery('.video-intro').removeClass('active');
        jQuery('.close-video-intro').trigger('click');
        jQuery('.video-intro.'+service).addClass('active');
        document.getElementById(jQuery('.video-intro.'+service+' video').attr('id')).play();
    });

    jQuery('.next-video').click(function(){
        nextVideo('next');
    });
    jQuery('.prev-video').click(function(){
        nextVideo('prev');
    });

    function nextVideo(type){
        var video_active = jQuery('.video-intro.active').attr('data-video-intro');
        if (type == 'next') {
            video_active = Number(video_active) + 1;
        }
        if (type == 'prev') {
            video_active = Number(video_active) - 1;
        }
        jQuery('.video-intro').removeClass('active');
        if (video_active == 1) {
            jQuery('.video-intro.seaport').addClass('active');
        }
        if (video_active == 2) {
            jQuery('.video-intro.manufacturing').addClass('active');
        }
        if (video_active == 3) {
            jQuery('.video-intro.warehouse').addClass('active');
        }
        if (video_active == 4) {
            jQuery('.video-intro.airport').addClass('active');
        }
        if (video_active < 1) {
            jQuery('.video-intro.airport').addClass('active');
        }
        if (video_active > 4) {
            jQuery('.video-intro.seaport').addClass('active');
        }
        jQuery('.popup-video').each(function() {
            document.getElementById(jQuery(this).attr('id')).pause();
        });
        document.getElementById(jQuery('.video-intro.active .popup-video').attr('id')).play();
    }

    //CLOSE VIDEO FRONTEND
    jQuery('.close-video-intro').click(function(){
        stopFrontendVideo();
    });

    function stopFrontendVideo(){;
        jQuery('.video-intro').removeClass('active');
        jQuery('.popup-video').each(function() {
            document.getElementById(jQuery(this).attr('id')).pause();
        });
    }

    // PLAY VIDEO HOME FRONTEND
    var vid_home_bg = document.getElementById("home_video_bg");
    if (vid_home_bg) {
        vid_home_bg.play();
    }
    
    jQuery(".btn-play-video").click(function(){
        jQuery('.video-home').show();
        document.getElementById('home_video_popup').play();
    });
    jQuery(".video-home .popup-content .close-video").click(function(){
        jQuery('.video-home').hide();
        document.getElementById('home_video_popup').pause();
    });
    
    //SLIDE IMAGE PRODUCT
    jQuery('.image img').click(function(){
        createSlider(jQuery(this));
    });

    jQuery('.description p').click(function(){
        createSlider(jQuery(this));
    });

    function createSlider(obj){
        var detail_item = obj.attr('data-img');
        var type = obj.attr('type-for');
        jQuery('.sub-item.'+type).addClass('spec');
        if (jQuery('.'+detail_item+ ' .thumbnail').length > 0) {
            jQuery('.'+detail_item+ ' .thumbnail').owlCarousel({
                loop:false,
                margin:10,
                nav:false,
                dots:false,
                touchDrag: true,
                mouseDrag: true,
                pullDrag: true,
                freeDrag: true,
                responsive:{
                    0:{
                        items:2
                    },
                    600:{
                        items:3,
                        loop: true
                    },
                    1000:{
                        items:5
                    }
                }
            })
        }
        if (jQuery('.'+detail_item+ ' .img-1').length == 0) {
            jQuery('img.main.'+detail_item).attr('src',jQuery('img.main.'+detail_item).attr('src'));
        }
        else{
            jQuery('.'+detail_item+ ' .img-1').trigger('click');
        }
    };
    
    
    jQuery('.img-thumbnail').click(function(){
        jQuery('.img-thumbnail').parent().removeClass('active');
        jQuery(this).parent().addClass('active');
        jQuery('.service-img.main.active.'+jQuery(this).attr('type-click')).attr('src',jQuery(this).attr('src'));
    });
    
    jQuery('.next-item').click(function(e){
        e.preventDefault();
        getImg('next',jQuery(this).attr('data-slide'));
    });

    jQuery('.prev-item').click(function(e){
        e.preventDefault();
        getImg('prev',jQuery(this).attr('data-slide'));
    });
    function getImg(type, data_slide) {
        var check_active = 0;
        var check_num = 0;
        jQuery('.img-thumbnail.'+data_slide).each(function(){
            check_num ++;
            if (jQuery(this).hasClass('active')) {
                check_active ++;
            }
        });
        if (check_active > 0) {
            var data_img = jQuery('.img-thumbnail.active.'+data_slide).attr('data-img');
            if (type == 'next') {
                data_img ++;
                jQuery('.slick-next.slick-arrow').trigger('click');
            }
            if (type == 'prev') {
                data_img --;
                jQuery('.slick-prev.slick-arrow').trigger('click');
            }            
            if (data_img < 1) {
                jQuery('.img-thumbnail.img-'+check_num+'.'+data_slide).trigger('click');
            }
            if (data_img > check_num) {
                // jQuery('.img-thumbnail.img-1.'+data_slide).trigger('click');
            }
            if (data_img >= 1 && data_img <= check_num) {
                jQuery('.img-thumbnail.img-'+data_img+'.'+data_slide).trigger('click');
            }
        }
        else
        {
            jQuery('.img-thumbnail.img-1.'+data_slide).trigger('click');
        }
    }
    jQuery(window).load(function(){
        var screen_width = jQuery(window).width();
        alignHeight('ul.tech li');
        if (screen_width <= 768) {
            jQuery('.services-1').trigger('click');
            jQuery('.description p.right').removeClass('right');
        }
        else {
            jQuery('.seaport_1 .description p').addClass('right');
            //
            jQuery('.manufacturing_1 .description p').addClass('right');
            jQuery('.manufacturing_2 .description p').addClass('right');
            //
            jQuery('.warehouse_1 .description p').addClass('right');
            jQuery('.warehouse_2 .description p').addClass('right');
            jQuery('.warehouse_3 .description p').addClass('right');
            //
            jQuery('.airport_1 .description p').addClass('right');
            jQuery('.airport_2 .description p').addClass('right');
        }
    });

    jQuery(window).resize(function(){
        var screen_width = jQuery(window).width();
        if (screen_width <= 768) {
            jQuery('.services-1').trigger('click');
            jQuery('.description p.right').removeClass('right');
        }
        else {
            jQuery('.seaport_1 .description p').addClass('right');
            //
            jQuery('.manufacturing_1 .description p').addClass('right');
            jQuery('.manufacturing_2 .description p').addClass('right');
            //
            jQuery('.warehouse_1 .description p').addClass('right');
            jQuery('.warehouse_2 .description p').addClass('right');
            jQuery('.warehouse_3 .description p').addClass('right');
            //
            jQuery('.airport_1 .description p').addClass('right');
            jQuery('.airport_2 .description p').addClass('right');
        }
    });


    //FUNCTION SET HEIGHT ALIGHT
    
    alignHeight('ul.tech li');
    jQuery(window).resize(function(){
        alignHeight('ul.tech li');
    });

    function alignHeight(selector) {
        jQuery(selector).css('height', '');
        var maxHeightName = 0;
        jQuery(selector).each(function() {
          if (maxHeightName < jQuery(this).height()) {
            maxHeightName = jQuery(this).height()
          }
        });
        if (maxHeightName > 0) {
          jQuery(selector).css('height', maxHeightName);
        }
    }

    // ST LOGO
    jQuery('.st-engineering').click(function(){
        window.open('https://www.stengg.com');
    });

    //PLAY VIDEO CLICK BUTTON
    jQuery('.watch-video a').click(function(){
        var type_video = jQuery(this).attr('type-video');
        var container = jQuery("html,body");
        var scrollTo = jQuery('.'+type_video+' .video');

        jQuery(scrollTo).attr("id","#"+type_video)

        // container.animate({scrollTop: scrollTo.offset().top - container.offset().top + container.scrollTop(), scrollLeft: 0},2000);

         var target = '#' + type_video;
         jQuery('html, body').animate({scrollTop: jQuery(target).offset().bottom}, 'slow');

        jQuery('#player_'+type_video).trigger('click');
    });
    
});